XLMANIP
=======

This is a work-in-progress Common Lisp system for reading and (someday) writing Microsoft Excel(tm) spreadsheets.
It is patterned on the Python `xlrd` package.

To date:

- Reads '.xlsx' spreadsheet contents.
- Test suite validates data against the Python `xlrd` test spreadsheets.
