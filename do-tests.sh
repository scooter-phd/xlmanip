#!/bin/sh

tmpf=/tmp/xlmanip$$a.lisp
cat >${tmpf} <<__EOF__
(proclaim '(optimize speed space))
(ql:quickload :xlmanip)
(ql:quickload :xlmanip/tests)
(time (asdf:oos 'asdf:test-op :xlmanip/tests))
(quit)
__EOF__

trap "rm -f ${tmpf}" 0 1 2 3 15


for l in sbcl ccl clisp ecl; do
  echo "\n======== $l ========\n"
  cl-launch --lisp $l --execute --quicklisp --init '(load "'"${tmpf}"'")'
done
