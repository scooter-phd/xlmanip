.. xlmanip:workbook documentation

.. cl:package:: xlmanip


Reader Debugging
================

The ``:verbose`` keyword argument emits debuggung output from the :cl:function:`xlmanip:xlread` reader. There are two kinds of debugging output: *levels* and *specials*. Debugging levels are additive: a higher debugging level generates more output and includes levels below it. For example, if the current debugging level is 3, output from debugging levels 2 and 1 are also emitted. Special debugging constants are intended for specific purposes that don't quite fit into the debugging level scheme. Special debugging output is only emitted when requested.

.. cl:function:: debug-level

.. cl:variable:: +xl-debug-all+

.. cl:variable:: +xl-debug-phases+

.. cl:variable:: +xl-debug-validate+

.. cl:variable:: +xl-debug-data+

.. cl:variable:: +xl-debug-rows+

.. cl:variable:: +xl-debug-xml+

.. cl:variable:: +xl-dump-shared-strings+

.. cl:variable:: +xl-debug-date-formats+

.. cl:variable:: +xl-debug-allocate+

.. cl:function:: workbook-emit-debug
