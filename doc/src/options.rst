.. XLMANIP options...

.. cl:package:: xlmanip


Reader options
==============

In addition to emitting debug output, these are options that control warnings and other information produced by the XLMANIP reader.
The unit tests turn them off temporarily.

.. cl:variable:: *xl-bad-date-guesses*

   This is a `defparameter`.
