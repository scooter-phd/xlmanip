.. xlmanip:workbook documentation

.. cl:package:: xlmanip


XLMANIP:WORKBOOK
=================

.. cl:type:: workbook

Accessing worksheets
--------------------

.. cl:generic:: get-worksheet

.. cl:method:: get-worksheet workbook common-lisp:string

.. cl:method:: get-worksheet workbook common-lisp:integer

.. cl:function:: workbook-nsheets


Workbook slot accessors (internals)
-----------------------------------

.. cl:generic:: workbook-verbose

Set or get the workbook's current verbosity level.

.. cl:method:: workbook-verbose workbook

.. cl:generic:: workbook-verbose-log

Set or get where the workbook's verbosity messages are logged.

.. cl:method:: workbook-verbose-log workbook

.. cl:generic:: workbook-datemode

.. cl:method:: workbook-datemode workbook

.. FIXME!  cl:generic:: workbook-biff-version

.. FIXME!  cl:generic:: workbook-name-objects

.. FIXME!  cl:generic:: workbook-codepage

.. FIXME!  cl:generic:: workbook-encoding

.. FIXME!  cl:generic:: workbook-countries

.. FIXME!  cl:generic:: workbook-user-name

.. FIXME!  cl:generic:: workbook-props

.. FIXME!  cl:generic:: workbook-font-list

.. FIXME!  cl:generic:: workbook-xf-list

.. FIXME!  cl:generic:: workbook-format-list

.. FIXME!  cl:generic:: workbook-format-map

.. FIXME!  cl:generic:: workbook-style-name-map

.. FIXME!  cl:generic:: workbook-color-map

.. FIXME!  cl:generic:: workbook-pallate-record

.. FIXME!  cl:generic:: workbook-name-and-scope-map

.. FIXME!  cl:generic:: workbook-name-map

.. FIXME!  cl:generic:: workbook-ragged-rows

.. FIXME!  cl:generic:: workbook-per-sheet-info

.. FIXME!  cl:generic:: workbook-sheet-visibility

.. FIXME!  cl:generic:: workbook-sh-abs-posn

.. FIXME!  cl:generic:: workbook-shared-strings

.. FIXME!  cl:generic:: workbook-rich-text-runlist-map

.. FIXME!  cl:generic:: workbook-raw-user-name

.. FIXME!  cl:generic:: workbook-sheethdr-count

.. FIXME!  cl:generic:: workbook-builtinfmtcount

.. FIXME!  cl:generic:: workbook-xfcount

.. FIXME!  cl:generic:: workbook-actual-fmt-count

.. FIXME!  cl:generic:: workbook-xf-index->xl-type

.. FIXME!  cl:generic:: workbook-xf-epilogue-done

.. FIXME!  cl:generic:: workbook-supbook-locals-inx

.. FIXME!  cl:generic:: workbook-supbook-addins-inx

.. FIXME!  cl:generic:: workbook-all-sheets-map

.. FIXME!  cl:generic:: workbook-externsheet-info

.. FIXME!  cl:generic:: workbook-externsheet-type-b57

.. FIXME!  cl:generic:: workbook-extnsht-name-from-num

.. FIXME!  cl:generic:: workbook-sheet-num-from-name

.. FIXME!  cl:generic:: workbook-extnsht-count

.. FIXME!  cl:generic:: workbook-supbook-types

.. FIXME!  cl:generic:: workbook-resources-released

.. FIXME!  cl:generic:: workbook-addin-func-names

