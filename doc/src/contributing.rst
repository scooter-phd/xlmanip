############
Contributing
############

Building The Documentation
--------------------------

From the source's top level directory:

.. code-block:: shell

   $ virtualenv-2.7 sphinx-env
   $ source sphinx-env/bin/activate
   $ pip install sphinx sphinxcontrib-cldomain sphinx_bootstrap_theme pygments_cl_repl
   $ cd doc
   $ make html
