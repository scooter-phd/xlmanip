.. Documentation for the XLMANIP system and package
.. (c) 2015, B. Scott Michel (bscottm@ieee.org)

License: `Lisp LGPL`_

``XLMANIP`` is a Common Lisp package for reading and writing Microsoft Excel spreadsheet files. It is roughly modeled on
(adapted from) the Python *xlrd* and *xlwr* modules. The source code is available on gitlab_.

**What XLMANIP is:** ``XLMANIP`` is currently a work-in-progress content extraction system and package. Only the ``XLSX``
(XML-based) spreadsheets can be successfully read. The writer does not exist.

The **really quick** start:

.. code-block:: common-lisp-repl

   CL-USER> (ql:quickload :xlmanip)
   CL-USER> (let* ((book (xlmanip:xlread (asdf:system-relative-pathname :xlmanip (uiop:strcat "tests/sheets/" "reveng1.xlsx"))))
                   (sheet (xlmanip:get-worksheet book "ZZZfirstsheet"))
                   (iter (xlmanip:make-cell-range-iterator sheet (xlmanip:worksheet-all-cells-range sheet)
                                                           :by-column t)))
              (do ((i (funcall iter)
                      (funcall iter)))
                  ((null i) 'completed)
                (format t "[~3D, ~3D] ~A~%"
                        (xlmanip:sheet-cell-row i)
                        (xlmanip:sheet-cell-col i)
                        (xlmanip:sheet-cell-value i))))
   ; Warning: Ambiguous date format: d=14, n=3 fmt="yyyy\\-mm\\-dd\\Thhmmss.000"
   ; While executing: XLMANIP::IS-DATE-FORMAT-STRING, in process repl-thread(12).
   [  0,   0] description
   [  1,   0] empty string
   [  2,   0] len near 128
   [  3,   0] len near 128
   [  4,   0] len near 128
   [  5,   0] non-ascii
   [  6,   0] zero
   [  7,   0] one
   [  8,   0] minus 1
   [  9,   0] 1.23
   [ 10,   0] 1.234
   [ 11,   0] 123456
   [ 12,   0] 1.23456789012345
   [ 13,   0] one third
   ...

The **really quick** explanation of the code:

- Read a workbook from the *reveng1.xlsx* test workbook collection. `(asdf:system-relative-pathname :xlmanip ...)`  generates
  the path to the test workbooks and spreadsheets.

- Get a reference to the worksheet named "*ZZZfirstsheet*".

- Make an iterator function that iterates over the entire worksheet. `(xlmanip:worksheet-all-cells-range)` creates a
  :cl:type:`cell-range` object that encompasses all of the cells in the worksheet.

- For each cell returned by the iterator, print the row, column and cell value.

.. _Lisp LGPL: http://opensource.franz.com/preamble.html
.. _gitlab: https://gitlab.com/scooter-phd/xlmanip


Reading Spreadsheets
--------------------

.. cl:package:: xlmanip

.. cl:function:: xlread

Documentation
=============

.. toctree::
   :maxdepth: 2

   workbook
   worksheet
   cells
   options
   debugging
   contributing

Indices and tables
==================

.. toctree::

   dependencies

* :ref:`genindex`
* :ref:`search`
