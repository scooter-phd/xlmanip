.. xlmanip:worksheet documentation

.. cl:package:: xlmanip


XLMANIP:WORKSHEET
=================

.. cl:type:: worksheet

Useful slot accessors
---------------------

.. note::
   These slot methods are read-write. However, it's not a good idea to actually call :cl:generic:`COMMON-LISP:SETF` on these
   slot methods because strange things will happen.

.. cl:generic:: worksheet-name

.. cl:method:: worksheet-name worksheet

.. cl:generic:: worksheet-nrows

.. cl:method:: worksheet-nrows worksheet

.. cl:generic:: worksheet-ncols

.. cl:method:: worksheet-ncols worksheet

.. cl:generic:: worksheet-dimnrows

.. cl:method:: worksheet-dimnrows worksheet

.. cl:generic:: worksheet-dimncols

.. cl:method:: worksheet-dimncols worksheet

Operating on Cell Ranges
------------------------

.. cl:type:: cell-range

.. cl:macro:: with-worksheet-rows

.. cl:macro:: with-all-worksheet-rows

.. cl:function:: worksheet-all-cells-range

.. cl:function:: make-cell-range-iterator

Sheet cells and references
--------------------------

The :doc:`cells` page documents the :cl:type:`sheet-cell` type.

.. cl:function:: parse-cell-ref

.. cl:function:: to-cell-ref

Internal Representation: Quadtrees
----------------------------------

Internally, a worksheet is a two level quadtree structure. This is a trade-off between space efficiency and access efficiency. A
XML worksheet can conceivably store :math:`2^{20}` cells, but in the nominal case, worksheets tend to be small. While an array
or vector-of-vectors solution would result in very fast access, such a solution can be very space inefficient for sparse
worksheets. A quadtree solves the problem for space efficiency, but for small spreadsheets, a single quadtree becomes very deep,
very quickly. Consequently, a quadtree-of-quadtrees (two level quadtree structure) overcomes the depth problem and optimizes for
the average case of small, dense worksheets.

- The first level quadtrees store `1024x1024` :cl:type:`sheet-cell` blocks. One quadtree stores cells `(0,0)` to `(1023, 1023)`,
  another quadtree stores cells `(1024, 1024)` to `(2047, 2047)`, etc. Generally speaking, small spreadsheets are smaller than
  `1024x1024` and there will only be *one* quadtree in the first level.

- The second level quadtrees store the actual :cl:type:`sheet-cell` objects.

.. cl:type:: quadtree

.. cl:function:: quadtree-children

   The accessor method to the :cl:type:`quadtree` node's children. At the first level of quadtrees, these children are
   :cl:type:`quadtree` objects. At the second level, these children are :cl:type:`sheet-cell` objects.

   :param quadtree: The quadtree node.

.. cl:function:: quadtree-boundary

   The accessor method to the :cl:type:`quadtree` node's bounding box. It is a cell range (:cl:type:`cell-range`).

   :param quadtree: The quadtree node.
