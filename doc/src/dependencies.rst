Dependencies
============

XLMANIP depends on the following CL packages, all of which can be downloaded and
updated via quicklisp_ (the preferred way of doing things):

.. table::

   ===============================  =========================================
            Package                              Description
   ===============================  =========================================
   `Common Lisp Zip Library`_       ZIP file reading
   `Babel`_                         Unicode handling
   CXML_, CXML-STP_                 XML parsing
   Alexandria_                      Utility functions, e.g., _iota_.
   parse-number_                    Numeric (non-integer) parsing.
   split-sequence_                  Sequence splitting/parsing.
   UIOP_                            Portability and a better ``strcat``.
   ===============================  =========================================

.. _quicklisp: https://www.quicklisp.org/beta/
.. _Common Lisp Zip Library: https://common-lisp.net/project/zip/
.. _Babel: https://common-lisp.net/project/babel/
.. _CXML: https://common-lisp.net/project/cxml/
.. _CXML-STP: http://www.lichteblau.com/cxml-stp/
.. _Alexandria: https://common-lisp.net/project/alexandria/
.. _parse-number: https://github.com/sharplispers/parse-number
.. _split-sequence: https://github.com/sharplispers/split-sequence
.. _UIOP: https://github.com/fare/asdf/tree/master/uiop
