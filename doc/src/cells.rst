.. XLMANIP cell documentation:

.. cl:package:: xlmanip


XLMANIP:SHEET-CELL
==================

Sheet cells are the most basic container of information within a spreadsheet.

.. cl:type:: sheet-cell

.. cl:function:: sheet-cell-ctype

   Accessor method to the cell's type. See :ref:`cell_types` for the various cell type constants.

.. cl:function:: sheet-cell-value

   Accessor method to the cell's value. May be :cl:variable:`COMMON-LISP:NIL`, a string or a single precision floating point number.

.. cl:function:: sheet-cell-formula

   Accessor method to the cell's formula. May be :cl:variable:`COMMON-LISP:NIL` or a string.

.. cl:function:: sheet-cell-row

   Accessor method to the cell's row position. This is a :cl:type:`COMMON-LISP:FIXNUM`.

.. cl:function:: sheet-cell-col

   Accessor method to the cell's column position.  This is a :cl:type:`COMMON-LISP:FIXNUM`.

.. cl:function:: sheet-cell-xf-index

   Accesor method to the cell's extended formatting index.

.. cl:variable:: empty-cell

   .. warning:: Keep in mind that a variable is essentially a pointer to something (a place). Modifying
      :cl:variable:`empty-cell` will cause the spreadsheet's contents to change very dramatically.

.. _cell_types:

Cell types
----------

.. cl:variable:: XL-CELL-EMPTY

.. cl:variable:: XL-CELL-TEXT

.. cl:variable:: XL-CELL-NUMBER

.. cl:variable:: XL-CELL-DATE

.. cl:variable:: XL-CELL-BOOLEAN

.. cl:variable:: XL-CELL-ERROR

.. cl:variable:: XL-CELL-BLANK
