(in-package #:cl-user)
(defpackage #:xlmanip-system (:use #:cl #:asdf))
(in-package #:xlmanip-system)

(defsystem :xlmanip
  :depends-on (:zip :babel :cxml :cxml-stp :alexandria :split-sequence :parse-number :uiop)
  :description "Library for reading and writing MS Excel spreadsheets"
  :author "B. Scott Michel (bscottm@ieee.org)"
  :license "Lisp-LGPL"
  :components ((:file "src/package")
               (:file "src/constants" :depends-on ("src/package"))
               (:file "src/utils" :depends-on ("src/package" "src/constants"))
               (:file "src/formatting" :depends-on ("src/package" "src/classes/workbook"))
               (:file "src/xlread" :depends-on ("src/package" "src/xlsx"))
               (:file "src/xlsx" :depends-on ("src/package" "src/formatting" "src/utils"
                                              "src/classes/workbook"
                                              "src/classes/worksheet"
                                              "src/classes/xl-format-data"
                                               "src/classes/sheet-cell"
                                              "src/classes/cell-note"
                                              "src/classes/cell-range"))
	       (:file "src/quadtree" :depends-on ("src/package"
						  "src/constants"
					          "src/classes/cell-range"
						  "src/classes/sheet-cell"))
	       (:file "src/worksheet-cells" :depends-on ("src/package"
	                                                 "src/quadtree"))

               (:file "src/classes/workbook" :depends-on ("src/package"
                                                          "src/constants"
                                                          "src/utils"
                                                          "src/classes/extended-format"
                                                          "src/classes/name-object"
                                                          "src/classes/sheet-info"))
               (:file "src/classes/worksheet" :depends-on ("src/package" "src/constants" "src/utils"
                                                           "src/classes/sheet-cell"
							   "src/worksheet-cells"))
               (:file "src/classes/extended-format" :depends-on ("src/package"))
               (:file "src/classes/name-object" :depends-on ("src/package"))
               (:file "src/classes/xl-format-data" :depends-on ("src/package"))
               (:file "src/classes/sheet-info" :depends-on ("src/package"))
               (:file "src/classes/sheet-cell" :depends-on ("src/package"
                                                            "src/constants"))
               (:file "src/classes/cell-note" :depends-on ("src/package"))
               (:file "src/classes/cell-range" :depends-on ("src/package"))))

;; To execute the unit tests:
;;
;; (asdf:operate 'asdf:load-op :xlmanip/tests)
;; (asdf:operate 'asdf:test-op :xlmanip/tests)

(defmethod perform ((o test-op) (c (eql (find-system :xlmanip))))
  (oos 'load-op 'xlmanip/tests)
  (oos 'test-op 'xlmanip/tests))

(defsystem xlmanip/tests
    :defsystem-depends-on (:fiveam)
    :depends-on (:xlmanip)
    :components ((:file "tests/xlmanip-tests")
		 (:file "tests/quadtree"
		  :depends-on ("tests/xlmanip-tests"))
		 (:file "tests/cell-parse"
		  :depends-on ("tests/xlmanip-tests"))
		 (:file "tests/data-validate"
		  :depends-on ("tests/xlmanip-tests" 
		    "tests/cell-parse"
		    "tests/quadtree"
		    ;; Ground truth data files:
		    "tests/validate/reveng1" "tests/validate/apachepoi_49609" "tests/validate/merged_cells"
		    "tests/validate/self_evaluation_report_2014-05-19"
		    "tests/validate/test_comments_excel" "tests/validate/test_comments_gdocs" "tests/validate/text_bar"))

		 (:file "tests/validate/reveng1"
		  :depends-on ("tests/xlmanip-tests"))
		 (:file "tests/validate/apachepoi_49609"
		  :depends-on ("tests/xlmanip-tests"))
		 (:file "tests/validate/merged_cells"
		  :depends-on ("tests/xlmanip-tests"))
		 (:file "tests/validate/self_evaluation_report_2014-05-19"
		  :depends-on ("tests/xlmanip-tests"))
		 (:file "tests/validate/test_comments_excel"
		  :depends-on ("tests/xlmanip-tests"))
		 (:file "tests/validate/test_comments_gdocs"
		  :depends-on ("tests/xlmanip-tests"))
		 (:file "tests/validate/text_bar"
		  :depends-on ("tests/xlmanip-tests"))))


(defmethod perform ((o test-op) (c (eql (find-system 'xlmanip/tests))))
  (uiop:symbol-call '#:fiveam '#:run!
    (uiop:find-symbol* '#:xlmanip-suite '#:xlmanip-unittest)))

(defmethod operation-done-p ((o test-op) (c (eql (find-system 'xlmanip/tests))))
  (values nil))
