#|
  This code was adapted from a quad tree implementation by Masayuki Takagi (kamonama@gmail.com).
  See the Github(tm) code repository: takagi/quadtree.

  Quadtrees track merged cell regions. This particular implementation is specialized to integer
  points.
|#

(in-package :xlmanip)

(eval-when (:compile-toplevel :load-toplevel)
  (dolist (var '(*worksheet-quad-capacity*
                  *max-worksheet-quad-depth*
                  *default-max-depth*
                  *default-max-capacity*))
    (makunbound var)))

(defvar *default-max-depth* 15)

(defvar *default-max-capacity* 8)

(defstruct (quadtree (:constructor %make-quadtree))
  "The quadtree structure.

:keyword children: An array of *things* or nil. When number of elements exceeds *max-capacity*, this node is split and
   the children redistributed into the subtrees.
:keyword nw: The northwest subtree, relative to the boundary's center.
:keyword ne: The northeast subtree, relative to the boundary's center.
:keyword sw: The southwest subtree, relative to the boundary's center.
:keyword se: The southeast subtree, relative to the boundary's center.
:keyword boundary: The bounding box for this node.
:keyword depth: The current depth for this subtree (indicates how deep the subtree is, used for sanity checking.)
:keyword max-depth: The maximal depth for a subtree.
:keyword max-capacity: The number of elements allowed per children array before splitting the node into subtrees.

.. note::
   You should not have to manually create a quadtree and insert it into a worksheet. This is done automagically for you."
  
  (children nil :type (or null (vector *)))
  (nw nil :type (or null quadtree))
  (ne nil :type (or null quadtree))
  (sw nil :type (or null quadtree))
  (se nil :type (or null quadtree))
  (boundary nil :type (or null cell-range))
  (depth 0 :type fixnum)
  (max-depth *default-max-depth* :type fixnum)
  (max-capacity *default-max-capacity* :type fixnum))

(defun make-quadtree (start-row start-col end-row end-col
                                &key (max-depth *default-max-depth*)
                                     (max-capacity *default-max-capacity*))
  "Make a new quadtree root (depth 0)."
  (%make start-row start-col end-row end-col 0 max-depth max-capacity))

(defun %make (start-row start-col end-row end-col depth max-depth max-capacity)
  (check-type start-row fixnum)
  (check-type start-col fixnum)
  (check-type end-row fixnum)
  (check-type end-col fixnum)

  (unless (<= 0 depth max-depth)
    (error "The value ~S is an linvalid value as depth." depth))

  (%make-quadtree :boundary (make-instance 'cell-range :start-row start-row :start-col start-col
                                                       :end-row end-row :end-col end-col)
                  :depth depth
                  :max-depth max-depth
                  :max-capacity max-capacity))

(declaim (inline quadtree-node-p quadtree-leaf-p quadtree-empty-p))

(defun quadtree-node-p (quadtree)
  (with-accessors ((children quadtree-children)
                   (nw quadtree-nw)
                   (ne quadtree-ne)
                   (sw quadtree-sw)
                   (se quadtree-se))
                  quadtree
  (and (null children)
       (or nw ne sw se))))

(defun quadtree-leaf-p (quadtree)
  (with-accessors ((children quadtree-children)
                   (nw quadtree-nw)
                   (ne quadtree-ne)
                   (sw quadtree-sw)
                   (se quadtree-se))
                  quadtree
    (and children
         (null (or nw ne sw se)))))

(defun quadtree-empty-p (qtree)
  (or (null qtree)
      (null (or (quadtree-children qtree)
                (quadtree-nw qtree)
                (quadtree-ne qtree)
                (quadtree-sw qtree)
                (quadtree-se qtree)))))

(defgeneric compare-child-objects (o1 o2)
  (:documentation "Generic comparators for sorting quadtree children. See CL-USER:SORT for
the semantics of comparison functions (returns t if less, nil if greater than or equal.)"))

(defun subdivide (quadtree)
  (unless (quadtree-leaf-p quadtree)
    (error "The quadtree ~A is already subdivided." quadtree))

  (with-accessors ((children quadtree-children)
                   (nw quadtree-nw)
                   (ne quadtree-ne)
                   (sw quadtree-sw)
                   (se quadtree-se)
                   (boundary quadtree-boundary)
                   (depth quadtree-depth)
                   (max-depth quadtree-max-depth)
                   (max-capacity quadtree-max-capacity))
                  quadtree

    (labels ((insert-child (c)
               ;; Don't just stop when the object is inserted into one of the child subtrees, because
               ;; the point might overlap with multiple subtrees.
               (insert-quadtree nw c)
               (insert-quadtree ne c)
               (insert-quadtree sw c)
               (insert-quadtree se c))

             (inner-reinsert (s)
               ;; Insert the middle element, then successively add elements on either side of
               ;; the midpoint. This tends to preserve tree balance.
               (let* ((slen (length s))
                      (mid (ash (1- slen) -1)))
                 (insert-child (aref s mid))
                 (do ((left (1- mid)
                            (1- left))
                      (right (1+ mid)
                             (1+ right)))
                   ((and (< left 0)
                         (>= right slen)))
                   #+(or) (format *error-output* "left ~D right ~D~%" left right)
                   (if (>= left 0)
                     (insert-child (aref s left)))
                   (if (< right slen)
                       (insert-child (aref s right)))))))

      (let* ((start-col (cell-range-start-col boundary))
             (start-row (cell-range-start-row boundary))
             (end-col (cell-range-end-col boundary))
             (end-row (cell-range-end-row boundary))
             (col-mid (ceiling (/ (+ end-col start-col) 2)))
             (row-mid (ceiling (/ (+ end-row start-row) 2)))
             (child-depth (1+ depth)))
        ;; Allocate the subquads
        (unless (<= child-depth max-depth)
          (error "Quadtree's subdivided child depth exceeds maximum quadtree depth (~D vs. ~D)~%"
                 child-depth max-depth))

        (setf nw (%make row-mid start-col end-row col-mid child-depth max-depth max-capacity)
              ne (%make row-mid col-mid end-row end-col child-depth max-depth max-capacity)
              sw (%make start-row start-col row-mid col-mid child-depth max-depth max-capacity)
              se (%make start-row col-mid row-mid end-col child-depth max-depth max-capacity))

        ;; Re-insert the children:
        (inner-reinsert children)
        (setf children nil))))
  t)

(defgeneric intersect-p (qtree thing)
  (:documentation "Quadtree <-> thing intersection predicate. Returns t if thing intersects
the quadtree's bounding box."))

(defgeneric point-intersect-p (thing row col)
  (:documentation "Thing <-> (row, col) intersection predicate. Thing can be a quadtree or cell-range or ...

Note: Not sure that this is really needed. Could re-purpose intersect-p to handle lists as a second argument."))

;; This method is here because it would otherwise cause a circular dependency between
;; quadtree and cell-range. Bad Foo(tm).
(defmethod intersect-p ((quad quadtree) (cr cell-range))
  (cell-range-overlap cr (quadtree-boundary quad)))

(defun insert-quadtree (quad object &key update)
  "Insert an object into a :cl:variable:`quadtree`. Note that the :cl:variable:`quadtree` may be subdivided
as a result of the insertion."
  (with-accessors ((children quadtree-children)
                   (nw quadtree-nw)
                   (ne quadtree-ne)
                   (sw quadtree-sw)
                   (se quadtree-se)
                   (depth quadtree-depth)
                   (max-depth quadtree-max-depth)
                   (max-capacity quadtree-max-capacity)) quad
    (cond
      ;; When the object does not intersect the quad, just return nil.
      ((not (intersect-p quad object)) nil)
      ;; When the quad is a node, recursively insert the object to its
      ;; children.
      ((quadtree-node-p quad) (insert-quadtree nw object)
                              (insert-quadtree ne object)
                              (insert-quadtree sw object)
                              (insert-quadtree se object)
                              t)
      ;; When the quad is full and is not at its max depth, subdivide it and
      ;; recursively insert the object.
      ((and (= (length children) max-capacity)
            (< depth max-depth))
       (subdivide quad)
       (insert-quadtree quad object))
      ;; Otherwise, insert the object to the quad.
      (t
       (cond ((null children)
              (setf children (make-array max-capacity :fill-pointer 0 :element-type (type-of object)))
              (vector-push object children))
             (t
              #+(or) (format *error-output* "-- Inserting ~A~%" object)
              (do* ((num-children (length children))
                    (low 0)
                    (high (1- num-children))
                    (mid (ash (+ high low) -1)
                         (ash (+ high low) -1)))
                (nil nil)
                (let* ((child (aref children mid))
                       (object-lt-child (compare-child-objects object child))
                       (child-lt-object (compare-child-objects child object)))
                  #+(or) (format *error-output* "low ~2D mid ~2D high ~2D object-lt-child ~3A child-lt-object ~3A~%"
                                 low mid high
                                 object-lt-child child-lt-object)
                  (cond ((> low high)
                         ;; Walked off the end
                         (vector-push-extend object children)
                         ;; and exit loop
                         (return))
                        ((= low mid high)
                         ;; All pointing at the same place, so shift children and insert:
                         (let ((insert-point (if object-lt-child
                                               mid
                                               (1+ mid))))
                           (setf (fill-pointer children) (1+ (fill-pointer children)))
                           (dotimes (i (- num-children insert-point))
                             (setf (aref children (- num-children i))
                                   (aref children (- num-children i 1))))
                           (setf (aref children insert-point) object))
                         ;; and exit
                         (return))
                        ((not (or object-lt-child child-lt-object))
                         ;; Neither is greater than the other -> equal. Replace the child with object
                         ;; if the update keyword is set:
                         (when update
                           (setf child object))
                         ;; and exit loop
                         (return))
                        ;; Otherwise, continue searching one side or the other:
                        (object-lt-child
                          (setf high mid))
                        (child-lt-object
                          (setf low (1+ mid))))))
              #+(or) (format *error-output* "children ~:W~%" children)))))))

(defun quadtree-query (quadtree row col)
  (check-type row fixnum)
  (check-type col fixnum)

  (let ((retval (cond
                  ((not (point-intersect-p quadtree row col)) nil)
                  ((quadtree-node-p quadtree)
                   (concatenate 'vector 
                                (quadtree-query (quadtree-nw quadtree) row col)
                                (quadtree-query (quadtree-ne quadtree) row col)
                                (quadtree-query (quadtree-sw quadtree) row col)
                                (quadtree-query (quadtree-se quadtree) row col)))
                  ((quadtree-leaf-p quadtree)
                   (remove nil (map 'vector
                                    (lambda (c)
                                      (when (point-intersect-p c row col) c))
                                    (quadtree-children quadtree)))))))
    (if (/= (length retval) 0)
      retval
      nil)))

(defmethod point-intersect-p ((quadtree quadtree) row col)
  (let ((retval (point-intersect-p (quadtree-boundary quadtree) row col))) 
    #+(or) (format t "(point-intersect-p (quadtree ~A) ~D ~D) = ~A~%" (quadtree-boundary quadtree) row col retval)
    retval))

(defmethod point-intersect-p ((c cell-range) row col)
  (let ((retval (with-accessors ((start-row cell-range-start-row)
                                 (start-col cell-range-start-col)
                                 (end-row cell-range-end-row)
                                 (end-col cell-range-end-col))
                                c
                  (and (<= start-col col)
                       (< col end-col)
                       (<= start-row row)
                       (or (= start-row end-row)
                           (< row end-row))))))
    #+(or) (format t "(point-intersect-p ~A ~D ~D) = ~A~%" c row col retval)
    retval))

(defun reset-quadtree (quadtree)
  (assert (= (quadtree-depth quadtree) 0))
  (setf (quadtree-children quadtree) nil
        (quadtree-nw quadtree) nil
        (quadtree-ne quadtree) nil
        (quadtree-sw quadtree) nil
        (quadtree-se quadtree) nil)
  t)
