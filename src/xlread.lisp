(in-package :xlmanip)

;; (require "zip")

(eval-when (:compile-toplevel :load-toplevel)
  ;; Ensure that only this version of xl-defined-name-map exists:
  (if (boundp '*zipSignature*)
      (makunbound '*zipSignature*)))

;; First 4 bytes of a zipped MS Excel spreadsheet
(defvar *zipSignature* 
  (make-array 4 :initial-contents #(#.(char-code #\P) #.(char-code #\K) #x03 #x04)))

(defun xlread (filename &key (verbose nil)
                             (verbose-log *trace-output*))
    "The Excel spreadsheet reader.

:param filename: The spreadsheet to read.
:type filename: String or ``#P\"path\"``
:key verbose: Verbosity keyword argument.
:key verbose-log: Optional stream to where verbose messages are logged.
:returns: A :cl:type:`workbook` object containing the workbook and worksheets.

Example::

    (let* ((my-workbook (xlread #P\"my_spreadsheet.xlsx\")))
      (dotimes (idx (workbook-nsheets my-workbook))
        (let ((sheet (get-worksheet :index idx)))
	    #| Do cool stuff with a worksheet here... |# )))
"
     
  ; Read file signature, determine if zippped, i.e., it's a newer format XML
  ; spreadsheet
  (let* ((fileSig (make-array 4 :element-type '(unsigned-byte 8))))
  		(with-open-file (zf filename :direction :input :element-type '(unsigned-byte 8))
        (read-sequence fileSig zf)
    		(emit-debug verbose verbose-log +xl-debug-data+ "fileSig = ~A, *zipSignature* = ~A~%" fileSig *zipSignature*)
    		(if (equalp *zipSignature* fileSig)
          (progn
            (emit-debug verbose verbose-log +xl-debug-phases+ "xlread: ZIP format detected.~%")
            (zip:with-zipfile (mszip filename)
              (let ((components (groom-entries (zip:zipfile-entries mszip))))
                (flet ((component-exists? (name)
                                          (multiple-value-bind (v exists) (gethash name components)
                                                               (declare (ignore v))
                                                               exists)))
                      (when (debug-threshold verbose +xl-debug-data+)
                        (format verbose-log "ZIP entries are:~%")
                        (with-hash-table-iterator (next-fun components)
                          (loop
                            (multiple-value-bind (more? hkey v) (next-fun)
                              (declare (ignore v))
                              (unless more? (return))
                              (format verbose-log "  ~A~%" hkey)))))
                      (if (component-exists? "xl/workbook.xml")
                          (xlsx-read mszip components verbose verbose-log)
                          (if (component-exists? "xl/workbook.bin")
                              (error "Excel 2007 xlsb format not supported.")
                              (if (component-exists? "content.xml")
                                  (error "OpenOffice.org ODS format not supported.")
                                  (error "Unkown spreadsheet/ZIP file format."))))))))
          (progn
            (when verbose (format verbose-log "xlread: BIFF format detected.~%")))))))

;; Groom the component entries in the hash table: Ensure they're all lower case and
;; all backslashes are converted to forward slashes for consistency. Apparently, there
;; are tools that don't conform to these rules.
(defun groom-entries (htab)
  (let ((groomed-htab (make-hash-table :test 'equal)))
    (with-hash-table-iterator (next-fun htab)
      (loop
        (multiple-value-bind (more? hkey v) (next-fun)
          (unless more? (return))
          (let ((newkey (substitute #\/ #\\ (string-downcase hkey))))
            (setf (gethash newkey groomed-htab) v)))))
	groomed-htab))
