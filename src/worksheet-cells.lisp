(in-package :xlmanip)

;; Solve for the maximum depth of the tree:
;; n = ((log_{2} max-rows + log_{2} max-cols - log_{2} *worksheet-quad-capacity*) / log_{2} 4)
;;
;; ceiling ensures that any fraction gets bumped up to the next higher integer.

(defvar *worksheet-quad-capacity* 32
  "Number of worksheet child elements per quad level.")

(defvar *max-worksheet-quad-depth*
  (ceiling (/ (- (+ (log +xml-utter-max-rows+ 2)
                    (log +xml-utter-max-cols+ 2))
                 (log *worksheet-quad-capacity* 2))
              (log 4 2))))

(defvar *subquad-capacity* 16
  "Number of worksheets in a sub-quadtree.")

(defvar *subquad-max-rows* 1024
  "Number of rows in a sub-quadtree.")

(defvar *subquad-max-cols* 1024
  "Number of columns in a sub-quadtree.")

(defvar *max-subquad-depth*
  (ceiling (/ (- (+ (log *subquad-max-rows* 2)
                    (log *subquad-max-cols* 2))
                 (log *subquad-capacity* 2))
              (log 4 2))))

(defun make-worksheet-cell-values ()
  "Construct the overall quadtree container for cell values."
  (make-quadtree 0 0 +xml-utter-max-rows+ +xml-utter-max-cols+
                 :max-depth *max-worksheet-quad-depth*
                 :max-capacity *worksheet-quad-capacity*))

(defun find-child-quadtree (qtree row col)
  "Find the child quadtree that contains `(row, col)`."
  (quadtree-query qtree row col))

(defun find-child-subvalue-quadtree (subvalue-vec row col)
  "Find the subvalue quadtree within a quadtree's children."
  (let* ((num-children (length subvalue-vec))
         (low 0)
         (high (1- num-children)))
    (do* ((mid (ash (+ high low) -1)
               ;; No chance of overflow for high+low
               (ash (+ high low) -1)))
         ((> low high) nil)
      (let* ((child (aref subvalue-vec mid))
             (child-boundary (quadtree-boundary child)))
        #+(or) (progn
                 (format *error-output* "low ~D mid ~D high ~D; low > high ~D~%" low mid high (> low high))
                 (format *error-output* "child ~:W~%" child)
                 (format *error-output* "(or (< ~D ~D) (< ~D ~D)~%"
                         col (cell-range-start-col child-boundary)
                         row (cell-range-start-row child-boundary))
                 (format *error-output* "(or (>= ~D ~D) (>= ~D ~D)~%"
                         col (cell-range-end-col child-boundary)
                         row (cell-range-end-row child-boundary)))
        (cond ((point-intersect-p child row col)
               (return child))
              ((or (< col (cell-range-start-col child-boundary))
                   (< row (cell-range-start-row child-boundary)))
               (setf high (1- mid)))
              ((or (>= col (cell-range-end-col child-boundary))
                   (>= row (cell-range-end-row child-boundary)))
               (setf low (1+ mid)))
              (t
               ;; Catch-all case, but should never happen.
               (return nil)))))))

(defmethod intersect-p ((quad quadtree) (thing quadtree))
  (cell-range-overlap (quadtree-boundary quad) (quadtree-boundary thing)))

(defun insert-cell-value (worksheet-qtree cell)
  (with-accessors ((cell-row sheet-cell-row)
                   (cell-col sheet-cell-col)) cell
    (let* ((subvalue-vec (find-child-quadtree worksheet-qtree cell-row cell-col))
           (value-quadtree (unless (null subvalue-vec)
                             (find-child-subvalue-quadtree subvalue-vec cell-row cell-col))))
      #+(or) (format *error-output* "null subvalue-vec ~A, null value-quadtree ~A~%" (null subvalue-vec) (null value-quadtree))
      (when (null value-quadtree)
        (let* ((lower-row (* (floor (/ cell-row *subquad-max-rows*)) *subquad-max-rows*))
               (lower-col (* (floor (/ cell-col *subquad-max-cols*)) *subquad-max-cols*))
               (upper-row (+ lower-row *subquad-max-rows*))
               (upper-col (+ lower-col *subquad-max-cols*)))
          (setf value-quadtree (make-quadtree lower-row lower-col upper-row upper-col
                                              :max-depth *max-subquad-depth*
                                              :max-capacity *subquad-capacity*))
          (insert-quadtree worksheet-qtree value-quadtree)
          #+(or) (format t "~:W~%" worksheet-qtree)))
      (insert-quadtree value-quadtree cell))))

(defmethod intersect-p ((quad quadtree) (thing sheet-cell))
  (with-accessors ((boundary quadtree-boundary))
                  quad
    (with-accessors ((brow-start cell-range-start-row)
                     (brow-end cell-range-end-row)
                     (bcol-start cell-range-start-col)
                     (bcol-end cell-range-end-col)) boundary
      (with-accessors ((cell-row sheet-cell-row)
                       (cell-col sheet-cell-col)) thing
        (and (>= cell-row brow-start)
             (<  cell-row brow-end)
             (>= cell-col bcol-start)
             (<  cell-col bcol-end))))))

(defmethod compare-child-objects ((o1 quadtree) (o2 quadtree))
  (compare-child-objects (quadtree-boundary o1) (quadtree-boundary o2)))
