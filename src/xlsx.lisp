;; ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
;; XLSX spreadsheet reader
;; ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=

(in-package :xlmanip)

#|
.. cl:function:: xlsx-read

XLSX (XML-based) spreadsheet reader.

:param zipf: The ZIP file reader object
:param components: The ZIP file's directory
:param verbose: Verbosity ``boolean`` flag.
:param verbose-log: Stream where verbose messages are logged.
|#
(defun xlsx-read (zipf components verbose verbose-log)
  ;; Ignore 'zipf' for the time being; might be needed at a future date.
  (declare (ignore zipf))

  (let* ((book (make-instance 'workbook
                              :verbose verbose
                              :verbose-log verbose-log)))
    (multiple-value-bind (rels-component rels-exist) (gethash "xl/_rels/workbook.xml.rels" components)
      (if rels-exist
          (let ((rels (process-rels book (zip:zipfile-entry-contents rels-component))))
            (multiple-value-bind (workbook-component workbook-exists) (gethash "xl/workbook.xml" components)
              (if workbook-exists
                  (progn
                    (process-workbook book (zip:zipfile-entry-contents workbook-component) rels)
                    (multiple-value-bind (coreprops-component coreprops-exist)
                        (gethash "docprops/core.xml" components)
                      ;; Missing core properties metadata is not an error.
                      (if coreprops-exist
                          (process-coreprops book (zip:zipfile-entry-contents coreprops-component))))
                    (multiple-value-bind (styles-component styles-exist)
                        (gethash "xl/styles.xml" components)
                      ;; Missing styles is not an error
                      (if styles-exist
                          (process-styles book (zip:zipfile-entry-contents styles-component))))
                    (multiple-value-bind (sharedstrings-component sharedstrings-exist)
                        (gethash "xl/sharedstrings.xml" components)
                      ;; Missing shared strings is not an error
                      (if sharedstrings-exist
                          (load-shared-strings book (zip:zipfile-entry-contents sharedstrings-component))))
                    ;; Finally load the worksheets...
                    (load-worksheets book components rels))
                  (error "Workbook metadata component not present."))))
          (error "Relationships component not present.")))
    book))

;; ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
;; Local utility functions:
;; ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=

(defun convert-name-scope (s)
  "Special conversion for processing a name's scope. Looks for the prefix '_xlnm' on the name, and, if present,
returns 1 (signifying a builtin name). Otherwise, converts the XML string to an integer scope."
  (if (eql (search "_xlnm." s) 0)
      1                                    ;; Builtin
      (convert-xml-unsigned-int s)))

(defun convert-sheet-visibility (s)
  (cond ((null s) 0)
        ((string-equal s "visible") 0)
        ((string-equal s "hidden") 1)
        ((string-equal s "veryhidden") 2)
                                        ; Default case: it is visible.
        (t 0)))

(defun convert-string-missing-blank (s)
  "Ensures that missing strings (i.e., ``nil``) are converted to empty strings; otherwise,
ensures that the string is Unicode."
  (if (not (null s))
      (ensure-unicode s)
      ""))

(defun element-collect-text-string (elt &key (preserve-space nil))
  "Collect the strings in all of the text nodes under an element ``elt`` and return the resulting
concatenated string. ``cxml-stp`` can split the text into separate nodes, which is why this function
exists to concatenate them."
  (if elt
      (let* ((space-attr (cxml-stp:find-attribute-named elt "space" URI_XMLSPACE)))
        (flet ((extract-text (node)
                 (if (typep node 'cxml-stp:text)
                   (uncook-text (cxml-stp:data node) (or preserve-space
                                                         (and space-attr
                                                              (string-equal "preserve" (cxml-stp:value space-attr)))))
                   "")))
          (uiop:reduce/strcat (cxml-stp:list-children elt) :key #'extract-text )))
      elt))

;; ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
;; CXML-STP of-name predicates:
;; ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=

(defvar is-relationships-node (cxml-stp:of-name "Relationships" URI_PKGREL)
  "CXML-STP predicate that matches 'Relationships' nodes.")
(defvar is-relationship-elt (cxml-stp:of-name "Relationship" URI_PKGREL)
  "CXML-STP predicate that matches 'Relationship' nodes.")

(defvar is-workbook-elt (cxml-stp:of-name "workbook" URI_SSML12))
(defvar is-defined-names-elt (cxml-stp:of-name "definedNames" URI_SSML12))
(defvar is-workbookPr-elt (cxml-stp:of-name "workbookPr" URI_SSML12))

(defvar is-sheets-elt (cxml-stp:of-name "sheets" URI_SSML12))
(defvar is-sheet-elt (cxml-stp:of-name "sheet" URI_SSML12))

(defvar is-stylesheet-elt (cxml-stp:of-name "styleSheet" URI_SSML12))

(defvar is-t-node (cxml-stp:of-name "t" URI_SSML12)
  "CXML-STP predicate to match 't' nodes.")
(defvar is-r-node (cxml-stp:of-name "r" URI_SSML12)
  "CXML-STP predicate to match 'r' nodes.")

(eval-when (:compile-toplevel :load-toplevel)
  ;; Ensure that only this version of xl-defined-name-map exists:
  (if (boundp 'xl-defined-name-map)
      (makunbound 'xl-defined-name-map))
  (if (boundp 'xl-core-props)
      (makunbound 'xl-core-props)))

(defvar xl-defined-name-map
  (list (list "name" 'common-name #'ensure-unicode)
        #+(or) (list "comment" nil #'ensure-unicode)
        #+(or) (list "customMenu" nil #'ensure-unicode)
        #+(or) (list "description" nil #'ensure-unicode)
        #+(or) (list "help" nil #'ensure-unicode)
        (list "localSheetId" 'scope #'convert-name-scope)
        (list "hidden" 'hidden #'convert-xml-boolean)
        (list "function" 'funcmacro #'convert-xml-boolean)
        (list "vbProcedure" 'vbasic #'convert-xml-boolean)
        (list "xml" 'macroflag #'convert-xml-boolean)
        (list "functionGroupId" 'funcgroup #'convert-xml-unsigned-int)
        #+(or) (list "shortcutKey" nil #'ensure-unicode)
        #+(or) (list "publishToServer" nil #'convert-xml-boolean)
        #+(or) (list "workbookParameter" nil #'convert-xml-boolean)
        ;; -- Slots that are initialized that don't correspond to a XML attribute ---
        #+(or) (list "" 'any-err nil)
        #+(or) (list "" 'any_external nil)
        #+(or) (list "" 'any_rel nil)
        #+(or) (list "" 'basic_formula_len nil)
        #+(or) (list "" 'binary nil)
        #+(or) (list "" 'builtin nil)
        #+(or) (list "" 'complex nil)
        #+(or) (list "" 'evaluated nil)
        #+(or) (list "" 'excel_sheet_index nil)
        #+(or) (list "" 'excel_sheet_num nil)
        #+(or) (list "" 'option_flags nil)
        #+(or) (list "" 'result nil)  ; Python converts this to None if no value
        #+(or) (list "" 'stack nil))  ; Python converts this to None if no value
  "Mapping from XML attributes to a slot and a data conversion function.")

(defvar xl-core-props
  ;; XML attribute, XML namespace, conversion function
  (list (list "lastModifiedBy" URI_CP #'convert-string-missing-blank)
        (list "creator" URI_DC #'convert-string-missing-blank)
        (list "modified" URI_DCTERMS #'convert-string-missing-blank)
        (list "created" URI_DCTERMS #'convert-string-missing-blank)))

#|
.. cl:function:: process-rels

Parse and collect workbook relationships.

:param book: A ``cl-excel:workbook`` object
:param rels-content: The ZIP file entry to the relationships document

:rtype: hash-table
:return: Mapping between relationship identifiers and ``(target rtype)`` pairs. ``rtype`` is
the trailing component of the relationship URI, which specifies the DTD to which the ``target``
conforms.
|#
(defun process-rels (book rels-content)
  (declare (type workbook book)
           (type (vector (unsigned-byte 8)) rels-content))
  (workbook-emit-debug book +xl-debug-phases+ "=== Relationships ===~%")
  (let* ((rel-document (cxml:parse rels-content (stp:make-builder)))
         ;; Follow the document structure: <Relationships> -> <Relationship>
         (relationships (cxml-stp:find-child-if is-relationships-node rel-document))
         (frels (cxml-stp:filter-children is-relationship-elt relationships))
         (rels (make-hash-table :test #'equal :size (length frels))))
    (workbook-emit-debug book +xl-debug-xml+ "~:_~:W~%" frels)
    (workbook-emit-debug book +xl-debug-data+ "(length frels) = ~S~%" (length frels))
    (if (< 0 (length frels))
        (dolist (elt frels)
          (let* ((rid (cxml-stp:attribute-value elt "Id"))
                 (target (cxml-stp:attribute-value elt "Target"))
                 (rtype (car (last (split-sequence:split-sequence #\/ (cxml-stp:attribute-value elt "Type"))))))
            (if (equal (subseq target 0 1) "/")
              (setf target (subseq target 1 (length target)))
              (setf target (uiop:strcat "xl/" target)))
            (workbook-emit-debug book +xl-debug-data+ "Id=~A, Target=~A, Type=~A~%" rid target rtype)
            ;; Note: rtype is already a list, via last, so use this to good advantage:
            (setf (gethash rid rels) (list target rtype))))
        (error "No Excel relationships found or invalid URL namespace."))
    rels))

#|
.. cl:function cl-excel:process-workbook

Extract basic information from the `xl/workbook.xml` document. This looks for a "workbook" XML element,
and parses that element's "definedNames", "workbookPr" and "sheet" children.

:param book: The workbook
:param workbook-content: The input ZIP octet stream to be parsed.
:param sheet-rels: Previously collect worksheet relationships.
|#
(defun process-workbook (book workbook-content sheet-rels)
  (declare (type workbook book)
           (type (vector (unsigned-byte 8)) workbook-content))
  (workbook-emit-debug book +xl-debug-phases+ "=== Workbook ===~%")
  (let ((workbook-xml (cxml:parse workbook-content (stp:make-builder))))
    (dolist (elt (cxml-stp:filter-children is-workbook-elt workbook-xml))
      (dolist (wb-elt (cxml-stp:filter-children (lambda (n) (or (funcall is-defined-names-elt n)
                                                                (funcall is-workbookPr-elt n)
                                                                (funcall is-sheets-elt n)))
                                                elt))
        (let ((wbelt-name (cxml-stp:local-name wb-elt)))
          (workbook-emit-debug book +xl-debug-xml+ " wb-elt = ~:_~:W~%" wb-elt)
          (workbook-emit-debug book +xl-debug-xml+ "wbelt-name: ~S~%" wbelt-name)

          ;; FIXME? Save all metadata in the workbook children and not just these three?
          (cond ((string= "definedNames" wbelt-name)
                 (workbook-emit-debug book +xl-debug-phases+ "--- defined names ---~%")
                 (dolist (defn (cxml-stp:filter-children (cxml-stp:of-name "definedName"
                                                                           (cxml-stp:namespace-uri wb-elt))
                                                         wb-elt))
                   (let ((name (make-instance 'name-object :workbook book))
                         (name-data (element-collect-text-string defn))
                         (preserve-spaces (cxml-stp:find-attribute-named defn "space" URI_XMLSPACE)))
                     (setf (workbook-name-objects book) name)
                     (setf (name-obj-index name) (fill-pointer (workbook-name-objects book)))
                     (dolist (defined-name xl-defined-name-map)
                       (destructuring-bind (attr slot convf) defined-name
                         (when slot
                           (let* ((attrval (cxml-stp:attribute-value defn attr))
                                  (cvtval (if (and convf attrval)
                                              (funcall convf attrval)
                                              attrval)))
                             (workbook-emit-debug book +xl-debug-xml+ "~S: attrval = ~S cvtval = ~S~%"
                                                  attr attrval cvtval)
                             (if cvtval
                               (setf (slot-value name slot) cvtval))))))
                     ;; Store the text or formula
                     (setf (name-formula-text name) (uncook-text name-data preserve-spaces))
                     (workbook-emit-debug book +xl-debug-data+ "~S~%" name))))

                ((string= "workbookPr" wbelt-name)
                 (progn
                   (workbook-emit-debug book +xl-debug-phases+ "--- workbookPr ---~%")
                   (let* ((dateattr (cxml-stp:attribute-value wb-elt "date1904"))
                          (datemode (convert-xml-boolean dateattr)))
                     (workbook-emit-debug book +xl-debug-data+ "datemode = ~S, attr = ~S~%" datemode dateattr)
                     (setf (workbook-datemode book) datemode))))

                ((string= "sheets" wbelt-name)
                 (workbook-emit-debug book +xl-debug-phases+ "--- sheets ---~%")
                 (dolist (sheet (cxml-stp:filter-children is-sheet-elt wb-elt))
                   (workbook-emit-debug book +xl-debug-xml+ "~:_~:W~%" wb-elt)
                   (let* ((rid (cxml-stp:attribute-value sheet "id" URI_ODREL))
                          (sheet-id (parse-integer (cxml-stp:attribute-value sheet "sheetId")))
                          (sheet-name (unescape-utf16 (cxml-stp:attribute-value sheet "name")))
                          (sheet-state (convert-sheet-visibility (cxml-stp:attribute-value sheet "state"))))
                     (declare (ignore sheet-state))
                     (workbook-emit-debug book +xl-debug-data+
                                          "rid = ~A, sheet-id = ~A, sheet-name = ~A~%"
                                          rid sheet-id sheet-name)
                     (multiple-value-bind (rel-data rel-exists) (gethash rid sheet-rels)
                       (if (and rel-data rel-exists)
                         (let ((rel-target (car rel-data))
                               (rel-type (cadr rel-data)))
                           (declare (ignore rel-target))
                           (if (string= rel-type "worksheet")
                             (multiple-value-bind (sheet index) (workbook-new-worksheet book nil sheet-name
                                                                                        :rel-id rid)
                               (setf (worksheet-number sheet) index)
                               (setf (worksheet-utter-max-rows sheet) +xml-utter-max-rows+)
                               (setf (worksheet-utter-max-cols sheet) +xml-utter-max-cols+))
                             (workbook-emit-debug book +xl-debug-data+
                                                  "Ignoring sheet type ~A~%" rel-type))))))))))))))

#|
.. cl:function:: process-coreprops

Extract properties from the core properties XML document, storing them in the workbooks
``props`` slot's hash table.

:param book: The workbook.
:param coreprops-content: The core properties XML document.
|#
(defun process-coreprops (book coreprops-content)
  ;; Sanity checking...
  (check-type book workbook)
  (check-type coreprops-content (vector (unsigned-byte 8)))

  (workbook-emit-debug book +xl-debug-phases+ "=== coreProps ===~%")
  (let ((coreprops-xml (cxml:parse coreprops-content (cxml-stp:make-builder)))
        (props-hash (workbook-props book)))
    (dolist (prop xl-core-props)
      (destructuring-bind (attr attr-ns cnv-func)
                          prop
        (let ((elt (cxml-stp:find-recursively-if (cxml-stp:of-name attr attr-ns) coreprops-xml)))
          (if (not (null elt))
            (setf (gethash attr props-hash)
                  (list attr-ns (funcall cnv-func (element-collect-text-string elt))))))))
    (setf (workbook-user-name book)
          (or (second (gethash "lastModifiedBy" props-hash))
              (second (gethash "creator" props-hash)))))
  (workbook-emit-debug book +xl-debug-data+ "workbook-user-name: ~A~%" (workbook-user-name book))
  book)

#|
.. cl:function:: process-styles
|#
(defun process-styles (book styles-content)
  (check-type book workbook)
  (check-type styles-content (vector (unsigned-byte 8)))

  (workbook-emit-debug book +xl-debug-phases+ "=== styles ===~%")
  (let* ((styles-document (cxml:parse styles-content (cxml-stp:make-builder)))
         (style-sheet-elem (cxml-stp:find-child-if is-stylesheet-elt styles-document))
         (is-cellStyleXfs (cxml-stp:of-name "cellStyleXfs" URI_SSML12))
         (is-cellXfs (cxml-stp:of-name "cellXfs" URI_SSML12))
         (is-numFmts (cxml-stp:of-name "numFmts" URI_SSML12))
         ;; known-date-formats is pretty magic. It's also very elastic in size, so use a hash table
         ;; instead of a more efficient array/vector representation
         (known-date-formats (make-hash-table :size 256)))
    ;; First element in the XF-to-XL type map is always a numeric format.
    (setf (gethash 0 (workbook-xf-index->xl-type book)) nil)

    ;; Mark the elements in known-date-formats that are date formats...
    (map nil (lambda (idx)
               (setf (gethash idx known-date-formats) t))
         (append (alexandria:iota 10 :start 14) (alexandria:iota 4 :start 45)))

    (if style-sheet-elem
        ;; Follow the document structure: <styleSheet>...</styleSheet>
        (dolist (elt (cxml-stp:filter-children (lambda (n) (or (funcall is-cellStyleXfs n)
                                                               (funcall is-cellXfs n)
                                                               (funcall is-numFmts n)))
                                               style-sheet-elem))
          (let ((elt-name (cxml-stp:local-name elt))
                (elt-namespace (cxml-stp:namespace-uri elt)))
            (cond
              #| Ignore cellStyleXfs for now. The Python code does, for the time being.
              ((string= elt-name "cellStyleXfs")
               nil) |#

              ((string= elt-name "cellXfs")
               ;; <cellXFs><xf>...</xf><xf>...</xf></cellXfs>
               (dolist (xf-elt (cxml-stp:filter-children (cxml-stp:of-name "xf" elt-namespace) elt))
                 (let* ((num-fmt-id (cxml-stp:attribute-value xf-elt "numFmtId") )
                        (fmt-idx (if num-fmt-id
                                   (multiple-value-bind (val pos) (parse-integer num-fmt-id :junk-allowed t)
                                     (if (eql pos (length num-fmt-id)) val 0))
                                   ;; Assume that the format is numeric (i.e., not a date) there isn't an
                                   ;; associated numeric format id.
                                   0))
                        (xf (make-instance 'extended-format :format-key fmt-idx))
                        (is-date (gethash fmt-idx known-date-formats) )
                        (xf-fmt-idx (length (workbook-xf-list book))))
                   (vector-push-extend xf (workbook-xf-list book))
                   ;; Update the extended formatting-to-guessed-cell-type mapping:
                   (setf (gethash xf-fmt-idx (workbook-xf-index->xl-type book))
                         (if is-date XL-CELL-DATE XL-CELL-NUMBER))

                   (workbook-emit-debug book +xl-debug-data+
                                        "xf-fmt-idx=~4D, fmt-idx=~4D (length workbook-xf-list) = ~4D~%"
                                        xf-fmt-idx fmt-idx (length (workbook-xf-list book)))))
               (dump-workbook-xf-index->xl-type book))

              ((string= elt-name "numFmts")
               ;; <numFmts><numFmt>...</numFmt><numFmt>...</numFmt></numFmts>
               (workbook-emit-debug book +xl-debug-xml+ "numFmts elt: ~:W~%" elt)
               (dolist (fmt (cxml-stp:filter-children (cxml-stp:of-name "numFmt" elt-namespace) elt))
                 (let* ((format-code  (cxml-stp:attribute-value fmt "formatCode"))
                        (fmt-idx (parse-integer (cxml-stp:attribute-value fmt "numFmtId") :junk-allowed t))
                        (is-date (is-date-format-string book format-code)))
                   (workbook-emit-debug book +xl-debug-xml+ "format: ~:W~%" fmt)
                   ;; Keep track of whether the format string looks like a date...??
                   (setf (gethash fmt-idx known-date-formats) is-date)
                   ;; Stash in the book's format-map. Whether the format is a date or numeric seems a bit
                   ;; squirrely here, but that's how the Python code seems to do it.
                   (setf (gethash fmt-idx (workbook-format-map book))
                         (make-instance 'xl-format-data
                                        :format-key fmt-idx
                                        :fmttype-class (if is-date FMT-FDT FMT-FNU)
                                        :format-str fmt))
                   (workbook-emit-debug book +xl-debug-data+
                                        "fmt-idx: ~A, format-code: ~A~%"
                                        fmt-idx format-code))))))))))

#|
.. cl:function:: load-shared-strings

Load the spreadsheet's strings from the "xl/sharedstrings.xml" string container. This function
follows the document structure to find and extract "<t>" and "<r>" elements where text is stored.
The "<r>" elements represent segmented text with embedded formatting, e.g., Arial text with
embedded Courier. The embedded formatting is currently discarded.

:param book: The workbook into which the shared strings are stored.
:param strings-content: The XML octet stream to read.
|#
(defun load-shared-strings (book strings-content)
  (check-type book workbook)
  (check-type strings-content (vector (unsigned-byte 8)))

  (workbook-emit-debug book +xl-debug-phases+ "=== shared strings ===~%")
  (let ((string-table (make-array 64 :adjustable t :fill-pointer 0 :element-type 'string)))
    (dolist (sst-elem (cxml-stp:filter-children (cxml-stp:of-name "sst" URI_SSML12)
                                                (cxml:parse strings-content (cxml-stp:make-builder))))
      (dolist (si-elem (cxml-stp:filter-children (cxml-stp:of-name "si" URI_SSML12) sst-elem))
        (let* ((si-children (cxml-stp:filter-children (lambda (n) (or (funcall is-t-node n)
                                                                      (funcall is-r-node n)))
                                                      si-elem)))
          ;; FIXME: Could collect the segmented string here.
          (vector-push-extend (uiop:reduce/strcat si-children :key #'collect-segmented-string) string-table))))

    (setf (workbook-shared-strings book) string-table))

  (when (debug-threshold (workbook-verbose book) +xl-dump-shared-strings+)
    (let ((log-file (workbook-verbose-log book))
          (shared-strings (workbook-shared-strings book)))
      (do ((offset 0 (1+ offset)))
          ((>= offset (length shared-strings)))
        (format log-file "~4,'0D: ~S~%" offset (aref shared-strings offset))))))

#|
.. cl-function:: load-worksheets

Iterate through a workbook's per-worksheet-info container and load each worksheet's data and associated
comments (if any.)
|#
(defun load-worksheets (book components rels)
  (check-type book workbook)
  (check-type components hash-table)
  (check-type rels hash-table)

  (workbook-emit-debug book +xl-debug-phases+ "==== Loading worksheets ====~%")
  (let ((calc-sheets (workbook-calc-sheets book)))
    (do ((sheet-idx 0 (1+ sheet-idx))
         (nsheets (length calc-sheets)))
        ((>= sheet-idx nsheets))
      (with-accessors ((sheet sheet-info-sheet)
                       (sheet-name sheet-info-name)
                       (rel-id sheet-info-rel-id))
                      (aref calc-sheets sheet-idx)
        ;; Must have a relationship id:
        (when (null rel-id)
          (error "load-worksheets: Relationship ID for worksheet ~S is NIL.~%" sheet-name))

        (let* ((rel-data (multiple-value-bind (rel-data rel-exists) (gethash rel-id rels)
                           (if (not rel-exists)
                             (error "load-worksheets: Relationship ~S not present in relationships hash table."
                                    rel-id)
                             rel-data)))
               (sheet-fname (car rel-data)))
          (workbook-emit-debug book +xl-debug-phases+
                               "[~4,'.D] loading ~S, rel-id ~S, fname ~S~%" sheet-idx sheet-name rel-id sheet-fname)
          (multiple-value-bind (sheet-component sheet-exists) (gethash sheet-fname components)
            (if sheet-exists
              (progn
                ;; Now we have the sheet's contents...
                (load-sheet-contents book sheet sheet-name sheet-fname (zip:zipfile-entry-contents sheet-component))
                (let* ((ws-str "worksheets/sheet")
                       (ws-idx (search ws-str sheet-fname))
                       (comment-fname (if ws-idx (uiop:strcat (subseq sheet-fname 0 ws-idx)
                                                              "comments"
                                                              (subseq sheet-fname (+ ws-idx (length ws-str)))))))
                  (if comment-fname
                    (multiple-value-bind (comment-component comment-exists) (gethash comment-fname components)
                      (if comment-exists
                        (let ((comment-contents (zip:zipfile-entry-contents comment-component)))
                          (load-sheet-comments book sheet sheet-name comment-fname comment-contents))
                        (workbook-emit-debug book +xl-debug-validate+ "No comments loaded (~S)~%" comment-fname)))))
                (worksheet-validate-dimensions sheet)
                (worksheet-emit-debug sheet +xl-debug-validate+ "Worksheet ~:W~%" sheet))
              (error "load-worksheets: Unable to locate ~S content (rel-id ~S, fname ~S)"
                     sheet-name rel-id sheet-fname))))))))

#|
.. cl-function: load-sheet-contents

Loads a single worksheet's contents into a vector of ``sheet-cell`` vectors.
|#
(defun load-sheet-contents (book sheet sheet-name sheet-fname contents)
  (check-type book workbook)
  (check-type sheet worksheet)
  (check-type sheet-name string)
  (check-type sheet-fname string)
  (check-type contents (vector (unsigned-byte 8)))

  (let* ((worksheet-doc (cxml:parse contents (cxml-stp:make-builder)))
         (worksheet-data (cxml-stp:find-child-if (cxml-stp:of-name "worksheet" URI_SSML12) worksheet-doc))
         ;; Row "r" attributes are optional; if missing, they are counted implicitly. If they are counted
         ;; implicitly, only emit the message once per worksheet
         (rowx -1)
         (implicit-row-flag nil)
         ;; The actual vector of vectors contents:
         (cell-tree (make-worksheet-cell-values)))
    ;; Document structure: <worksheet>...</worksheet>
    (if worksheet-data
        (let* ((sheetdata-filter (cxml-stp:of-name "sheetData" URI_SSML12))
               (dimension-filter (cxml-stp:of-name "dimension" URI_SSML12))
               (mergecells-filter (cxml-stp:of-name "mergeCells" URI_SSML12))
               (row-element-filter (cxml-stp:of-name "row" URI_SSML12))
               (mergecell-element-filter (cxml-stp:of-name "mergeCell" URI_SSML12)))
          (dolist (worksheet-elt (cxml-stp:filter-children (lambda (n) (or (funcall sheetdata-filter n)
                                                                           (funcall dimension-filter n)
                                                                           (funcall mergecells-filter n)))
                                                           worksheet-data))
            (let ((worksheet-elt-name (cxml-stp:local-name worksheet-elt)))
              ;; <worksheet><sheetData>...</sheetData></worksheet>
              ;; FIXME? Should we do anything with the "sheetViews" and "cols" elements?
              (cond ((string= worksheet-elt-name "sheetData")
                     (dolist (row-data (cxml-stp:filter-children row-element-filter worksheet-elt))
                       (let ((r-elt (cxml-stp:attribute-value row-data "r")))
                         (if r-elt
                           (setf rowx (multiple-value-bind (val pos) (parse-integer r-elt :junk-allowed t)
                                        (if (and (> pos 0)
                                                 (= pos (length r-elt)))
                                          val
                                          (error "Invalid \"r\" row attribute: ~S" r-elt))))
                           (progn
                             (when (not implicit-row-flag)
                               (warn "Warning: Implicitly numbered rows in worksheet.~%")
                               (setf implicit-row-flag t))
                             (incf rowx)))

                         (worksheet-emit-debug sheet +xl-debug-rows+ "<row> ~D~%" rowx)
                         (load-row-data book sheet sheet-name rowx cell-tree row-data))))

                    ((string= worksheet-elt-name "dimension")
                     (let* ((range-attr (cxml-stp:attribute-value worksheet-elt "ref"))
                            (range-sep-pos (if range-attr (position #\: range-attr) 0))
                            (range-end (if range-sep-pos (subseq range-attr (1+ range-sep-pos)) range-attr)))
                       (multiple-value-bind (end-row end-col worksheet-name endpos) (parse-cell-ref range-end)
                         (declare (ignore worksheet-name endpos))
                         (setf (worksheet-dimnrows sheet) (1+ end-row))
                         (setf (worksheet-dimncols sheet) (1+ end-col)))))

                    ((string= worksheet-elt-name "mergeCells")
                     (dolist (merge-cell (cxml-stp:filter-children mergecell-element-filter worksheet-elt))
                       (let* ((range-attr (cxml-stp:attribute-value merge-cell "ref"))
                              (range-sep-pos (if range-attr (position #\: range-attr) 0))
                              (range-start (if range-sep-pos (subseq range-attr 0 range-sep-pos)))
                              (range-end (if range-sep-pos (subseq range-attr (1+ range-sep-pos)))))
                         (multiple-value-bind (start-row start-col ign-worksheet ign-endpos) (parse-cell-ref range-start)
                           (declare (ignore ign-worksheet ign-endpos))
                           (multiple-value-bind (end-row end-col worksheet-name endpos) (parse-cell-ref range-end)
                             (declare (ignore worksheet-name endpos))
                             (worksheet-emit-debug sheet +xl-debug-data+
                                                   "Merged cells (~A): (~D, ~D) to (~D, ~D)~%"
                                                   range-attr start-row start-col end-row end-col)
                             (vector-push-extend (make-instance 'cell-range
                                                                :start-row start-row :start-col start-col
                                                                :end-row end-row :end-col end-col)
                                                 (worksheet-merged-cells sheet))))))))))

          ;; Stash the cell value quadtree into the sheet...
          (setf (worksheet-cell-values sheet) cell-tree))
        ;; 'else' forms:
        (error "Invalid worksheet structure, expecting \"worksheet\" element in ~S" sheet-fname))))

#|
.. cl-function:: load-sheet-comments

Load authors and comments associatd with a worksheet. This function gets invoked if there are
actual comments to load.
|#
(defun load-sheet-comments (book sheet sheet-name comment-fname comment-contents)
  (check-type book workbook)
  (check-type sheet worksheet)
  (check-type sheet-name string)
  (check-type comment-fname string)
  (check-type comment-contents (vector (unsigned-byte 8)))
  
  (let* ((comment-doc (cxml:parse comment-contents (cxml-stp:make-builder)))
         (comments-elt (cxml-stp:find-child-if (cxml-stp:of-name "comments" URI_SSML12) comment-doc)))
    (cond (comments-elt
            (let* ((author-elt (cxml-stp:find-child-if (cxml-stp:of-name "authors" URI_SSML12) comments-elt))
                   (collected-authors (loop :for author :in (cxml-stp:filter-children (cxml-stp:of-name "author" URI_SSML12)
                                                                                      author-elt)
                                            :collect (element-collect-text-string author)))
                   (comment-authors (make-array (length collected-authors) :initial-contents collected-authors)))
              (setf (worksheet-note-authors sheet) comment-authors)
              (when (debug-threshold (worksheet-verbose sheet) +xl-debug-data+)
                (format (worksheet-verbose-log sheet) "Author vector:~%")
                (dotimes (idx (length comment-authors))
                  (format (worksheet-verbose-log sheet) "[~4,'.D]: ~S~%" idx (aref comment-authors idx))))

              (let* ((comment-data (cxml-stp:find-child-if (cxml-stp:of-name "commentList" URI_SSML12) comments-elt))
                     (cell->note (worksheet-cell->note sheet)))
                (if comment-data
                  (dolist (comment (cxml-stp:filter-children (cxml-stp:of-name "comment" URI_SSML12) comment-data))
                    (let ((comment-ref (cxml-stp:attribute-value comment "ref"))
                          (comment-authorid (cxml-stp:attribute-value comment "authorId")))
                      (multiple-value-bind (row col _1 _2) (parse-cell-ref comment-ref)
                        (declare (ignore _1 _2))
                        (dolist (text-elt (cxml-stp:filter-children (cxml-stp:of-name "text" URI_SSML12) comment))
                          (let ((cmnt (uiop:reduce/strcat (all-r-t-nodes text-elt) :key #'collect-segmented-string)))
                            (multiple-value-bind (id-val pos) (parse-integer comment-authorid :junk-allowed t)
                              (when (< pos (length comment-authorid))
                                (format *error-output* "Badly formed authorId: ~S~%" comment-authorid))
                              (setf (gethash (list row col) cell->note)
                                    (make-instance 'cell-note :text cmnt :author-id id-val))))))))
                  (error "Invalid comment structure, expecting \"commentList\" element in ~S" comment-fname)))))
        (t (error "Invalid comment structure, expecting \"comments\" element in ~S" comment-fname))))

  (when (debug-threshold (worksheet-verbose sheet) +xl-debug-data+)
    (let ((n-notes (hash-table-count (worksheet-cell->note sheet)))
          (n-authors (length (worksheet-note-authors sheet))))
      (worksheet-emit-debug sheet +xl-debug-phases+ "Processed ~D comment~:P, ~D comment author~:P~%"
                            n-notes n-authors)
      (loop :for rowcol :being :the hash-keys :in (worksheet-cell->note sheet) :using (hash-value cmnt)
            :do (format (worksheet-verbose-log sheet)
                        "~6A [~4D] ~S~%" 
                        rowcol (cell-note-author-id cmnt) (cell-note-text cmnt))))))

(defun load-row-data (book sheet sheet-name rowx cell-tree row-data)
  (check-type book workbook)
  (check-type sheet worksheet)
  (check-type sheet-name string)
  (check-type rowx integer)
  (check-type cell-tree quadtree)
  (check-type row-data cxml-stp:element)

  (let* ((shared-strings (workbook-shared-strings book))
         (max-col most-negative-fixnum)
         (max-row (worksheet-nrows sheet)))
    (dolist (col-elt (cxml-stp:filter-children (cxml-stp:of-name "c" (cxml-stp:namespace-uri row-data)) row-data))
      (let ((r-elt (cxml-stp:attribute-value col-elt "r"))
            (xf-index (cxml-stp:attribute-value col-elt "s"))
            (colx -1)
            (cell-row -1)
            (implicit-col-flag nil)
            (cell-type (cxml-stp:attribute-value col-elt "t")))
        (if r-elt
          (multiple-value-bind (ridx cidx _ endpos) (parse-cell-ref r-elt)
            (declare (ignore _))

            (if (/= endpos (length r-elt))
              (warn "Warning: Partially parsed cell reference ~S~%" r-elt)
              (if (/= (1- rowx) ridx)
                (warn "Warning: Row indices do not match, ~A != ~A~%" ridx rowx)))
            (setf colx cidx)
            (setf cell-row ridx))
          (progn
            (if (not implicit-col-flag)
              (progn
                (warn "Warning: Implicitly numbered columns in worksheet.~%")
                (setf implicit-col-flag t)))
            (incf colx)
            (setf cell-row (1- rowx))))

        (setf max-row (max max-row (1+ cell-row)))
        (setf xf-index (if xf-index
                         (parse-integer xf-index)
                         ;; Extended formatting defaults to 0 (numeric format) if missing.
                         0))

        (labels ((float-val (s)
                   (handler-case (parse-number:parse-real-number s :float-format 'double-float)
                     (parse-number:invalid-number ()
                       (format *error-output* "Warning: (~D, ~D) invalid floating point number ~S, assuming 0.0~%"
                               cell-row colx s)
                       0.0)))
                 (parent-namespace (elt)
                   (cxml-stp:namespace-uri (cxml-stp:parent elt))))
          (let* ((col-elt-namespace (parent-namespace col-elt))
                 (f-elt (cxml-stp:find-child-if (cxml-stp:of-name "f" col-elt-namespace) col-elt))
                 (v-elt (cxml-stp:find-child-if (cxml-stp:of-name "v" col-elt-namespace) col-elt)))
            (workbook-emit-debug book +xl-debug-xml+ "col-elt = ~:W~%" col-elt)

            (cond ((or (string= cell-type "n")
                       (null cell-type))
                   ;; Numeric value
                   (if v-elt
                     (let ((v-val (float-val (element-collect-text-string v-elt))))
                       (workbook-emit-debug book +xl-debug-data+ "Col ~D N: v-val ~F~%"
                                            colx v-val)
                       (insert-cell-value cell-tree
                                          (make-sheet-cell :ctype (worksheet-cell-xf-index book v-elt xf-index)
                                                           :value (coerce v-val 'double-float)
                                                           :xf-index xf-index :row cell-row :col colx)))))

                  ((string= cell-type "s")
                   ;; Shared string index
                   (if v-elt
                     (let* ((s-idx (parse-integer (element-collect-text-string v-elt)))
                            (v-val (aref shared-strings s-idx)))
                       (workbook-emit-debug book +xl-debug-data+ "Col ~D S: v-val ~A (~S)~%"
                                            colx s-idx v-val)
                       (insert-cell-value cell-tree
                                          (make-sheet-cell :ctype XL-CELL-TEXT
                                                           :value v-val
                                                           :xf-index xf-index :row cell-row :col colx)))))

                  ((string= cell-type "str")
                   ;; Formula string result
                   (if f-elt
                     (let ((f-val (element-collect-text-string f-elt))
                           (v-val (if v-elt (element-collect-text-string v-elt :preserve-space t) "")))
                       (workbook-emit-debug book +xl-debug-data+
                                            "Col ~D STR: f-val ~S v-val ~S~%"
                                            colx f-val v-val)
                       (insert-cell-value cell-tree
                                          (make-sheet-cell :ctype XL-CELL-TEXT
                                                           :value v-val
                                                           :formula f-val
                                                           :xf-index xf-index :row cell-row :col colx)))))

                  ((string= cell-type "b")
                   ;; Boolean
                   (let ((v-val (if v-elt
                                  (parse-integer (element-collect-text-string v-elt))
                                  (progn
                                    (format *error-output* "(~D, ~D): Incorrect boolean (~A), assuming 'false'"
                                            cell-row colx (element-collect-text-string v-elt))
                                    nil))))
                     (workbook-emit-debug book +xl-debug-data+ "Col ~D B: v-val ~A~%"
                                          colx v-val)
                     (insert-cell-value cell-tree
                                        (make-sheet-cell :ctype XL-CELL-BOOLEAN
                                                         :value v-val
                                                         :formula nil
                                                         :xf-index xf-index :row cell-row :col colx))))
                  ((string= cell-type "e")
                   ;; Error data, <v> child contains "#REF!"
                   (let ((f-val (unescape-utf16 (element-collect-text-string f-elt)))
                         (v-val (element-collect-text-string v-elt)))
                     (workbook-emit-debug book +xl-debug-data+ "Col ~D E: f-val ~A v-val ~A~%"
                                          colx f-val v-val)
                     (insert-cell-value cell-tree (make-sheet-cell :ctype XL-CELL-ERROR
                                                                   :value (string-to-cell-error v-val)
                                                                   :formula f-val
                                                                   :xf-index xf-index :row cell-row :col colx))))

                  ((string= cell-type "inlineStr")
                   ;; Excel doesn't produce this, but 3rd party tools do... need to look for <is> tag:
                   (let* ((is-elt (cxml-stp:find-child-if (cxml-stp:of-name "is" URI_SSML12) col-elt))
                          (is-str (if is-elt (unescape-utf16 (element-collect-text-string is-elt)))))
                     (if is-str
                       (insert-cell-value cell-tree
                                          (make-sheet-cell :ctype XL-CELL-ERROR
                                                           :value is-str
                                                           :formula nil
                                                           :xf-index xf-index :row cell-row :col colx)))))

                  (t (format *error-output* "Unknown cell type ~A at row:~D col:~D~%" cell-type cell-row colx)))))
        (setf max-col (max max-col (1+ colx)))))

    (setf (worksheet-ncols sheet) max-col)
    (setf (worksheet-nrows sheet) max-row)

    #+(or) (when (debug-threshold (worksheet-verbose sheet) +xl-debug-data+)
             (let ((verbose-log (worksheet-verbose-log sheet)))
               nil))))

(defun is-r-or-t-node (n)
  "Predicate that matches either a 'r' or 't'node. Used by the segmented string code."
  (or (funcall is-r-node n)
      (funcall is-t-node n)))

(defun all-r-t-nodes (elt)
  (cxml-stp:filter-children #'is-r-or-t-node elt))

;; ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
;; Segmented string functions
;; (sorta.)
;; ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=

(defun collect-segmented-string (elem)
  "Collect a segmented string. This is a string that is either a single 't' node or
a 'r' node with a collection of 't' node children (the string segments). Currently,
nothing is really being done with the formatting segments, just the concatenated
string formed from the 't' nodes is collected."
  (labels ((collect-preserving-spaces (elt)
             (element-collect-text-string elt :preserve-space t)))
    (let* ((tag-name (cxml-stp:local-name elem)))
      (cond ((string-equal tag-name "t")
             (collect-preserving-spaces elem))
            ((string-equal tag-name "r")
             (uiop:reduce/strcat (cxml-stp:filter-children is-t-node elem) :key #'collect-preserving-spaces))))))
