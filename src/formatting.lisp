;; ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
;; Excel/Gnumeric/... formatter/formatting code
;; ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=

(in-package :xlmanip)

(declaim (ftype (function (workbook string) boolean) is-date-format-string))

(defparameter *xl-bad-date-guesses* t
  "Output warnings about date format strings that might be correct, but don't pass the
:cl:function:`xlmanip:is-date-format-string` test. Normally, this parameter is true (``t``).")

(eval-when (:compile-toplevel :load-toplevel)
  ;; Ensure that only this version of xl-non-date-formats exists:
  (if (boundp 'xl-non-date-formats)
      (makunbound 'xl-non-date-formats)))

(defvar xl-non-date-formats (make-hash-table :test #'equal :size 6))

;; Fill the hash table with general and numeric formats.
(dolist (fmt '("0.00E+00" "##0.0E+0" "General"
               "GENERAL"								;; OOo Calc 1.1.4 does this.
               "general"								;; pyExcelerator 0.6.3 does this.
               "@"))
  (setf (gethash fmt xl-non-date-formats) 1))

(defun is-date-format-string (book fmt)
  (declare (type workbook book)
           (type string fmt))
  (let ((state 0)
        (s (make-array 24 :element-type 'character :adjustable t :fill-pointer 0))
        ;; The return value
        (retval nil))
    ;; Filter the format string to remove escapes, bracketed expressions and some
    ;; miscellaneous format characters:
    (map nil (lambda (c)
               (cond ((eql state 0)
                      (cond ((eql c #\")
                             (setf state 1))

                            ((member c '(#\\ #\_ #\*))
                             (setf state 2))

                            ((eql c #\[)
                             (setf state 3))

                            ((not (member c '(#\$ #\- #\+ #\/ #\( #\) #\: #\Space)))
                             (vector-push-extend c s))))

					  ;; Skip over quoted things
                      ((eql state 1)
                       (if (eql c #\")
                       	   (setf state 0)))

                      ;; Skip backslashed, underscored or starred escapes
                      ((eql state 2)
                       (setf state 0))

                      ;; Skip over bracketed sequences
                      ((eql state 3)
                       (if (eql c #\])
                           (setf state 0)))

                      (t (error "is-date-format-string state error, c = ~S~%." c))))
         fmt)

    ;; Is the filtered format a known general or numeric format?
    (workbook-emit-debug book +xl-debug-date-formats+ "is-date-format-string: reduced format ~S~%" s)
    (multiple-value-bind (_ non-date) (gethash s xl-non-date-formats)
      (declare (ignore _))
      (if (not non-date)
          ;; Nope. Look at the characters in the filtered string and count
          ;; the number of date and numeric format characters
          (let ((date-count 0)
                (num-count 0)
                (got-sep nil))
            (map nil (lambda (c)
                       (if (member c '(#\y #\m #\d #\h #\s))
                           (incf date-count)
                           (if (member c '(#\0 #\# #\?))
                               (incf num-count)
                               (if (eql c #\;)
                                   (setf got-sep t)))))
                 s)
            (workbook-emit-debug book +xl-debug-date-formats+ "is-date-format-string: date-count ~A num-count ~A~%"
                                 date-count num-count)
            (if (and (> date-count 0)
                     (= 0 num-count))
                (setf retval t)
                (if (and (> num-count 0)
                         (= 0 date-count))
                    ;; Paranoia...
                    (setf retval nil)
                    (progn
                      (if (and *xl-bad-date-guesses*
                               (> date-count 0)
                               (> num-count 0))
                          (warn "Ambiguous date format: d=~D, n=~D fmt=~S"
                                date-count num-count fmt)
                          (if (and *xl-bad-date-guesses*
                                   (not got-sep))
                              (warn "Format string ~S produces constant result"fmt)))
                      (setf retval (> date-count num-count))))))))
    (workbook-emit-debug book +xl-debug-date-formats+ "is-date-format-string returns = ~A~%" retval)
    ;; Return the result
    retval))
