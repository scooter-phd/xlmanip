;; ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
;; Constants
;; ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=

(in-package :xlmanip)

;; Debugging levels
(defconstant +xl-debug-all+ t
  "Emit all possible debug output from both levels and specials. This is especially
verbose.")
(defconstant +xl-debug-level1+ 1)
(defconstant +xl-debug-phases+ 2
  "Output the successive reading phases while workbook is being read.")
(defconstant +xl-debug-validate+ 4
  "Emit messages related to data validation and sanity checking.")
(defconstant +xl-debug-data+ 6
  "Output converted data, intermediate objects.")
(defconstant +xl-debug-rows+ 7
  "Output worksheet row data while worksheet loads.")
(defconstant +xl-debug-xml+ 8
  "Output debugging related to XML document reading/writing..")
(defconstant +xl-debug-level9_ 9)

(eval-when (:compile-toplevel :load-toplevel)
  (defconstant +xl-debug-special-shift+ 5)
  (defun xl-make-special-debug (x)
    (declare (type integer x))
    (if (> x 0)
        (ash 1 (+ x (- +xl-debug-special-shift+ 1)))
        (error "xl-make-special-debug: Invalid shift value: ~D" x))))

(defconstant +xl-debug-non-special-mask+ (- (ash 1 +xl-debug-special-shift+) 1)
  "Maximum debugging level, before the specials.")

(defconstant +xl-dump-shared-strings+ #.(xl-make-special-debug 1)
             "(Special.) Shared string table debuggging messages.")
(defconstant +xl-debug-date-formats+ #.(xl-make-special-debug 2)
             "(Special.) Date format guessing.")
(defconstant +xl-debug-allocate+ #.(xl-make-special-debug 3)
             "(Special.) Row and cell allocation messages.")

;; Cell "types"
(defconstant XL-CELL-EMPTY 0
  "Empty cell indicator.")
(defconstant XL-CELL-TEXT 1
  "Text cell indicator.")
(defconstant XL-CELL-NUMBER 2
  "Numeric cell indicator.")
(defconstant XL-CELL-DATE 3
  "Date cell indicator.")
(defconstant XL-CELL-BOOLEAN 4
  "Boolean cell indicator.")
(defconstant XL-CELL-ERROR 5
  "Error cell (e.g. \"REF!\" errors).")
(defconstant XL-CELL-BLANK 6
  "Blank cell (i.e., it exists, but no text)")

;; Format constants:
(defconstant FMT-FUN 0
  "Unknown format")
(defconstant FMT-FDT 1
  "Date format")
(defconstant FMT-FNU 2
  "Numeric format")
(defconstant FMT-FGE 3
  "General format")
(defconstant FMT-FTX 4
  "Text format")

;; XML-based spreadsheet maximum row, column limits
(defconstant +xml-utter-max-rows+ (ash 1 20)
  "Maximum rows for XML-based spreadsheets.")
(defconstant +xml-utter-max-cols+ (ash 1 14)
  "Maximum columns for XML-based spreadhsheets.")

;; Date mode constants: Windows 1900-based dates or Mac 1904-based dates
(defconstant DATEMODE-WINDOWS nil)
(defconstant DATEMODE-MAC t)

;; XML name spaces
(eval-when (:compile-toplevel :load-toplevel)
  ;; Ensure that only these versions of the URI namespaces exist:
  (loop for urins in '(URI_SSML12 URI_ODREL URI_PKGREL URI_CP URI_DC URL_DCTERMS URI_XMLSPACE) do
        (if (boundp urins)
            (makunbound urins))))

(defvar URI_SSML12 "http://schemas.openxmlformats.org/spreadsheetml/2006/main")
(defvar URI_ODREL "http://schemas.openxmlformats.org/officeDocument/2006/relationships")
(defvar URI_PKGREL "http://schemas.openxmlformats.org/package/2006/relationships")
(defvar URI_CP "http://schemas.openxmlformats.org/package/2006/metadata/core-properties")
(defvar URI_DC "http://purl.org/dc/elements/1.1/")
(defvar URI_DCTERMS "http://purl.org/dc/terms/")
(defvar URI_XMLSPACE "http://www.w3.org/XML/1998/namespace")

;; Cell error codes
(defconstant CELL-ERR-NULL #x00)
(defconstant CELL-ERR-DIV0 #x07)
(defconstant CELL-ERR-VALUE #x0f)
(defconstant CELL-ERR-REF! #x17)
(defconstant CELL-ERR-NAME? #x1d)
(defconstant CELL-ERR-NUM! #x24)
(defconstant CELL-ERR-N/A #x2a)
