(in-package :xlmanip)

#|
~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
A cell note: some text and author reference mapped to a cell
row and column.
~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
|#

(defclass cell-note ()
  ((text :initform nil
         :initarg :text
         :accessor cell-note-text)
   (author-id :initform -1
              :initarg :author-id
              :type fixnum
              :accessor cell-note-author-id))
  (:documentation "A cell note: some text and an author reference mapped to a
cell reference ``(row, col)``"))
