(in-package :xlmanip)

#|
~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
eXtended Formatting information
~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
|#

(defclass extended-format ()
  ((is-style :initform t
             :accessor extended-format-is-style
             :documentation "``t`` indicates a cell XF, ``nil`` indicates a style XF.")
   
   (parent-style :initform 0
                 :accessor extended-format-parent-style
                 :documentation "If a cell XF, index into ``(workbook-xf-list)`` of this XF's
style XF. If a style XF, the value should be #xFFF.")

   (format-flag :initform 0
                :accessor extended-format-flag
                :documentation "None.")

   (alignment-flag :initform 0
                   :accessor extended-format-alignment-flag
                   :documentation "")

   (border-flag :initform 0
                :accessor extended-format-border-flag
                :documentation "")

   (background-flag :initform 0
                     :accessor extended-format-background-flag
                     :documentation "")

   (protection-flag :initform 0
                     :accessor extended-format-protection-flag
                     :documentation "")

   (xf-index :initform 0
             :accessor extended-format-xf-index
             :documentation "Index into ``(workbook-xf-list)``.")

   (font-index :initform 0
               :accessor extended-format-font-index
               :documentation "Index into ``(workbook-font-list)``")

   (format-key :initform 0
               :initarg :format-key
               :accessor extended-format-key
               :documentation "Key into ``(workbook-format-map)`` hash table.

Warning: OOo docs on the XF record call this \"Index to FORMAT record\".
It is not an index in the Python sense. It is a key to a map. It is true _only_
for Excel 4.0 and earlier files that the key into format-map from an XF instance
is the same as the index into format-list, and _only_ if the index is less than 164.")

   (protection :initform nil
               :accessor extended-format-protection
               :documentation "An instance of an XFProtection object.")

   (background :initform nil
               :accessor extended-format-background
               :documentation "An instance of an XFBackground object.")

   (alignment :initform nil
              :accessor extended-format-alignment
              :documentation "An instance of an XFAlignment object.")

   (border :initform nil
           :accessor extended-format-border
           :documentation "An instance of an XFBorder object."))
  (:documentation "eXtended Formatting information for cells, rows, columns and styles.

Each of the 6 flags below describes the validity of a specific group of attributes.

In cell XFs, flag==0 means the attributes of the parent style XF are used,
(but only if the attributes are valid there); flag==1 means the attributes
of this XF are used.

In style XFs, flag==0 means the attribute setting is valid; flag==1 means
the attribute should be ignored.

Note that the API provides both \"raw\" XFs and \"computed\" XFs -- in the latter
case, cell XFs have had the above inheritance mechanism applied."))
