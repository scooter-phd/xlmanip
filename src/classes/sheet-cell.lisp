(in-package :xlmanip)

#|
~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
Cell contents:
~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
|#

;; Probably argues for a refactoring into several derived classes.
(defstruct sheet-cell
  "A sheet cell has four attributes, in addition to its `(row, col)` position:
                   
- Cell type (ctype)
- Cell value: string or floating point number (value)
- Formula (formula)
- Extended formatting index (xf-index)

This structure's default constructor creates an empty cell, c.f., :cl:variable:`empty-cell`"
  (ctype XL-CELL-EMPTY :type fixnum)
  (value nil :type (or null string rational real boolean))
  (formula nil :type (or null string))
  (xf-index 0 :type fixnum)
  (row 0 :type fixnum)
  (col 0 :type fixnum))

(eval-when (:compile-toplevel :load-toplevel)
  ;; Ensure that only this version of empty-cell exists:
  (if (boundp 'empty-cell)
      (makunbound 'empty-cell)))

(defvar empty-cell (make-sheet-cell :ctype XL-CELL-EMPTY :xf-index 0)
  "The empty spreadsheet cell.")

(defmethod print-object ((object sheet-cell) stream)
  (with-accessors ((ctype sheet-cell-ctype)
                   (value sheet-cell-value)
                   (formula sheet-cell-formula)
                   (xf-index sheet-cell-xf-index)
                   (row sheet-cell-row)
                   (col sheet-cell-col))
                  object
    (print-unreadable-object (object stream :type t :identity t)
      (if (or (eql object empty-cell)
              (equal object empty-cell))
          (format stream "--empty cell--")
          (format stream "@(~D, ~D) ctype:~A value:~S formula:~S xf-index:~D"
                  row col (xl-cell-type ctype) value formula xf-index)))))

(defgeneric value-to-sheet-cell (v)
  (:documentation "Create a sheet cell from value. This is a generic wrapping function to convert
types into a appropriate sheet cell (text, number, boolean, etc). Feel free to define your
own data structure-specific value-to-sheet-cell conversions."))

(defmethod value-to-sheet-cell ((v string))
  (make-sheet-cell :ctype XL-CELL-TEXT
                   :value v))

(defmethod value-to-sheet-cell ((v real))
  (make-sheet-cell :ctype XL-CELL-NUMBER
                   :value v))

(defmethod value-to-sheet-cell ((v t))
  ;; Catch-all to create boolean values.
  (cond ((or (null v)
             (eq v t))
         (make-sheet-cell :ctype XL-CELL-BOOLEAN :value v))
        (t 
         (error "value-to-sheet-cell cannot convert ~S" v))))

(defmethod compare-child-objects ((o1 sheet-cell) (o2 sheet-cell))
  (compare-sheet-cell-position o1 o2))

(defun compare-sheet-cell-position (c1 c2)
  (with-accessors ((row-c1 sheet-cell-row)
                   (col-c1 sheet-cell-col)) c1
    (with-accessors ((row-c2 sheet-cell-row)
                     (col-c2 sheet-cell-col)) c2
      (or (< row-c1 row-c2)
          (and (= row-c1 row-c2)
               (< col-c1 col-c2))))))
