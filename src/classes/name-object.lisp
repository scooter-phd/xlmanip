(in-package :xlmanip)


#|
~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
Name objects
~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
|#
(defclass name-object ()
  ((workbook :initform nil
             :initarg :workbook
             :accessor name-workbook
             :documentation "Parent workbook")

   (obj-index :initform 0
              :type (integer 0 *)
              :accessor name-obj-index
              :documentation "The index of this object in the workbook's name-obj-list array.")

   (common-name :initform nil
                 :accessor name-common-name
                 :documentation "This object's common name, as a string. If builtin, decoded per the
  OpenOffice.org documentation.")

   (hidden :initform nil
           :type boolean
           :accessor name-hidden
           :documentation "Visbility: ``t`` is visible, ``nil`` is hidden")

   (funcmacro :initform nil
              :type boolean
              :accessor name-funcmacro
              :documentation "Function macro flag: ``t`` indicates a function macro,
    ``nil`` indicates a command macro.

  :type: Generalized boolean")

   (vbasic :initform nil
           :accessor name-vbasic
           :type boolean
           :documentation "VidualBasic macro flag, only relevant if macroflag is not nil.
  ``t`` indicates a VisualBasic macro, ``nil`` indicates a sheet macro.")

   (macroflag :initform nil
              :type boolean
              :accessor name-macroflag
              :documentation "Macro flag, controls the behavior of other settings, such as
  ``vbasic``. ``t`` indicates a macro name, ``nil`` indicates a standard name.")

    (complexformula :initform nil
                    :type boolean
                    :accessor name-complexformula
                    :documentation "Simple vs. complex formula flag (note: no examples of this
  have been sighted in the real world. ``t`` indicates a complex formula (array formula or
  user-defined.) ``nil`` indicates a simple formula.")

   (builtin :initform nil
            :accessor name-builtin
            :documentation "Builtin vs. user-defined name. Common examples of a builtin
name include Print-Area, Print-Titles. See the OpenOffice documentation for the complete
list. ``t`` indicates a builtin name, ``nil`` indicates a user-defined name.")

   (funcgroup :initform 0
              :type fixnum
              :accessor name-funcgroup
              :documentation "Function group, relevant only if macroflag is ``t``. OpenOffice
documentation has all of the legitimate values.")

   (binary :initform nil
           :type boolean
           :accessor name-binary
           :documentation "Binary data vs. formula definition. Note: No examples have been
  sighted. ``t`` indicates binary data, ``nil`` indicates forumla definition.")

   (raw-formula :initform nil
                :type (or null (vector (unsigned-byte 8)))
                :accessor name-raw-formula
                :documentation "Compiled bytecode formula (BIFF format only.)")

   (formula-text :initform ""
                 :type string
                 :accessor name-formula-text
                 :documentation "A string representing a formula or text.")

   (scope :initform -1
          :type fixnum
          :accessor name-scope
          :documentation "The name's scope within the workbook or spreadsheet.
  -1 -> The name is globally visible in all calculation sheets.
  -2 -> The name belongs to a macro sheet or VisualBasic/VBA sheet.
  -3 -> The name is invalid.
  0 <= n <= (workbook-nsheets book) -> The name is local to the particular sheet
  indexed by this scope.")

   (result :initform nil
           :accessor name-result
           :documentation "The result of evaluating the formula, if not ``nil``, and
it will be a single instance of the ``operand`` class. Otherwise, the result is ``nil``
if there isn't a formula or an error was encountered while evaluating it."))
  (:documentation "Container for named references, formulae, macros, etc.

:note: Name information is not extracted from files older than Excel 5.0,
   (book-version < 50)"))

(defmethod print-object ((object name-object) stream)
  (print-unreadable-object (object stream :type t :identity t)
    (format stream "~S ~S"
            (name-common-name object)
            (name-formula-text object))))
