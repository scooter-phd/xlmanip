(in-package :xlmanip)


#|
~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
The Workbook. 'Nuf said.
~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
|#

(defclass workbook ()
  ((verbose :initarg :verbose
            :initform nil
            :accessor workbook-verbose
            :type (or boolean fixnum)
            :documentation "Debug verbosity flag.")

   (verbose-log :initarg :verbose-log
                :initform *trace-output*
                :accessor workbook-verbose-log
                :documentation "Output stream for verbose mode messages. Defaults to
*trace-output*.")

  (datemode :initform DATEMODE-WINDOWS
            :accessor workbook-datemode
            :type boolean
            :documentation "Which date system was in force when this file was last saved.
0 => 1900 system (the Excel for Windows default).
1 => 1904 system (the Excel for Macintosh default).")

   (biff-version :initform 0
                 :accessor workbook-biff-version
                 :documentation "Version of BIFF (Binary Interchange File Format) used to create the file.
Latest is 8.0 (represented here as 80), introduced with Excel 97.
Earliest supported by this module: 2.0 (represented as 20).")

   (name-objects :initform (make-array 16 :adjustable t :fill-pointer 0)
                  :accessor workbook-name-objects
                  :documentation "An array of ``name-object`` objects for each NAME record in the workbook.")

   (codepage :initform nil :accessor workbook-codepage
             :documentation "An integer denoting the character set used for strings in this file.
For BIFF 8 and later, this will be 1200, meaning Unicode; more precisely, UTF-16-LE.
For earlier versions, this is used to derive the appropriate Python encoding
to be used to convert to Unicode.

Examples: 1252 -> 'cp1252', 10000 -> 'mac-roman'")

   (encoding :initform nil
             :accessor workbook-encoding
             :documentation "The encoding that was derived from the codepage.")

   (countries :initform nil
              :accessor workbook-countries
              :documentation "A tuple containing the (telephone system) country code for:
[0]: the user-interface setting when the file was created.<br />
[1]: the regional settings.<br />
Example: (1, 61) meaning (USA, Australia).

This information may give a clue to the correct encoding for an unknown codepage.
For a long list of observed values, refer to the OpenOffice.org documentation for
the COUNTRY record.")

   (user-name :initform ""
              :accessor workbook-user-name
              :documentation "What (if anything) is recorded as the name of the last user to save the file.")

   (props :initform (make-hash-table :test 'equal)
          :accessor workbook-props
          :documentation "XML core properties (docprops/core.xml)")

   (font-list :initform nil :accessor workbook-font-list
              :documentation "A list of Font class instances, each corresponding to a FONT record.")

   ;; The initial array size is arbitrary, but appears fairly reasonable, given the 163 predefined
   ;; formats in Excel.
   (xf-list :initform (make-array 256 :adjustable t :fill-pointer 0 :element-type 'extended-format)
            :accessor workbook-xf-list
            :documentation "A list of XF class instances, each corresponding to an XF record.")

   (format-list :initform nil :accessor workbook-format-list
                :documentation "A list of Format objects, each corresponding to a FORMAT record, in
the order that they appear in the input file. It does <i>not</i> contain builtin formats.
If you are creating an output file using (for example) pyExcelerator, use this list.
The collection to be used for all visual rendering purposes is format-map.")

   (format-map :initform (make-hash-table :test 'eql) :accessor workbook-format-map
               :documentation "The mapping from XF.format-key to Format object.")

   (style-name-map :initform (make-hash-table :test 'equal) :accessor workbook-style-name-map
                   :documentation "This provides access via name to the extended format information for
both built-in styles and user-defined styles.

It maps <i>name</i> to (<i>built-in</i>, <i>xf-index</i>), where:

<i>name</i> is either the name of a user-defined style,
or the name of one of the built-in styles. Known built-in names are
Normal, RowLevel-1 to RowLevel-7, ColLevel-1 to ColLevel-7,
Comma, Currency, Percent, 'Comma [0]', 'Currency [0]', Hyperlink,
and 'Followed Hyperlink'

<i>built-in</i> 1 :initform built-in style, 0 :initform user-defined
<i>xf-index</i> is an index into Book.xf-list.

References: OOo docs s6.99 (STYLE record); Excel UI Format/Style")

   (color-map :initform (make-hash-table :test 'equal) :accessor workbook-color-map
              :documentation "This provides definitions for colour indexes. Please refer to the
above section 'The Palette; Colour Indexes' for an explanation of how colours are represented
in Excel.

Colour indexes into the palette map into (red, green, blue) tuples. 'Magic' indexes,
e.g. 0x7FFF map to None. <i>colour-map</i> is what you need if you want to render cells on screen
or in a PDF file. If you are writing an output XLS file, use <i>palette-record</i>.")

   (palatte-record :initform (make-hash-table :test 'equal) :accessor workbook-pallate-record
                   :documentation "If the user has changed any of the colours in the standard palette, the XLS
file will contain a PALETTE record with 56 (16 for Excel 4.0 and earlier)
RGB values in it, and this list will be e.g. [(r0, b0, g0), ..., (r55, b55, g55)].
Otherwise this list will be empty. This is what you need if you are
writing an output XLS file. If you want to render cells on screen or in a PDF
file, use color-map.")

    (name-and-scope-map :initform (make-hash-table :test 'equal) :accessor workbook-name-and-scope-map
                        :documentation "A mapping from (lower-case-name, scope) to a single Name object.")

    (name-map :initform (make-hash-table :test 'equal) :accessor workbook-name-map
              :documentation "A mapping from lower-case-name to a list of Name objects. The list is
sorted in scope order. Typically there will be one item (of global scope)
in the list.")

   ;; All of the slots above are class slots in the Python code.
   ;; These slots show up in the Python --init-- method, so that makes me wonder if
   ;; there's a clever refactoring that needs to happen.

   #| calc-sheets replaced the sheet-list, sheet-names slots. Currently implemented as an array |#

   (calc-sheets :initform (make-array 8 :adjustable t
                                           :fill-pointer 0
                                           :element-type 'sheet-info)
                   :accessor workbook-calc-sheets
                   :documentation "This is the array that tracks per-sheet data: the sheet itself,
  the sheet's name")

   (all-sheets-map :initform '()
                   :accessor workbook-all-sheets-map
                   :documentation "maps an all-sheets index to a calc-sheets index (or -1)")

   (sheet-visibility :initform '() :accessor workbook-sheet-visibility
                     :type list
                     :documentation "Visibility attributes are derived from BOUNDSHEET record")

   (sh-abs-posn :initform '()
                :accessor workbook-sh-abs-posn
                :documentation "List of absolute positions in the stream for each sheet.
Used for on-demand loading.")

   (shared-strings :initform nil
                   :accessor workbook-shared-strings
                   :type (or null (vector string)))

   (rich-text-runlist-map :initform (make-hash-table :test 'equal)
                          :accessor workbook-rich-text-runlist-map)

   (raw-user-name :initform nil
                  :accessor workbook-raw-user-name
                  :type boolean)

   (sheethdr-count :initform 0 :accessor workbook-sheethdr-count
                   :type fixnum
                   :documentation "BIFF 4W only.")

   (builtinfmtcount :initform -1 :accessor workbook-builtinfmtcount
                    :type fixnum
                    :documentation "Unknown as yet. BIFF 3, 4S, 4W")

   (xfcount :initform 0 :accessor workbook-xfcount
            :type fixnum)

   (actual-fmt-count :initform 0 :accessor workbook-actual-fmt-count
                     :type fixnum)

   (xf-index->xl-type :initform (make-hash-table :test #'eql)
                            :accessor workbook-xf-index->xl-type
                            :documentation "Format-to-cell-type conversion hash table, indexed by the Excel
format code, e.g., FMT-FDT format for dates translates to CELL-TYPE-DATE.")

   (xf-epilogue-done :initform nil
                     :accessor workbook-xf-epilogue-done
                     :type boolean)

   (supbook-count :initform 0)

   (supbook-locals-inx :initform nil
                       :accessor workbook-supbook-locals-inx)

   (supbook-addins-inx :initform nil
                       :accessor workbook-supbook-addins-inx)

   (externsheet-info :initform '() :accessor workbook-externsheet-info)

   (externsheet-type-b57 :initform '() :accessor workbook-externsheet-type-b57)

   (extnsht-name-from-num :initform (make-hash-table :test 'equal)
                          :accessor workbook-extnsht-name-from-num)

   (sheet-num-from-name :initform (make-hash-table :test 'equal)
                        :accessor workbook-sheet-num-from-name)

   (extnsht-count :initform 0
                  :accessor workbook-extnsht-count)

   (supbook-types :initform '()
                  :accessor workbook-supbook-types)

   (resources-released :initform nil
                       :accessor workbook-resources-released
                       :type boolean)

   (addin-func-names :initform '()
                     :accessor workbook-addin-func-names))
  (:documentation "The basic spreadsheet workbook container.
                   
:keyword boolean verbose: Verbosity level; see :cl:function:`debug-level` for verbosity level info.
:keyword stream verbose-log: Stream to which verbose messages are logged. Defaults to *TRACE-OUTPUT*.

.. note:: You **never** instantiate a :cl:type:`workbook` object via MAKE-INSTANCE on your own. You let :cl:function:`xlread`
   instantiate, read and return the workbook."))


(proclaim '(inline workbook-emit-debug))

(defun workbook-emit-debug (book level &rest args)
  "Emit debugging output related to a workbook and its worksheets. This function passes its
arguments to COMMON-LISP:FORMAT if the requested debugging level is active.


:param book: The workbook
:param level: The requested debugging level, which is compared to the workbook's internal
  debugging level.
:param args: COMMON-LISP:FORMAT string and arguments.
  
Worksheets have a back pointer to their parent workbook; worksheets emit
debugging solely via their parent workbook's settings."
  (apply #'emit-debug (workbook-verbose book) (workbook-verbose-log book) level args))

(defun init-format-info (book)
  "Reinitialize formatting info, called for each sheet read."
  (with-accessors ((format-map workbook-format-map)
                   (format-list workbook-format-list)
                   (xfcount workbook-xfcount)
                   (actual-fmt-count workbook-actual-fmt-count)
                   (xf-index->xl-type workbook-xf-index->xl-type)
                   (xf-epilogue-done workbook-xf-epilogue-done)
                   (xf-list workbook-xf-list)
                   (font-list workbook-font-list))
                  book
    (setf format-map (make-hash-table :test 'equal))
    (setf format-list '())
    (setf xfcount 0)
    (setf actual-fmt-count 0)
    (setf xf-index->xl-type (make-hash-table :test 'equal))
    (setf (gethash 0 xf-index->xl-type) XL-CELL-NUMBER)
    (setf xf-epilogue-done nil)
    (setf xf-list (make-array 256 :adjustable t :fill-pointer 0 :element-type 'extended-format))
    (setf font-list '())))

;; NOTE: Must have "congruent lambda" list for the :after initialize-method:
(defmethod initialize-instance :after ((w workbook) &key verbose verbose-log)
  (declare (ignore verbose verbose-log))
  ;; Post-slot :initform, :initarg fixups that depend on slot values:
  (init-format-info w))

(defmethod (setf workbook-name-objects) (nobj (self workbook))
  "Push a new element onto the tail of the defined names/named objects array."
  (vector-push-extend nobj (workbook-name-objects self)))

(proclaim '(inline workbook-nsheets))
(defun workbook-nsheets (book)
  "The number of worksheets present in the workbook file.

:param workbook book: The workbook
:rtype: integer
:return: The number of worksheets in the workbook."
  (length (workbook-calc-sheets book)))

(defmethod print-object ((object workbook) stream)
  (print-unreadable-object (object stream :type t :identity t)
    (format stream "sheets:~D "
            (workbook-nsheets object))))

(defun dump-workbook-xf-index->xl-type (book)
  (with-accessors ((verbose workbook-verbose) 
                   (verbose-log workbook-verbose-log))
                  book
    (when (debug-threshold verbose +xl-debug-data+)
      (let* ((htab (workbook-xf-index->xl-type book)))
        (format verbose-log "(workbook-xf-index->xl-type book):~%")
        (loop for key in (sort (alexandria:hash-table-keys htab) #'<)
           do (format verbose-log "[~4,'0D] ~A~%" key (format-type-string (gethash key htab))))))))

(defgeneric get-worksheet (book name-or-index)
  (:documentation "Get a specific worksheet from a workbook.

:param book: The workbook to be interrogated.
:param name-or-index: The name of the worksheet or its offset into the worksheet array.
:returns: The worksheet corresponding to the name or index, if found, otherwise, ``nil``."))

(defmethod get-worksheet ((book workbook) (name string))
  "Get a worksheet from a workbook referenced by its name.

:param workbook book: The workbook to be interrogated.
:param string name: The worksheet's name.
:returns: The :cl:type:`worksheet` corresponding to the name, if found, otherwise, ``nil``."
  (let ((retval nil))
    (loop :for s :across (workbook-calc-sheets book)
          :do (when (string= name (sheet-info-name s))
                (setf retval (sheet-info-sheet s))
                (return)))
    retval))

(defmethod get-worksheet ((book workbook) (index integer))
  "Get a worksheet from a workbook referenced by its index into the workbook's
worksheet array.

:param workbook book: The workbook to be interrogated.
:param integer name: The worksheet's name.
:returns: The :cl:type:`worksheet` corresponding to the index, if found, otherwise, ``nil`` (i.e., if out
   range or negative.)

.. seealso:: :cl:function:`worksheet-nsheets`
   
Example usage::

    (let ((book (xlread #p\"some-spreadsheeet.xlsx\")))
      (dotimes (i (worksheet-nsheets book))
        (let ((sheet (get-worksheet book i)))
          #| Do lemon scented coolness with sheet here. |#)))"
  (let ((calc-sheets (workbook-calc-sheets book)))
    (if (<= 0 index (1- (length calc-sheets)))
      (sheet-info-sheet (aref calc-sheets index))
      nil)))
