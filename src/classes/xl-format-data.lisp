(in-package :xlmanip)

#|
Note: The structure of these classes follows the Python xlrd package (mostly). The
documentation for each slot adapted from the Python documentation.
|#

#|
~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
Format strings
~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
|#

(defclass xl-format-data ()
  ((format-key :initform 0
               :initarg :format-key
               :accessor xl-format-data-key
               :documentation "The key into the ``workbook-format-map`` hash table.")
   (fmttype-class :initform FMT-FUN
               :initarg :fmttype-class
               :accessor xl-format-fmttype-class
               :documentation "The format string's classification, which is only used
to distinguish between numeric and date formats. This will be one of ``FMT-FUN``,
``FMT-FDT``, ``FMT-FNU``, ``FMT-FGE``, or ``FMT-FTX``. See also ``is-date-format-string``.")
   (format-str :initform ""
               :initarg :format-str
               :accessor xl-format-data-str
               :documentation "The format string."))
  (:documentation ""))
