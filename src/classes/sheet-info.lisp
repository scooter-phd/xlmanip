(in-package :xlmanip)

#|
~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
Per-sheet metadata:
~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
|#

(defclass sheet-info ()
  ((sheet :initform nil
          :initarg :sheet
          :accessor sheet-info-sheet
          :documentation "The actual worksheet.")
   (name :initform ""
         :initarg :name
         :type string
         :accessor sheet-info-name
         :documentation "The name of the sheet within the workbook.")
   (rel-id :initform ""
           :initarg :rel-id
           :accessor sheet-info-rel-id
           :documentation "The relationship identifier for this worksheet."))
  (:documentation "Tracking structure for worksheets, their names, relationship identifiers and targets."))
