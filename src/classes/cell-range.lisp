(in-package :xlmanip)


#|
~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
A cell range. Common use cases include formulae and merged cells
~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
|#

(defclass cell-range ()
  ((start-row :initform 0
              :type fixnum
              :initarg :start-row
              :accessor cell-range-start-row)
   (start-col :initform 0
              :initarg :start-col
              :type fixnum
              :accessor cell-range-start-col)
   (end-row :initform 0
            :initarg :end-row
            :type fixnum
            :accessor cell-range-end-row)
   (end-col :initform 0
            :type fixnum
            :initarg :end-col
            :accessor cell-range-end-col))
  (:documentation "Container for a cell range. Use :cl:function:`make-instance` to create a cell range manually,
or :cl:function:`parse-cell-ref` to convert a string, e.g., 'B5:D1'.

The construtor handles the case when the range is reversed, 'D1:B5' by swapping the rows and columns in the
`initialize-instance :around` method.

:keyword start-row: The starting row for the cell range.
:keyword start-col: The starting column for the cell range.
:keyword end-row: The ending row for the cell range.
:keyword end-col: The ending column for the cell range.
"))

(defmethod initialize-instance :around ((thing cell-range) &rest initargs
                                        &key start-row start-col end-row end-col &allow-other-keys)
  (declare (ignore initargs))
  (check-type start-row fixnum)
  (check-type start-col fixnum)
  (check-type end-row fixnum)
  (check-type end-col fixnum)

  (if (< end-row start-row)
    (let ((x-row start-row)
          (x-col start-col))
      #-(or) (format *error-output* "initialize-instance cell-range: reversing start/end row, col~%")
      (setf start-row end-row)
      (setf start-col end-col)
      (setf end-row x-row)
      (setf end-col x-col))
    (if (and (= start-row end-row)
             (< end-col start-col))
      (let ((x-col start-col))
        #-(or) (format *error-output* "initialize-instance cell-range: reversing start/end col~%")
        (setf start-col end-col)
        (setf end-col x-col))))
  (call-next-method thing :start-row start-row :start-col start-col :end-row end-row :end-col end-col))

(defun cell-range-overlap (r1 r2)
  (check-type r1 cell-range)
  (check-type r2 cell-range)

  (labels ((compare-ranges (r1 r2)
             (let ((r1-start-row (the fixnum (cell-range-start-row r1)))
                   (r1-start-col (the fixnum (cell-range-start-col r1)))
                   (r1-end-row (the fixnum (cell-range-end-row r1)))
                   (r1-end-col (the fixnum (cell-range-end-col r1)))
                   (r2-start-row (the fixnum (cell-range-start-row r2)))
                   (r2-start-col (the fixnum (cell-range-start-col r2)))
                   (r2-end-row (the fixnum (cell-range-end-row r2)))
                   (r2-end-col (the fixnum (cell-range-end-col r2))))
               (labels ((inside-r2 (r c)
                          (and (>= r r2-start-row)
                               (>= c r2-start-col)
                               (<  r r2-end-row)
                               (<  c r2-end-col)))
                        (outside-r2 (r c)
                          (not (inside-r2 r c))))
                 (or
                  ;; r1 is completely contained in r2 (note: could write this as two calls to inside-r2, but that involves
                  ;; more comparisons than necessary.)
                  (and (>= r1-start-row r2-start-row)
                       (>= r1-start-col r2-start-col)
                       (<  r1-end-row   r2-end-row)
                       (<  r1-end-col   r2-end-col))
                  ;; r1 start outside of r2, r1 end inside of r2
                  (and (outside-r2 r1-start-row r1-start-col)
                       (inside-r2 r1-end-row r1-end-col))
                  ;; r1 start inside of r2, r1 end outside of r2
                  (and (inside-r2 r1-start-row r1-start-col)
                       (outside-r2 r1-end-row r1-end-col))
                  ;; r1 surrounds r2
                  (and (<= r1-start-row r2-start-row)
                       (>= r1-end-row   r2-end-row)
                       (or
                        ;; r1's start column is left of r2's end column
                        (<  r1-start-col r2-end-col)
                        ;; r1's end column is right of r2's start column
                        (>=  r1-end-col  r2-start-col))))))))
    (or (compare-ranges r1 r2)
        (compare-ranges r2 r1))))

(defmethod compare-child-objects ((o1 cell-range) (o2 cell-range))
  (compare-cell-ranges o1 o2))

(defun compare-cell-ranges (r1 r2)
  "cell-range sort predicate."
  (let ((r1-start-row (the fixnum (cell-range-start-row r1)))
        (r1-start-col (the fixnum (cell-range-start-col r1)))
        (r1-end-row   (the fixnum (cell-range-end-row r1)))
        (r1-end-col   (the fixnum (cell-range-end-col r1)))
        (r2-start-row (the fixnum (cell-range-start-row r2)))
        (r2-start-col (the fixnum (cell-range-start-col r2)))
        (r2-end-row   (the fixnum (cell-range-end-row r2)))
        (r2-end-col   (the fixnum (cell-range-end-col r2))))
      ;; Keep in mind that this orders the ranges, which are preferentially
      ;; ordered by start row, start col, end-row, end-col.
      (or (< r1-start-row r2-start-row)
          (and (= r1-start-row r2-start-row)
               (or (< r1-start-col r2-start-col)
                   (and (= r1-start-col r2-start-col)
                        (or (< r1-end-row r2-end-row)
                            (and (= r1-end-row r2-end-row)
                                 (< r1-end-col r2-end-col)))))))))

(defmethod print-object ((object cell-range) stream)
  (with-accessors ((start-row cell-range-start-row)
                   (start-col cell-range-start-col)
                   (end-row cell-range-end-row)
                   (end-col cell-range-end-col)) object
    (print-unreadable-object (object stream :type t :identity t)
      (format stream "(~D, ~D):(~D, ~D)"
              start-row start-col end-row end-col))))
