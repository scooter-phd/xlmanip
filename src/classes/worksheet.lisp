(in-package :xlmanip)

#|
~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
The worksheet container. This looks like it is deseparate need of refactoring.
~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
|#

(defclass worksheet ()
  ((book :initform nil
         :initarg :book
         :accessor worksheet-book
         :documentation "The parent workbook.")

   (biff-version :initform 0
                 :initarg :biff-version
                 :type fixnum
                 :accessor worksheet-biff-version
                 :documentation "For BIFF-based Excel workbooks, this is the file's BIFF version. Does not
apply to XML-based workbooks. (FIXME: Refactor?)")

   (read-position :initform nil
                  :initarg :read-position
                  :type (or null fixnum)
                  :accessor worksheet-read-position
                  :documentation "Current read position in the BIFF stream (?)")

   (bt :initform (make-array 16
                             :element-type '(unsigned-byte 8)
                             :adjustable t
                             :fill-pointer 1
                             :initial-element XL-CELL-EMPTY)
       :accessor worksheet-bt
       :documentation "FIXME...?")

   (bf :initform (make-array 16
                             :element-type '(signed-byte 8)
                             :adjustable t
                             :fill-pointer 1
                             :initial-element -1)
       :accessor worksheet-bf
       :documentation "FIXME...?")

   (name :initform nil
         :initarg :name
         :type (or null string)
         :accessor worksheet-name
         :documentation "")

   (number :initform -1
           :type fixnum
           :accessor worksheet-number
           :documentation "This sheet's index into the workbook's ``calc-sheets`` array.")

   (formatting-info :initform nil
                    :initarg :formatting-info
                    :accessor worksheet-formatting-info
                    :documentation "")

   (xf-index->xl-type :initform nil
                            :initarg :xf-index->xl-type
                            :accessor worksheet-xf-index->xl-type
                            :documentation "")

   (nrows :initform 0
          :type fixnum
          :accessor worksheet-nrows
          :documentation "Actual number of rows in this worksheet.")

   (ncols :initform 0
          :type fixnum
          :accessor worksheet-ncols
          :documentation "Actual number of columns in the worksheet.")

   (maxdatarowx :initform -1
                :type fixnum
                :accessor worksheet-maxdatarowx
                :documentation "Highest row index containing a non-empty cell")

   (maxdatacolx :initform -1
                :type fixnum
                :accessor worksheet-maxdatacolx
                :documentation "Highest col index containing a non-empty cell")

   (dimnrows :initform 0
             :type fixnum
             :accessor worksheet-dimnrows
             :documentation "Number of rows, according to a BIFF DIMENSIONS record.")

   (dimncols :initform 0
             :type fixnum
             :accessor worksheet-dimncols
             :documentation "Number of columns, according to a BIFF DIMENSIONS record.")

   ;; The following three 'cell-' arrays have to be adjusted/resized as needed to accomodate
   ;; the spreadsheet's data.

   (cell-values :initform nil
                :type (or null quadtree)
                :accessor worksheet-cell-values
                :documentation "The worksheet's cells. This is a quad tree of sheet-cells.")

   (defcolwidth :initform nil
                :accessor worksheet-defcolwidth
                :documentation "")

   (standardwidth :initform nil
                  :accessor worksheet-standardwidth
                  :documentation "")

   (default-row-height :initform nil
                       :accessor worksheet-default-row-height
                       :documentation "")

   (default-row-height-mismatch :initform 0
                                :accessor worksheet-default-row-height-mismatch
                                :documentation "")

   (default-row-hidden :initform 0
                       :accessor worksheet-default-row-hidden
                       :documentation "")

   (default-additional-space-above :initform 0
                                   :accessor worksheet-default-additional-space-above
                                   :documentation "")

   (default-additional-space-below :initform 0
                                   :accessor worksheet-default-additional-space-below
                                   :documentation "")

   (colinfo-map :initform (make-hash-table :test #'equal)
                :accessor worksheet-colinfo-map
                :documentation "")

   (rowinfo-map :initform (make-hash-table :test #'equal)
                :accessor worksheet-rowinfo-map
                :documentation "")

   (col-label-ranges :initform '()
                     :accessor worksheet-col-label-ranges
                     :documentation "")

   (row-label-ranges :initform '()
                     :accessor worksheet-row-label-ranges
                     :documentation "")

   (merged-cells :initform (make-array 16 :adjustable t :fill-pointer 0 :element-type '(or null cell-range) :initial-element nil)
                 :accessor worksheet-merged-cells
                 :documentation "Vector that keeps track of merged cell ranges.")

   (rich-text-runlist-map :initform (make-hash-table :test #'equal)
                          :accessor worksheet-rich-text-runlist-map
                          :documentation "")

   (horizontal-page-breaks :initform '()
                           :accessor worksheet-horizontal-page-breaks
                           :documentation "")

   (vertical-page-breaks :initform '()
                         :accessor worksheet-vertical-page-breaks
                         :documentation "")

   (xf-index-stats :initform #(0 0 0 0)
                   :accessor worksheet-xf-index-stats
                   :documentation "")

   (visibility :initform  0
               :initarg :visibility
               :type fixnum
               :accessor worksheet-visibility
               :documentation "Spreadsheet visibility from the BIFF BOUNDSHEET record.")

   ;; WINDOW2 options:

   (show-formulas :initform nil
                  :type boolean
                  :accessor worksheet-show-formulas
                  :documentation "")

   (show-grid-lines :initform t
                    :type boolean
                    :accessor worksheet-show-grid-lines
                    :documentation "")

   (show-sheet-headers :initform t
                       :type boolean
                       :accessor worksheet-show-sheet-headers
                       :documentation "")

   (panes-are-frozen :initform nil
                     :type boolean
                     :accessor worksheet-panes-are-frozen
                     :documentation "")

   (show-zero-values :initform t
                     :type boolean
                     :accessor worksheet-show-zero-values
                     :documentation "")

   (automatic-grid-line-colour :initform t
                               :type boolean
                               :accessor worksheet-automatic-grid-line-colour
                               :documentation "")

   (columns-from-right-to-left :initform nil
                               :type boolean
                               :accessor worksheet-columns-from-right-to-left
                               :documentation "")

   (show-outline-symbols :initform t
                         :type boolean
                         :accessor worksheet-show-outline-symbols
                         :documentation "")

   (remove-splits-if-pane-freeze-is-removed :initform nil
                                            :accessor worksheet-remove-splits-if-pane-freeze-is-removed
                                            :documentation "Multiple sheets can be selected, but only one can be active.
                                                           [Hold down Ctrl and click multiple tabs in the file in OOo.]")

   (sheet-visible :initform nil
                  :type boolean
                  :accessor worksheet-sheet-visible
                  :documentation "\"sheet-visible\" should really be called \"sheet-active\"
and is ``t`` when this sheet is the sheet displayed when the file is open. More than likely
only one sheet should ever be set as visible.

This would correspond to the Book's sheet-active attribute, but that doesn't exist as WINDOW1
records aren't currently processed. The real thing is the visibility attribute from the BOUNDSHEET record.")

   (show-in-page-break-preview :initform nil
                               :type boolean
                               :accessor worksheet-show-in-page-break-preview
                               :documentation "")

   (first-visible-rowx :initform 0
                       :type fixnum
                       :accessor worksheet-first-visible-rowx
                       :documentation "")

   (first-visible-colx :initform 0
                       :type fixnum
                       :accessor worksheet-first-visible-colx
                       :documentation "")

   (gridline-colour-index :initform #x40
                          :type fixnum
                          :accessor worksheet-gridline-colour-index
                          :documentation "")

   (gridline-colour-rgb :initform nil
                        :accessor worksheet-gridline-colour-rgb
                        :documentation "Prefer gridline-colour-index in BIFF8 spreadsheets and newer.")

   (hyperlink-list :initform nil
                   :accessor worksheet-hyperlink-list
                   :documentation "")

   (hyperlink-map :initform (make-hash-table :test #'equal)
                  :accessor worksheet-hyperlink-map
                  :documentation "")

   (cell->note :initform (make-hash-table :test #'equal)
               :accessor worksheet-cell->note
               :documentation "Mapping from cell references ``(row, col)`` and associated notes, authors.")

   (note-authors :initform nil
                 :accessor worksheet-note-authors
                 :documentation "Cell note author vector. Indices in this vector directly correspond to the
respective author identifier in a cell note.")

   (cooked-page-break-preview-mag-factor :initform 60
                                         :accessor worksheet-cooked-page-break-preview-mag-factor
                                         :documentation "Values calculated by xlrd to predict the mag factors that
will actually be used by Excel to display your worksheet. Pass these values to xlwt when writing XLS files.

Warning 1: Behaviour of OOo Calc and Gnumeric has been observed to differ from Excel's.

Warning 2: A value of zero means almost exactly what it says. Your sheet will be displayed as a very tiny
speck on the screen. xlwt will reject attempts to set a mag-factor that is not (10 <= mag-factor <= 400).")

   (cooked-normal-view-mag-factor :initform 100
                                  :accessor worksheet-cooked-normal-view-mag-factor
                                  :documentation "")

    ;; Values (if any) actually stored on the XLS file

   (cached-page-break-preview-mag-factor :initform nil
                                         :accessor worksheet-cached-page-break-preview-mag-factor
                                         :documentation "Value comes from WINDOW2 record.")

   (cached-normal-view-mag-factor :initform nil
                                  :accessor worksheet-cached-normal-view-mag-factor
                                  :documentation "")

   (scl-mag-factor :initform nil
                   :accessor worksheet-scl-mag-factor
                   :documentation "Value comes from the SCL record.")

   (ixfe :initform nil
         :accessor worksheet-ixfe
         :documentation "BIFF2 only.")

   (cell-attr-to-xfx :initform (make-hash-table :test #'equal)
                     :accessor worksheet-cell-attr-to-xfx
                     :documentation "")

#|
        #### Don't initialise this here, use class attribute initialisation.
        #### self.gcw = (0, ) * 256 ####
|#

    (utter-max-rows :initform 256
                   :type fixnum
                   :accessor worksheet-utter-max-rows
                   :documentation "")

   (utter-max-cols :initform 256
                   :type fixnum
                   :accessor worksheet-utter-max-cols
                   :documentation "")

   (first-full-rowx :initform -1
                    :accessor worksheet-first-full-rowx
                    :documentation "The first full (non-empty) row's index."))
  (:documentation "Contains the data for one worksheet.

In the cell access functions, `rowx` is a zero-based row index and `colx` is a zero-based
column index.

.. warning::
   You don't instantiate this class yourself. You access :cl:type:`worksheet` objects via the :cl:type:`workbook` object
   returned by :cl:function:`xlread`"))

(defun worksheet-emit-debug (sheet level &rest args)
  (let ((book (worksheet-book sheet)))
    (apply #'emit-debug (workbook-verbose book) (workbook-verbose-log book) level args)))

(defun worksheet-verbose (sheet)
  "Return verbosity level associated with this sheet's workbook."
  (workbook-verbose (worksheet-book sheet)))

(defun worksheet-verbose-log (sheet)
  "Return the verbose output stream associated with this sheet's workbook."
  (workbook-verbose-log (worksheet-book sheet)))

;; NOTE: Must have "congruent lambda" list for the :after initialize-method:
(defmethod initialize-instance :after ((w worksheet) &key book biff-version read-position name
                                                          formatting-info xf-index->xl-type
                                                          visibility)
  (declare (ignore book biff-version read-position name formatting-info xf-index->xl-type visibility))
  ;; Post-slot :initform, :initarg fixups that depend on slot values:
  (worksheet-emit-debug w +xl-debug-data+ "initialize-instance :after ((w worksheet)) invoked.~%")
  (setf (worksheet-utter-max-rows w) (if (>= (worksheet-biff-version w) 80)
                                             65536
                                             16384)))

(defun worksheet-cell-xf-index (book v xf-index)
  "Determine a cell's formatting type. If the value is null, return ``XL-CELL-EMPTY``,
since the value is an empty cell. Otherwise, look up the cell's extended formatting
metadata via ``xf-index``."
  (if v
      (let ((actual-xf (gethash xf-index (workbook-xf-index->xl-type book))))
        (if actual-xf
            actual-xf
            (progn
              (format *error-output* "Unaccounted/unknown XF index: ~D~%" xf-index)
              XL-CELL-TEXT)))
      XL-CELL-EMPTY))

(defun worksheet-put-cell (rowx colx cell-type val formula xf-index)
  (declare (ignore rowx colx cell-type val formula xf-index))
  (format t "worksheet-put-cell~%"))

(defmethod print-object ((object worksheet) stream)
  (with-accessors ((name worksheet-name)
                   (nrows worksheet-nrows)
                   (ncols worksheet-ncols))
                  object
    (print-unreadable-object (object stream :type t :identity t)
      (format stream "~A (~Dx~D)" name nrows ncols))))

;; ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
;; Methods that must be defined after classes exist:
;; ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=

(defun workbook-new-worksheet (book read-position name &key rel-id)
  "Append a new worksheet to a workbook, returning the sheet and its index in the workbook's
``calc-sheets`` array.

:param book: The target workbook
:param read-position: The stream read position from which the worksheet can be read; this
only applies to BIFF spreadsheets.
:param name: The worksheet's name.
:param rel-id: The relationship identifier between the worksheet and its associated XML
  file.

:rtype: (values sheet index)
:return: A ``worksheet`` instance and its index into the workbook's ``calc-sheets``
array."
  (with-accessors ((calc-sheets workbook-calc-sheets)) book
    (let* ((index (fill-pointer calc-sheets))
           (sheet (make-instance 'worksheet
                                 :book book
                                 :name name
                                 :biff-version (workbook-biff-version book)
                                 :read-position read-position
                                 :formatting-info (workbook-format-map book)
                                 :xf-index->xl-type (workbook-xf-index->xl-type book)
                                 :visibility (let ((viz (nth index (workbook-sheet-visibility book))))
                                               (if (null viz) 0 viz))))
           (this-sheet-info (make-instance 'sheet-info
                                           :sheet sheet
                                           :name name
                                           :rel-id rel-id)))
    (vector-push-extend this-sheet-info calc-sheets)
    (setf (worksheet-number sheet) index)
    (values sheet index))))

#|
Not really happy having code here that walks the cell-values quadtree. Might rethink if
there's a more generic way of doing this.
|#
(defun worksheet-extents (sheet)
  (labels ((find-extents (qtree min-row min-col max-row max-col)
             (with-accessors ((nw quadtree-nw)
                              (ne quadtree-ne)
                              (sw quadtree-sw)
                              (se quadtree-se)
                              (children quadtree-children))
                             qtree
               (cond ((quadtree-node-p qtree)
                      (multiple-value-bind (nw-min-row nw-min-col nw-max-row nw-max-col)
                                           (find-extents nw min-row min-col max-row max-col)
                        (multiple-value-bind (ne-min-row ne-min-col ne-max-row ne-max-col)
                                             (find-extents ne nw-min-row nw-min-col nw-max-row nw-max-col)
                          (multiple-value-bind (sw-min-row sw-min-col sw-max-row sw-max-col)
                                               (find-extents sw ne-min-row ne-min-col ne-max-row ne-max-col)
                            (find-extents se sw-min-row sw-min-col sw-max-row sw-max-col)))))

                     ((quadtree-leaf-p qtree)
                      (let ((all-rows (map 'vector #'sheet-cell-row children))
                            (all-cols (map 'vector #'sheet-cell-col children)))
                        (values
                          (reduce #'min all-rows :initial-value min-row)
                          (reduce #'min all-cols :initial-value min-col)
                          (reduce #'max all-rows :initial-value max-row)
                          (reduce #'max all-cols :initial-value max-col))))

                     (t (values min-row min-col max-row max-col)))))

           (iterate-subtrees (sheet-qt min-row min-col max-row max-col)
             (cond ((quadtree-node-p sheet-qt)
                    (with-accessors ((nw quadtree-nw)
                                     (ne quadtree-ne)
                                     (sw quadtree-sw)
                                     (se quadtree-se))
                                    sheet-qt
                      (multiple-value-bind (nw-min-row nw-min-col nw-max-row nw-max-col)
                                           (iterate-subtrees nw min-row min-col max-row max-col)
                        (multiple-value-bind (ne-min-row ne-min-col ne-max-row ne-max-col)
                                             (iterate-subtrees ne nw-min-row nw-min-col nw-max-row nw-max-col)
                          (multiple-value-bind (sw-min-row sw-min-col sw-max-row sw-max-col)
                                               (iterate-subtrees sw ne-min-row ne-min-col ne-max-row ne-max-col)
                            (iterate-subtrees se sw-min-row sw-min-col sw-max-row sw-max-col))))))

                   ((quadtree-leaf-p sheet-qt)
                    (with-accessors ((children quadtree-children))
                                    sheet-qt
                      (dotimes (i (length children))
                        (multiple-value-bind (row-1 col-1 row-2 col-2)
                                             (find-extents (aref children i) min-row min-col max-row max-col)
                          (setf min-row (min min-row row-1))
                          (setf min-col (min min-col col-1))
                          (setf max-row (max max-row row-2))
                          (setf max-col (max max-col col-2))))
                      (values min-row min-col max-row max-col)))

                   (t (values min-row min-col max-row max-col))))

           (merged-cell-extents (merged-cells max-row max-col)
             (let ((merged-row-max (reduce #'max (map 'vector #'cell-range-end-row merged-cells) :initial-value max-row))
                   (merged-col-max (reduce #'max (map 'vector #'cell-range-end-col merged-cells) :initial-value max-col)))
               (values merged-row-max merged-col-max))))

    (let ((cell-values (worksheet-cell-values sheet)))
      (if (not (quadtree-empty-p cell-values))
          (multiple-value-bind (min-row min-col max-row max-col)
                               (iterate-subtrees cell-values
                                                 most-positive-fixnum most-positive-fixnum
                                                 most-negative-fixnum most-negative-fixnum)
            (multiple-value-bind (merged-max-row merged-max-col)
                                 (merged-cell-extents (worksheet-merged-cells sheet) max-row max-col)
              (values min-row min-col merged-max-row merged-max-col)))
          ;; Something sensible for an empty sheet
          (values -1 -1 -1 -1)))))

(defun worksheet-validate-dimensions (sheet)
  "Verify the worksheet's dimensions"
  (with-accessors ((book worksheet-book)
                   (name worksheet-name)
                   (nrows worksheet-nrows)
                   (ncols worksheet-ncols)
                   (dimnrows worksheet-dimnrows)
                   (dimncols worksheet-dimncols)
                   (merged-cells worksheet-merged-cells)) sheet
    (let ((nr nrows)
          (nc ncols))
      ;; Gather up the actual dimensions of the sheet, with some sanity checking:
      (worksheet-emit-debug sheet +xl-debug-validate+ "Sheet ~A: nrows ~D, ncols ~D~%"
                            name nrows ncols)

      (multiple-value-bind (_1 _2 extent-rows extent-cols)
                           (worksheet-extents sheet)
        (declare (ignore _1 _2))
        (setf nr (1+ extent-rows))
        (setf nc (1+ extent-cols)))

      (dolist (comment-ref (alexandria:hash-table-keys (worksheet-cell->note sheet)))
        (setf nr (max nr (first comment-ref)))
        (setf nc (max nc (second comment-ref))))

      (worksheet-emit-debug sheet +xl-debug-validate+ "Sheet ~A: ~Dx~D dim, ~Dx~D counted~%"
                            name dimnrows dimncols nr nc)

      (when (or (/= nr dimnrows)
                (/= nc dimncols))
        (worksheet-emit-debug sheet +xl-debug-validate+ "Sheet ~A: Dimension mismatch ~Dx~D actual vs. ~Dx~D dim~%"
              name nr nc dimnrows dimncols))

      ;; Stash the results
      (setf nrows nr)
      (setf ncols nc))))

(defun collect-cell-value-subtrees (cell-values crange)
  "Query a :cl:type:`worksheet` for all leaves that overlap the cell range `crange`.

:param quadtree cell-values: The cell value quadtree (note: this is a quadtree of quadtrees.)
:param cell-range crange: The cell range to query.
:return a newly allocated vector of matching quadtree leaves whose children overlap the cell range."

  (check-type cell-values quadtree)
  (check-type crange cell-range)

  (let* ((retval (make-array 0 :fill-pointer 0 :adjustable t)))
    (labels ((inner-query (qtree)
               (cond ((null qtree)
                      retval)
                     ((quadtree-node-p qtree)
                      (inner-query (quadtree-sw qtree))
                      (inner-query (quadtree-se qtree))
                      (inner-query (quadtree-nw qtree))
                      (inner-query (quadtree-ne qtree)))
                     ((quadtree-leaf-p qtree)
                      (with-accessors ((children quadtree-children)) qtree
                        (dotimes (i (length children))
                          (let* ((child (aref children i))
                                 (boundary (quadtree-boundary child)))
                            (when (cell-range-overlap crange boundary)
                              (inner-cell-query child))))))
                     (t retval)))
             (inner-cell-query (qtree)
               (cond ((null qtree)
                      retval)
                     ((quadtree-node-p qtree)
                      (inner-cell-query (quadtree-sw qtree))
                      (inner-cell-query (quadtree-se qtree))
                      (inner-cell-query (quadtree-nw qtree))
                      (inner-cell-query (quadtree-ne qtree)))
                     ((quadtree-leaf-p qtree)
                      (let ((boundary (quadtree-boundary qtree)))
                        #+(or) (format *error-output* "-- consider ~A overlaps ~A~%"
                                       boundary crange)
                        (when (cell-range-overlap crange boundary)
                          #+(or) (format *error-output* ":: push ~A~%" boundary)
                          (vector-push-extend qtree retval))))
                     (t retval))))
      (inner-query cell-values))
    retval))

(defparameter *debug-column-iter* nil
  "Switch for debugging :cl:function:`iter-by-column`")

(defun iter-by-column (leaves crange)
  "docstring"
  (let* ((c-start (cell-range-start-col crange))
         (c c-start)
         (r-start (cell-range-start-row crange))
         (r-limit (cell-range-end-row crange))
         (leaf 0)
         (cursor -1)
         (r r-start))
    ;; :reset t -> Reset the iterator to its initial state
    ;; :empty-cells t -> Emit empty cells when the current (r, c) is not found. (TODO)
    (labels ((inner-iter-by-column (&key reset empty-cells)
               ;; Note: Using declare is dangerous, should be safe here:
               (declare (type fixnum leaf cursor))
               ;; Just for paranoia:
               (check-type leaf fixnum)
               (check-type cursor fixnum)

               (if (not reset)
                   ;; Normal iteration:
                   (cond ((> c (cell-range-end-col crange))
                          ;; Ran out of columns, signal iteration end.
                          nil)

                         ((or (>= leaf (length leaves))
                              (> r r-limit))
                          ;; Ran out of quadtree nodes or exceeded the row range?
                          (when *debug-column-iter*
                            (format *error-output* ":: ~A~%"
                                    (if (> r r-limit)
                                        "row limit reached"
                                        "leaves exhausted")))
                          (setf r r-start)
                          (setf c (1+ c))
                          (setf leaf 0)
                          (setf cursor -1)
                          (inner-iter-by-column))

                         ((or (>= r (cell-range-end-row (quadtree-boundary (aref leaves leaf))))
                              (>= c (cell-range-end-col (quadtree-boundary (aref leaves leaf)))))
                          ;; Hit the end of the leaf?
                          (when *debug-column-iter*
                            (format *error-output* ":: r > boundary~%"))
                          (setf leaf (1+ leaf))
                          (setf cursor -1)
                          (inner-iter-by-column))

                         (t
                          (when *debug-column-iter*
                            (format *error-output* ":: r ~3D c ~3D leaf ~3D cursor ~3D~%"
                                         r c leaf cursor))

                          ;; Binary search for the currently requested row/column
                          (let* ((leaf-sheet-cells (quadtree-children (aref leaves leaf)))
                                 (low (1+ cursor))
                                 (high (1- (length leaf-sheet-cells)))
                                 (found-cell nil))
                            (do* ((mid (ash (+ high low) -1)
                                       (ash (+ high low) -1)))
                                 ((or found-cell
                                      (> low high)))
                              (let* ((cell (aref leaf-sheet-cells mid))
                                     (cell-r (sheet-cell-row cell))
                                     (cell-c (sheet-cell-col cell)))
                                (when *debug-column-iter*
                                  (format *error-output* ":: low = ~3D mid = ~3D high = ~3D cell-r = ~D cell-c = ~D~%"
                                          low mid high cell-r cell-c))
                                (cond ((and (= r cell-r)
                                            (= c cell-c))
                                       (setf found-cell cell)
                                       ;; Minor optimization that keeps track of where we the last successful search ended,
                                       ;; making the next search less exhaustive.
                                       (setf cursor mid))
                                      ((or (< r cell-r)
                                           (and (= r cell-r)
                                                (< c cell-c)))
                                       (setf high (1- mid)))
                                      ((or (> r cell-r)
                                           (and (= r cell-r)
                                                (> c cell-c)))
                                       (setf low (1+ mid))))))
                            (setf r (1+ r))
                            (if found-cell
                                found-cell
                                (inner-iter-by-column)))))
                   (progn
                     ;; Reset the iterator back to its initial state:
                     (setf c c-start)
                     (setf r r-start)
                     (setf leaf 0)
                     (setf cursor -1)))))
      #'inner-iter-by-column)))

(defparameter *debug-row-iter* nil
  "Switch for debugging :cl:function:`iter-by-row`")

(defun iter-by-row (leaves crange)
  (let* ((c-start (cell-range-start-col crange))
         (c-limit (cell-range-end-col crange))
         (r-start (cell-range-start-row crange))
         (r-limit (cell-range-end-row crange))
         (leaf 0)
         (cursor -1)
         (r r-start))
    ;; :reset t -> Reset the iterator to its initial state
    ;; :empty-cells t -> Emit empty cells when the current (r, c) is not found. (TODO)
    (labels ((inner-iter-by-row (&key reset empty-cells)
               (declare (type fixnum leaf cursor))
               ;; Just for paranoia:
               (check-type leaf fixnum)
               (check-type cursor fixnum)

               (if (not reset)
                   ;; Normal iteration:
                   (cond ((> r r-limit)
                          ;; Ran out of rows, signal iteration end.
                          nil)

                         ((or (>= leaf (length leaves))
                              (<  r (cell-range-start-row (quadtree-boundary (aref leaves leaf)))))
                          ;; Ran out of quadtree nodes or exceeded the row range?
                          (when *debug-row-iter*
                            (format *error-output* "|| ~A~%"
                                    (if (>= leaf (length leaves))
                                        "leaves exhausted"
                                        "r > cell-range's start row")))
                          (setf r (1+ r))
                          ;; Don't rescan from the beginning of the leaves, just back up to the first leaf
                          ;; where the current row starts.
                          (setf leaf (do ((i (1- leaf)
                                             (1- i)))
                                         ((or (< i 0)
                                              (> r (cell-range-end-row (quadtree-boundary (aref leaves i)))))
                                          (1+ i))
                                       #| nothing |#))
                          (setf cursor -1)
                          (inner-iter-by-row))

                         ((>= r (cell-range-end-row (quadtree-boundary (aref leaves leaf))))
                          ;; This leaf precedes the requested row, skip ahead by a leaf
                          (when *debug-row-iter*
                            (format *error-output* "|| r >= boundary~%"))
                          (setf leaf (1+ leaf))
                          (setf cursor -1)
                          (inner-iter-by-row))

                         (t
                          (when *debug-row-iter*
                            (format *error-output* "|| iterating: r ~3D leaf ~3D cursor ~3D~%"
                                    r leaf cursor))
                          (let ((leaf-sheet-cells (quadtree-children (aref leaves leaf))))
                            (cond ((>= cursor 0)
                                   (cond ((< cursor (length leaf-sheet-cells))
                                          ;; Regular iteration: Advance the cursor, return the cell
                                          (let ((cell (aref leaf-sheet-cells cursor)))
                                            (cond ((and (= r (sheet-cell-row cell))
                                                        (<= c-start (sheet-cell-col cell) c-limit))
                                                   (setf cursor (1+ cursor))
                                                   cell)
                                                  (t
                                                   (when *debug-row-iter*
                                                     (format *error-output* "|| walked off range @(~3D, ~3D)~%"
                                                             (sheet-cell-row cell)
                                                             (sheet-cell-col cell)))
                                                   (setf cursor -1)
                                                   (setf leaf (1+ leaf))
                                                   (inner-iter-by-row)))))
                                         (t
                                          (setf cursor -1)
                                          (setf leaf (1+ leaf))
                                          (inner-iter-by-row))))
                                  (t
                                   ;; Find low end of the column range in leaf's child sheet-cells
                                   (let ((low 0)
                                         (high (1- (length leaf-sheet-cells)))
                                         (range-start -1))
                                     (do ((mid (ash (+ high low) -1)
                                               (ash (+ high low) -1)))
                                         ((or (> low high)
                                              (>= range-start 0))
                                          nil)
                                       (let* ((cell (aref leaf-sheet-cells mid))
                                              (cell-r (sheet-cell-row cell)))
                                         (when *debug-row-iter*
                                           (format *error-output* "|| l=~3D m=~3D h=~3D r=~3D cell-r=~3D~%"
                                                   low mid high r cell-r))
                                         (cond ((> r cell-r)
                                                (setf low (1+ mid)))
                                               ((< r cell-r)
                                                (setf high (1- mid)))
                                               ((= r cell-r)
                                                (if (or (= mid 0)
                                                        (> r (sheet-cell-row (aref leaf-sheet-cells (1- mid)))))
                                                    (setf range-start mid)
                                                    (setf high mid))))))
                                     (cond ((>= range-start 0)
                                            (setf cursor (1+ range-start))
                                            (aref leaf-sheet-cells range-start))
                                           (t
                                            (setf cursor -1)
                                            (setf leaf (1+ leaf))
                                            (inner-iter-by-row))))))))))))
      #'inner-iter-by-row)))

(defun make-cell-range-iterator (sheet crange &key (by-column))
  "Create an iterative function closure across a cell range. This is the basic workhorse for looping across collections of cells.
Looping in row-major order and column-major order are both supported.

:param sheet: The worksheet from which the returned iterator returns cells.
:param crange: The cell range over which the returned iterator operates.
:keyword by-column: Causes the returned iterator to iterate by column if :cl:variable:`t`, instead of by row (the default).

Example usage is::

   (let* ((book (xlmanip:xlread (asdf:system-relative-pathname :xlmanip (uiop:strcat \"tests/sheets/\" \"reveng1.xlsx\"))))
          (sheet (xlmanip:get-worksheet book \"ZZZfirstsheet\"))
          (iter (xlmanip:make-cell-range-iterator sheet
                                                  (make-instance 'xlmanip:cell-range
                                                                 :start-row 0 :start-col 0
                                                                 :end-row (xlmanip:worksheet-nrows sheet)
                                                                 :end-col (xlmanip:worksheet-ncols sheet))
                                                  :by-column t)))
     (do ((i (funcall iter)
             (funcall iter)))
         ((null i) 'completed)
       (format t \"[~3D, ~3D] ~A~%\"
               (xlmanip:sheet-cell-row i)
               (xlmanip:sheet-cell-col i)
               (xlmanip:sheet-cell-value i))))
"

  (check-type sheet worksheet)
  (check-type crange cell-range)

  (labels ((order-by-column (r1 r2)
             (let* ((r1-boundary (quadtree-boundary r1))
                    (r2-boundary (quadtree-boundary r2))
                    (r1-start-row (the fixnum (cell-range-start-row r1-boundary)))
                    (r1-start-col (the fixnum (cell-range-start-col r1-boundary)))
                    (r1-end-row   (the fixnum (cell-range-end-row r1-boundary)))
                    (r1-end-col   (the fixnum (cell-range-end-col r1-boundary)))
                    (r2-start-row (the fixnum (cell-range-start-row r2-boundary)))
                    (r2-start-col (the fixnum (cell-range-start-col r2-boundary)))
                    (r2-end-row   (the fixnum (cell-range-end-row r2-boundary)))
                    (r2-end-col   (the fixnum (cell-range-end-col r2-boundary))))
                 (or (< r1-start-col r2-start-col)
                     (and (= r1-start-col r2-start-col)
                          (or (< r1-start-row r2-start-row)
                              (and (= r1-start-row r2-start-row)
                                   (or (< r1-end-col r2-end-col)
                                       (and (= r1-end-col r2-end-col)
                                            (< r1-end-row r2-end-row))))))))))
    (let ((leaves (sort (collect-cell-value-subtrees (worksheet-cell-values sheet) crange)
                        ;; compare-child-objects method specializer orders by row/col (but could
                        ;; have a better name here.)
                        (if by-column #'order-by-column #'compare-child-objects))))
      #+(or) (format *error-output* "~:W~%" leaves)
      (if by-column
          (iter-by-column leaves crange)
          (iter-by-row leaves crange)))))

(defun worksheet-all-cells-range (sheet)
  "Generate a :cl:type:`cell-range` that encompasses the entire worksheet. This is suitable for calling
:cl:function:`make-cell-range-iterator` when working on the worksheet as an aggregate."
  (check-type sheet worksheet)
  (make-instance 'cell-range
                 :start-row 0 :start-col 0
                 :end-row (xlmanip:worksheet-nrows sheet)
                 :end-col (xlmanip:worksheet-ncols sheet)))

(defun worksheet-map-rows (sheet start-row end-row map-func)
  (let ((n-cols (worksheet-ncols sheet)))
    (labels ((make-row-array () (make-array n-cols :element-type 'sheet-cell :initial-element empty-cell)))
      (let ((accum-r (make-row-array))
            (iter-func (make-cell-range-iterator sheet
                                                 (make-instance 'cell-range
                                                                :start-row start-row :start-col 0
                                                                :end-row end-row :end-col n-cols)))
            (current-r -1)
            (max-c -1))
        (do ((iter (funcall iter-func)
                   (funcall iter-func)))
            ((null iter))
          (let ((current-c (sheet-cell-col iter)))
            (when (/= (sheet-cell-row iter) current-r)
              ;; New row started
              (when (>= current-r 0)
                ;; Trim down the row array, if neeeded
                (unless (>= (1+ max-c) n-cols)
                  (setf accum-r (adjust-array accum-r (1+ max-c))))
                ;; Process the row
                (funcall map-func current-r accum-r)
                ;; Renew the array
                (setf accum-r (make-row-array)))
              (setf current-r (sheet-cell-row iter)))
            ;; Continue to accumulate the row
            (setf (aref accum-r current-c) iter)
            (setf max-c (max max-c current-c))))
        ;; Final row:
        (unless (>= (1+ max-c) n-cols)
          (setf accum-r (adjust-array accum-r (1+ max-c))))
        (funcall map-func current-r accum-r)))))

(defmacro with-worksheet-rows (sheet start-row end-row (row-var row-vector &optional (return-val nil))
                               &body body)
  "Operate on individual worksheet rows. This macro creates an iterator that accumulates the contents of each row into a vector,
executing the body forms on the vector.

:note: The accumulation vector is initialized to :cl:variable:`empty-cell`, **not copies of** :cl:variable:`empty-cell`.
   Consequently, if you mutate or change the :cl:type:`sheet-cell` and it happens to be the :cl:variable:`empty-cell`, you
   will see strange results.

:param worksheet sheet: The worksheet
:param fixnum start-row: The starting row
:param fixnum end-row: The ending row
:param symbol row-var: The variable name in the body that is the current row number.
:param symbol row-vector: The variable name in the body that is assigned the accumulated row vector.
:param return-val: The value to return from this macro.

Example usage::

    (let* ((book (xlmanip:xlread (asdf:system-relative-pathname :xlmanip (uiop:strcat \"tests/sheets/\"
                                                                                      \"apachepoi_49609.xlsx\"))))
           (sheet (xlmanip:get-worksheet book 0)))
      (xlmanip:with-worksheet-rows sheet 0 5 (r accum-row)
        ;; You can use declarations here, since the body is wrapped within a *labels* function:
        (declare (ignore r))
        (format t \"~:W~%\" accum-row)))

This snippet outputs the first six rows in the first worksheet from the *apachepoi_49609.xlsx* workbook.
"
  `(progn
     (worksheet-map-rows ,sheet ,start-row ,end-row (lambda (,row-var ,row-vector) ,@body))
     ,return-val))

(defmacro with-one-worksheet-row (sheet row (row-var row-vector &optional retval) &body body)
  `(with-worksheet-rows ,sheet ,row ,row (,row-var ,row-vector ,retval) ,@body))

(defmacro with-all-worksheet-rows (sheet (row-var row-vector &optional retval) &body body)
  "Convenience macro that operates on all rows. See :cl:macro:`with-worksheet-rows`.

Example usage::

    (let* ((book (xlmanip:xlread (asdf:system-relative-pathname :xlmanip (uiop:strcat \"tests/sheets/\"
                                                                                      \"apachepoi_49609.xlsx\"))))
           (sheet (xlmanip:get-worksheet book 0)))
      (xlmanip:with-all-worksheet-rows sheet (r accum-row)
        (declare (ignore r))
        (format t \"~:W~%\" accum-row)))
"
  `(with-worksheet-rows ,sheet 0 (worksheet-nrows ,sheet) (,row-var ,row-vector ,retval) ,@body))
