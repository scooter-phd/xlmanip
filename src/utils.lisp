#|
~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
Utility functions that do not have any dependencies in other files. These
include:

- Debugging and logging
- UCS2/UTF-16 conversions
- Cell reference conversion (to and from)
- Pretty printing
~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
|#

(in-package :xlmanip)

(declaim (inline emit-debug debug-threshold
                 ensure-unicode
                 uncook-text
                 string-in-set))

(defun debug-threshold (verbose level)
  (declare (type (or integer boolean) verbose)
           (type integer level))
  (or (and (integerp verbose)
           (let ((verb-level (logand verbose +xl-debug-non-special-mask+))
                 (verb-special (logand verbose (lognot +xl-debug-non-special-mask+)))
                 (debug-level (logand level +xl-debug-non-special-mask+))
                 (debug-special (logand level (lognot +xl-debug-non-special-mask+))))
             ;; (format t "[v-l ~X d-l ~X v-s ~X d-s ~X[" verb-level debug-level verb-special debug-special)
             (or (and (> debug-level 0)
                      (>= verb-level debug-level))
                 (> (logand verb-special debug-special) 0))))
      (eq verbose t)))

(defun emit-debug (verbose log-file level &rest args)
  "Emit debug information when the debug level greater than or equal to the threshold level.
This is a wrapper around a call to ``cl:format``

:param verbose: Current verbosity (debug) level
:param log-file: Stream to which debug information is sent. This passed as the second argument
to ``cl:format``.
:param level: The debug threshold (see the ``+xl-debug-...+`` constants.)
:param args: The argument list passed to ``format``."

  (when (debug-threshold verbose level)
    (apply #'format (cons log-file args))))

(defun debug-level (&rest args)
  "Utility function to combine debug levels and specials from a variable length
argument list. The debug level is the highest debug level passed in the argument
list. Special debug levels are simply accumulated. For example::

    (import xlmanip:xlread)
    (xlread #P\"my_spreadsheet.xlsx\" :verbose (debug-level *xlmanip-debug-phases*
                                                          *xlmanip-dump-shared-string*))
"
  (let ((d-level 0)
        (d-specials 0))
    (dolist (arg args) (if (< arg +xl-debug-non-special-mask+)
                           (if (< d-level arg)
                               (setf d-level arg))
                           (setf d-specials (logior d-specials arg))))
    ;; (format t "mask ~X, level ~X, specials ~X~%" +xl-debug-non-special-mask+ d-level d-specials)
    (logior (logand d-level +xl-debug-non-special-mask+) 
            (logand d-specials (lognot +xl-debug-non-special-mask+)))))

(defun string-in-set (s s-set)
  (some (lambda (test-s) (string-equal s test-s)) s-set))

(defun convert-xml-boolean (s &key ((:default def-value) nil))
  "Convert XML boolean values to CL generalized boolean. No value returns the ``default`` (``nil``)."
  (cond ((string-in-set s '("1" "true" "on")) t)
        ((string-in-set s '("0" "false" "off")) nil)
        (t def-value)))

(defun convert-xml-unsigned-int (s)
  (multiple-value-bind (val pos) (parse-integer s :junk-allowed t)
    (declare (ignore pos))
    (cond ((null val) (error "Missing or invalid value: ~S" s))
          ((< val 0) (error "Expected unsigned value: ~S" val))
          (t val))))

(defun ensure-unicode (s)
  "Many CL implementations support Unicode strings; this method ensures that octet vectors are converted
 into their corresponding string representation."
  (if (> 0 (length s))
      (multiple-value-bind (vector-subtype vectype-valid) (subtypep (type-of s) '(vector (unsigned-byte 8)))
      	(if (and vector-subtype vectype-valid)
            (babel:octets-to-string s :encoding :utf-8))))
  ;; All else fails, return the original string...
  s)

(defun uncook-text (s preserve)
  "Strip whitespace around the string unless ``preserve`` is ``true``, then unescape UTF-16/UCS2 characters."
  (let ((stripped-s (if (not preserve)
                        (string-trim '(#\Space #\Tab #\Newline #\Return) s)
                        s)))
    (ensure-unicode (unescape-utf16 stripped-s))))

;; NOTE: Potential issue here iff (code-char c) doesn't support UTF-16/UCS2.
(defun unescape-utf16 (s)
  "Translate escaped UTF-16/UCS2 characters to their Unicode character equivalent within strings. These
escape sequences are delimited as '_xdddd_', where 'dddd' is the escaped character."
  (labels ((hexdigit (c) (let* ((i (- (char-code c) (char-code #\0)))
                                (j (+ (- (char-code (char-upcase c)) (char-code #\A)) 10)))
                           (or (< i 10)
                               (< j 16))))
           (unescape (pos len work-s)
             (if (and pos
                      (< pos len))
                 ;; If pos < len, then we know we have an underscore and have to parse.
                 ;; Could probably simplify with using regex packages (cl-ppre?), but not
                 ;; sure it has the same efficiency.
                 (if (and (< (+ pos 6) (length work-s))
                          (eql (aref work-s (+ pos 1)) #\x)
                          (hexdigit (aref work-s (+ pos 2)))
                          (hexdigit (aref work-s (+ pos 3)))
                          (hexdigit (aref work-s (+ pos 4)))
                          (hexdigit (aref work-s (+ pos 5)))
                          (eql (aref work-s (+ pos 6)) #\_))
                     ;; convert escaped character
                     (let ((lhs (subseq work-s 0 pos))
                           (rhs (subseq work-s (+ pos 7)))
                           (cval (parse-integer (subseq work-s (+ pos 2) (+ pos 6)) :radix 16)))
                       (uiop:strcat lhs
                                    (string (code-char cval))
                                    (unescape (position #\_ rhs) (length rhs) rhs)))
                     (unescape (position #\_ work-s :start (+ pos 1)) len work-s))
                 work-s)))
    (unescape (position #\_ s) (length s) s)))
  
(defconstant +char-a+ (char-code #\A))
(defconstant +char-z+ (char-code #\Z))
(defconstant +char-0+ (char-code #\0))
(defconstant +char-9+ (char-code #\9))
(defconstant +alpha-scale+ (1+ (- +char-z+ +char-a+)))

(defun parse-cell-ref (cell-ref)
  "Parse a cell reference, such as \"A1\", \"AK47\" or \"$BF$22\", to array row and column indices, i.e., 0-based
indices. For example, \"A1\" parses to `(0, 0)` and \"AK47\" is `(46, 36)`. It also handles *absolute* references
of the form \"$A$1\" by ignoring the dollar signs.

:cl:function:`parse-cell-ref` also handles worksheet-prefixed references, such as \"Sheet2!ZZ65\".

:param string cell-ref: The cell reference.
:rtype: Multiple values: ``(values row-index column-index worksheet-name endpos)``
:return: `row-index` and `column-index` indices are self-explanitory. `worksheet-name` is either a string or nil.
   `endpos` is the position in the string where the conversion stopped (typically used to ensure that the entire
   string was parsed.)"
  
  (let ((col-idx 0)
        (row-idx 0)
        (endpos 0)
        (worksheet-name nil)
        (bang-sep (position #\! cell-ref))
        (cell-ref-to-parse cell-ref))
    (when (and bang-sep (> bang-sep 0))
      (setf worksheet-name (subseq cell-ref 0 bang-sep))
      (setf endpos (1+ bang-sep))
      (setf cell-ref-to-parse (subseq cell-ref endpos)))
    
    (map nil (lambda (c)
               (let ((up-c (char-code (char-upcase c))))
                 ;; (format t "up-c ~A col-idx ~3D~%" up-c col-idx)
                 (cond ((<= +char-a+ up-c +char-z+)
                        (setf col-idx (+ (1+ (- up-c +char-a+))
                                         (* col-idx +alpha-scale+)))
                        (incf endpos))
                       ((<= +char-0+ up-c +char-9+)
                        (setf row-idx (+ (- up-c +char-0+)
                                         (* row-idx 10)))
                        (incf endpos))
                       ((= up-c (char-code #\!))
                        ;; Worksheet prefix
                        (setf worksheet-name (subseq cell-ref 0 endpos))
                        (setf col-idx 0)
                        (setf row-idx 0)
                        (incf endpos))
                       ((= up-c (char-code #\$))
                        (incf endpos))
                       (t (error "Invalid cell reference character ~S" c)))))
         cell-ref-to-parse)
    (values (if row-idx (1- row-idx) row-idx)
            (if col-idx (1- col-idx) col-idx)
            worksheet-name
            endpos)))

(defun to-cell-ref (row col &optional sheet-name)
  "Create a cell reference from a row and column. This is the inverse of :cl:function:`parse-cell-ref`."
  (let ((retval (make-array (+ (length sheet-name) 32)
                            :element-type 'character 
                            :initial-element #\Space 
                            :fill-pointer 0 
                            :adjustable t))
        ;; Array references are 0-based, whereas the cell references are 1-based:
        (t-row (1+ row))
        (t-col (1+ col)))
    (when sheet-name
      (format retval "~A!" sheet-name))
    (do ((ridx 0 (1+ ridx)))
        ((<= t-col +alpha-scale+))
      (multiple-value-bind (d r) (truncate t-col +alpha-scale+)
        (vector-push-extend (code-char (+ (1- d) +char-a+)) retval)
        (setf t-col r)))
    (vector-push-extend (code-char (+ (1- t-col) +char-a+)) retval)
    (format retval "~D" t-row)
    retval))

#|
~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
Pretty-printing and string conversion functions:
~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
|#

(defun format-type-string (fmt-type)
  (cond ((eql fmt-type FMT-FUN) "FMT-FUN")
        ((eql fmt-type FMT-FDT) "FMT-FDT")
        ((eql fmt-type FMT-FNU) "FMT-FNU")
        ((eql fmt-type FMT-FGE) "FMT-FGE")
        ((eql fmt-type FMT-FTX) "FMT-FTX")
        (t (let ((s (make-array 16 :fill-pointer 0 :element-type 'character)))
             (format s "?????? (~D)" fmt-type)
             s))))

(defun xl-cell-type (cell-type)
  (cond ((eql cell-type XL-CELL-EMPTY) "XL-CELL-EMPTY")
        ((eql cell-type XL-CELL-TEXT) "XL-CELL-TEXT")
        ((eql cell-type XL-CELL-NUMBER) "XL-CELL-NUMBER")
        ((eql cell-type XL-CELL-DATE) "XL-CELL-DATE")
        ((eql cell-type XL-CELL-BOOLEAN) "XL-CELL-BOOLEAN") 
        ((eql cell-type XL-CELL-ERROR) "XL-CELL-ERROR")
        ((eql cell-type XL-CELL-BLANK) "XL-CELL-BLANK")
        (t (let ((s (make-array 16 :adjustable t :fill-pointer 0 :element-type 'character)))
             (format s "??cell-type(~D)??" cell-type)
             s))))

(defun string-to-cell-error (s)
  (cond ((string-equal s "#NULL!") CELL-ERR-NULL)
        ((string-equal s "#DIV/0!") CELL-ERR-DIV0)
        ((string-equal s "#VALUE!") CELL-ERR-VALUE)
        ((string-equal s "#REF!") CELL-ERR-REF!)
        ((string-equal s "#NAME?") CELL-ERR-NAME?)
        ((string-equal s "#NUM!") CELL-ERR-NUM!)
        ((string-equal s "#N/A") CELL-ERR-N/A)
        (t (progn
             (format *error-output* "Unrecognized cell error string (~S), assuming #N/A~%" s)
             CELL-ERR-N/A))))
