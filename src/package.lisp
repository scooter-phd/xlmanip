;; ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
;; Package definitions for XLMANIP
;; ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=

(in-package :cl-user)

(cl:defpackage :xlmanip
  (:use :cl)
  (:nicknames :cl-excel)
  (:export #:xlread

           #:workbook
           #:workbook-emit-debug

           #:workbook-verbose
           #:workbook-verbose-log
           #:workbook-datemode
#|
           #:workbook-biff-version
           #:workbook-name-objects
           #:workbook-codepage
           #:workbook-encoding
           #:workbook-countries
           #:workbook-user-name
           #:workbook-props
           #:workbook-font-list
           #:workbook-xf-list
           #:workbook-format-list
           #:workbook-format-map
           #:workbook-style-name-map
           #:workbook-color-map
           #:workbook-pallate-record
           #:workbook-name-and-scope-map
           #:workbook-name-map
           #:workbook-ragged-rows
           #:workbook-calc-sheets
           #:workbook-sheet-visibility
           #:workbook-sh-abs-posn
           #:workbook-shared-strings
           #:workbook-rich-text-runlist-map
           #:workbook-raw-user-name
           #:workbook-sheethdr-count
           #:workbook-builtinfmtcount
           #:workbook-xfcount
           #:workbook-actual-fmt-count
           #:workbook-xf-index->xl-type
           #:workbook-xf-epilogue-done
           #:workbook-supbook-locals-inx
           #:workbook-supbook-addins-inx
           #:workbook-all-sheets-map
           #:workbook-externsheet-info
           #:workbook-externsheet-type-b57
           #:workbook-extnsht-name-from-num
           #:workbook-sheet-num-from-name
           #:workbook-extnsht-count
           #:workbook-supbook-types
           #:workbook-resources-released
           #:workbook-addin-func-names
|#

           #:workbook-nsheets
           #:get-worksheet
           #:*debug-column-iter*
           #:*debug-row-iter*
           #:make-cell-range-iterator
           #:worksheet-all-cells-range

           #:worksheet
           #:worksheet-name
           #:worksheet-cell-values
           #:worksheet-nrows
           #:worksheet-ncols
           #:worksheet-dimnrows
           #:worksheet-dimncols
           #:worksheet-extents
           #:worksheet-map-rows
           #:with-worksheet-rows
           #:with-one-worksheet-row
           #:with-all-worksheet-rows
           #+5am #:collect-cell-value-subtrees

           #:sheet-cell
           #:make-sheet-cell
           #:sheet-cell-ctype
           #:sheet-cell-value
           #:sheet-cell-formula
           #:sheet-cell-xf-index
           #:sheet-cell-row
           #:sheet-cell-col
           #:value-to-sheet-cell

           #:make-worksheet-cell-values
           #:insert-cell-value

           #:cell-range

           #:empty-cell

           ;; Debugging levels
           #:+xl-debug-all+
           #:+xl-debug-phases+
           #:+xl-debug-validate+
           #:+xl-debug-data+
           #:+xl-debug-rows+
           #:+xl-debug-xml+
           #:+xl-debug-non-special-mask+
           ;; Debugging specials
           #:+xl-dump-shared-strings+
           #:+xl-debug-date-formats+
           #:+xl-debug-allocate+

           ;; Parameters
           #:*xl-bad-date-guesses*

           ;; Utility function for combining debugging levels and specials
           #:debug-level
           #:debug-threshold

           ;; Cell constants
           #:XL-CELL-EMPTY
           #:XL-CELL-TEXT
           #:XL-CELL-NUMBER
           #:XL-CELL-DATE
           #:XL-CELL-BOOLEAN
           #:XL-CELL-ERROR
           #:XL-CELL-BLANK

           ;; Format constants
           #:FMT-FUN
           #:FMT-FDT
           #:FMT-FNU
           #:FMT-FGE
           #:FMT-FTX

           ;; Quadtree (for testing)
           #:quadtree
           #:quadtree-children
           #:quadtree-boundary
           #:insert-quadtree
           #:quadtree-query
           #:intersect-p
           #+5am #:compare-child-objects
           #+5am #:make-quadtree
           #+5am #:*worksheet-quad-capacity*
           #+5am #:*max-worksheet-quad-depth*

           ;; For testing (primarily):
           #+5am #:convert-xml-boolean
           #+5am #:convert-xml-unsigned-int
           #+5am #:unescape-utf16
           #+5am #:xl-cell-type
           #:parse-cell-ref
           #:to-cell-ref))
