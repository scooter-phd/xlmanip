;; Check data integrity (actual vs. expected contents)

(in-package :xlmanip-unittest)
(in-suite xlmanip-suite)

(defparameter *compare-verbose* nil)
(defparameter *float-compare-verbose* nil)

(defun compare-floats (f1 f2)
  "Compare two floating point values, allowing for up to 4 bits of noise in the units-last-place (low bits)."
  (multiple-value-bind (f1-signif f1-expon f1-sign) (integer-decode-float f1)
    (when *float-compare-verbose*
      (format *error-output* "f1: signif = 0x~10,'0X expon = 0x~2X sign = ~D~%" f1-signif f1-expon f1-sign))
    (multiple-value-bind (f2-signif f2-expon f2-sign) (integer-decode-float f2)
      (when *float-compare-verbose*
        (format *error-output* "f2: signif = 0x~10,'0X expon = 0x~2X sign = ~D~%" f2-signif f2-expon f2-sign))
      (and (eql f1-sign f2-sign)
           (eql f1-expon f2-expon)
           (or (eql f1-signif f2-signif)
               (eql (logand f1-signif #x0f)
                    (logand f2-signif #x0f)))))))

(defun compare-book-data (fname testbook)
  (mapcar
   (lambda (sheet-data)
     (destructuring-bind (sheet-name sheet-nrows sheet-ncols sheet-dimnrows sheet-dimncols row-data)
         sheet-data
       (let ((testsheet (xlmanip:get-worksheet testbook sheet-name)))
         (when *compare-verbose*
           (format *error-output*
                   "testsheet ~A sheet-rows ~D sheet-cols ~D nrows ~D ncols ~D dimnrows ~A/~A dimncols ~A/~A~%"
                   testsheet sheet-nrows sheet-ncols
                   (xlmanip:worksheet-nrows testsheet)
                   (xlmanip:worksheet-ncols testsheet)
                   (xlmanip:worksheet-dimnrows testsheet)
                   sheet-dimnrows
                   (xlmanip:worksheet-dimncols testsheet)
                   sheet-dimncols))
         (is-true testsheet)
         (is (eql sheet-dimnrows (xlmanip:worksheet-dimnrows testsheet)))
         (is (eql sheet-dimncols (xlmanip:worksheet-dimncols testsheet)))
         (is (eql sheet-nrows (xlmanip:worksheet-nrows testsheet)))
         (is (eql sheet-ncols (xlmanip:worksheet-ncols testsheet)))
         ;; Compare row contents:
         (dotimes (i (length row-data))
           (let* ((testsheet-row (aref row-data i))
                  (testsheet-rindex (aref testsheet-row 0))
                  (testsheet-rdata  (aref testsheet-row 1)))
             (xlmanip:with-one-worksheet-row testsheet testsheet-rindex (r sheet-row)
               (if (/= (length sheet-row) (length testsheet-rdata))
                   (fail (format nil "~A: rindex ~D len rdata ~D len sheet-row ~D~%rdata     ~:W~%sheet-row ~:W~%"
                                 sheet-name testsheet-rindex (length testsheet-rdata) (length sheet-row)
                                 testsheet-rdata sheet-row))
                   (do ((i 0 (1+ i))
                        (ok t ok))
                       ((or (>= i (length sheet-row))
                            (not ok)))
                     (let* ((cell (aref sheet-row i))
                            (cell-value (sheet-cell-value cell))
                            (cell-ctype (sheet-cell-ctype cell))
                            (testdata-content (aref testsheet-rdata i))
                            (testdata-col (first testdata-content))
                            (testdata-value (second testdata-content)))
                       (setf ok (and ok
                                     (= testdata-col i)
                                     (or (and (eql cell empty-cell)
                                              (eql cell testdata-value))
                                         (let ((testdata-ctype (third testdata-content)))
                                           (and (eql (sheet-cell-ctype cell) testdata-ctype)
                                                (cond ((or (eql testdata-ctype XL-CELL-TEXT)
                                                           (eql testdata-ctype XL-CELL-BLANK))
                                                       (string= cell-value testdata-value))
                                                      ((or (eql testdata-ctype XL-CELL-NUMBER)
                                                           (eql testdata-ctype XL-CELL-DATE))
                                                       (compare-floats cell-value testdata-value))
                                                      ((eql testdata-ctype XL-CELL-BOOLEAN)
                                                       (= cell-value testdata-value))
                                                      ((eql testdata-ctype XL-CELL-ERROR)
                                                       t)))))))
                       ;; If something failed, diagnose the problem...
                       (cond ((/= testdata-col i)
                              (fail (format nil "~A: row ~3D col=~3D/i=~3D column index mismatch"
                                            sheet-name testsheet-rindex testdata-col i)))
                             ((and (or (equal cell empty-cell)
                                       (equal testdata-value empty-cell))
                                   (not (equal cell testdata-value)))
                              (fail (format nil
                                            "~A: row ~3D col=~3D/i=~3D empty cell mismatch~%data:     ~:W~%sheet cell: ~:W"
                                            sheet-name testsheet-rindex testdata-col i
                                            testdata-value cell)))
                             ((not ok)
                              (fail (format nil
                                            "~A: row ~3D col=~3D/i=~3D ~A mismatch~%rdata:      ~:W~%sheet cell: ~:W"
                                            sheet-name testsheet-rindex testdata-col i
                                            (xlmanip:xl-cell-type (third testdata-content))
                                            (aref testsheet-rdata i)
                                            cell-value)))))))))))))
   (symbol-value (intern fname))))

;; Basic open operation: Do the spreadsheets open without reporting an error?
(setf xlmanip:*xl-bad-date-guesses* nil)
(def-test data-validate (:depends-on (AND cell-references quadtree-test))
  (dolist (fname '("reveng1.xlsx"
                   "apachepoi_49609.xlsx"
                   "merged_cells.xlsx"
                   "self_evaluation_report_2014-05-19.xlsx"
                   "test_comments_excel.xlsx"
                   "test_comments_gdocs.xlsx"
                   "text_bar.xlsx"))
    (let ((xlread-verbosity 0))
      (when *compare-verbose*
        (setf xlread-verbosity (if (numberp *compare-verbose*)
                                   *compare-verbose*
                                   (xlmanip:debug-level xlmanip:+xl-debug-data+)))
        (format *error-output* "fname ~A: " fname))
      (let ((book (xlmanip:xlread (asdf:system-relative-pathname :xlmanip (uiop:strcat "tests/sheets/" fname))
                                  :verbose xlread-verbosity)))
        (compare-book-data fname book)))))
