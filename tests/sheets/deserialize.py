#!/usr/bin/env python
from __future__ import print_function

import xlrd
import sys

def babelString(s):
    if any([(ord(c) < 0x20 or ord(c) > 0x7e) for c in s]):
        octets = []
        for c in s.encode('utf-8'):
            ci = ord(c)
            if ci > 0x20 and ci < 0x7f:
                octets.append("(char-code #\\%c)" % c)
            elif ci == 0x0a:
                octets.append("(char-code #\\Linefeed)")
            elif ci == 0x08:
                octets.append("(char-code #\\Backspace)")
            elif ci == 0x09:
                octets.append("(char-code #\\Tab)")
            elif ci == 0x0d:
                octets.append("(char-code #\\Return)")
            elif ci == 0x0c:
                octets.append("(char-code #\\Page)")
            elif ci == 0x20:
                octets.append("(char-code #\\Space)")
            else:
                octets.append("#x%02x" % (ord(c)))

        return (' '.join(["(babel:octets-to-string",
                          "(make-array %d :element-type '(unsigned-byte 8)",
                          ":initial-contents (list %s)) :encoding :utf-8)"]) % (len(octets), ' '.join(octets)))
    else:
        return "\"" + s + "\""


def cellCtype(c):
    if c.ctype == xlrd.XL_CELL_EMPTY:
        return "xlmanip:XL-CELL-EMPTY"
    elif c.ctype == xlrd.XL_CELL_TEXT:
        return "xlmanip:XL-CELL-TEXT"
    elif c.ctype == xlrd.XL_CELL_NUMBER:
        return "xlmanip:XL-CELL-NUMBER"
    elif c.ctype == xlrd.XL_CELL_DATE:
        return "xlmanip:XL-CELL-DATE"
    elif c.ctype == xlrd.XL_CELL_BOOLEAN:
        return "xlmanip:XL-CELL-BOOLEAN"
    elif c.ctype == xlrd.XL_CELL_ERROR:
        return "xlmanip:XL-CELL-ERROR"
    elif c.ctype == xlrd.XL_CELL_BLANK:
        return "xlmanip:XL-CELL-BLANK"
    else:
        return "nil"

if __name__ == '__main__' and len(sys.argv) == 2:
    # print("Processing %s\n" % sys.argv[1])
    fname = sys.argv[1]
    print("(in-package :xlmanip-unittest)")
    print("(when (boundp '|%s|)\n  (makunbound '|%s|))" % (fname, fname))
    wb = xlrd.open_workbook(filename=fname)
    sheetdribble = []
    for sheet in wb.sheet_names():
        the_sheet = wb.sheet_by_name(sheet)
        rdribble = []

        for i in range(the_sheet.nrows):
            rdata = the_sheet.row(i)
            cdata = []
            last_col = len(rdata) - 1
            while last_col >= 0 and (rdata[last_col].ctype == xlrd.XL_CELL_EMPTY or rdata[last_col].ctype == xlrd.XL_CELL_BLANK):
              last_col = last_col - 1
            if last_col >= 0:
              for (v, j) in zip(rdata, range(last_col + 1)):
                  if v.ctype == xlrd.XL_CELL_TEXT:
                      cdata.append("(list %d %s %s)" % (j, babelString(v.value), cellCtype(v)))
                  elif v.ctype == xlrd.XL_CELL_EMPTY or v.ctype == xlrd.XL_CELL_BLANK:
                      cdata.append("(list %d xlmanip:empty-cell)" % (j))
                  elif v.ctype == xlrd.XL_CELL_NUMBER or v.ctype == xlrd.XL_CELL_DATE:
		      dval = ("%.16e" % (v.value)).replace('e', 'd')
                      cdata.append("(list %d %s %s)" % (j, dval, cellCtype(v)))
                  else:
                      cdata.append("(list %d %s %s)" % (j, v.value, cellCtype(v)))

	    if len(cdata) > 0:
	      rdribble.append(("(vector %d (vector %s))" % (i, ' '.join(cdata))))
	    else:
	      rdribble.append("(vector %d #())" % (i))

        if len(rdribble) > 0:
            sheetdribble.append(("(list \"%s\" %d %d %d %d\n      (vector\n        %s))" %
              (sheet, the_sheet.nrows, the_sheet.ncols, the_sheet._dimnrows, the_sheet._dimncols, '\n        '.join(rdribble))))

    sys.stdout.write(("(defvar |%s|\n  (list\n    %s))" % (fname, '\n    '.join(sheetdribble))).encode('utf-8'))
