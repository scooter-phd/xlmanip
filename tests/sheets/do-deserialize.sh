#!/bin/bash

for s in *.xlsx; do
  f=../validate/$(basename -s .xlsx $s).lisp
  echo $f
  python deserialize.py $s > $f
done

for s in *.xls; do
  f=../validate/$(basename -s .xls $s).lisp
  echo $f
  python deserialize.py $s > $f
done
