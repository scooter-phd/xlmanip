;; Cell reference parsing tests:

(in-package :xlmanip-unittest)
(in-suite xlmanip-suite)

(defun cell-ref-harness (inp row col &optional worksheet-name)
  (multiple-value-bind (row-index col-index sheet-name endpos)
                       (xlmanip:parse-cell-ref inp)
;;     (format t "cell-ref-harness: row = ~D, col = ~D, endpos = ~D/~D, sheet-name = ~S~%" 
;;             row-index 
;;             col-index 
;;             endpos 
;;             (length inp) 
;;             sheet-name)

    (is (and (eql row row-index)
             (eql col col-index)
             (eql endpos (length inp))
             (or (null worksheet-name)
                 (string-equal worksheet-name sheet-name))))))

;; Verify that cell reference parsing works:
(def-test cell-references ()
  (cell-ref-harness "A1" 0 0)
  (cell-ref-harness "B1" 0 1)
  (cell-ref-harness "AA25" 24 26)
  (cell-ref-harness "Worksheet!A1" 0 0 "worksheet")
  (cell-ref-harness "Worksheet!AA1" 0 26 "worksheet"))
