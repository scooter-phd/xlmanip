(in-package :xlmanip-unittest)
(when (boundp '|ragged.xls|)
  (makunbound '|ragged.xls|))
(defvar |ragged.xls|
  (list
    (list "Sheet1" 5 4 5 4
      (vector
        (vector 0 (vector (list 0 "a" xlmanip:XL-CELL-TEXT) (list 1 "b" xlmanip:XL-CELL-TEXT) (list 2 "c" xlmanip:XL-CELL-TEXT)))
        (vector 1 (vector (list 0 "d" xlmanip:XL-CELL-TEXT) (list 1 "e" xlmanip:XL-CELL-TEXT)))
        (vector 2 (vector (list 0 "f" xlmanip:XL-CELL-TEXT)))
        (vector 3 (vector (list 0 "g" xlmanip:XL-CELL-TEXT) (list 1 "h" xlmanip:XL-CELL-TEXT) (list 2 "I" xlmanip:XL-CELL-TEXT) (list 3 "j" xlmanip:XL-CELL-TEXT)))
        (vector 4 (vector (list 0 "k" xlmanip:XL-CELL-TEXT) (list 1 xlmanip:empty-cell) (list 2 xlmanip:empty-cell) (list 3 "l" xlmanip:XL-CELL-TEXT)))))))