(in-package :xlmanip-unittest)
(when (boundp '|issue20.xls|)
  (makunbound '|issue20.xls|))
(defvar |issue20.xls|
  (list
    (list "Sheet1" 12 12 17 13
      (vector
        (vector 0 (vector (list 0 "aasa" xlmanip:XL-CELL-TEXT)))
        (vector 1 (vector (list 0 "asasa" xlmanip:XL-CELL-TEXT)))
        (vector 2 (vector (list 0 "asasa" xlmanip:XL-CELL-TEXT)))
        (vector 3 (vector (list 0 "asasa" xlmanip:XL-CELL-TEXT)))
        (vector 4 (vector (list 0 "asasa" xlmanip:XL-CELL-TEXT)))
        (vector 5 #())
        (vector 6 #())
        (vector 7 #())
        (vector 8 #())
        (vector 9 #())
        (vector 10 (vector (list 0 xlmanip:empty-cell) (list 1 xlmanip:empty-cell) (list 2 xlmanip:empty-cell) (list 3 xlmanip:empty-cell) (list 4 xlmanip:empty-cell) (list 5 xlmanip:empty-cell) (list 6 xlmanip:empty-cell) (list 7 xlmanip:empty-cell) (list 8 xlmanip:empty-cell) (list 9 xlmanip:empty-cell) (list 10 xlmanip:empty-cell) (list 11 "asasa" xlmanip:XL-CELL-TEXT)))
        (vector 11 (vector (list 0 "legenda " xlmanip:XL-CELL-TEXT) (list 1 xlmanip:empty-cell) (list 2 xlmanip:empty-cell) (list 3 xlmanip:empty-cell) (list 4 xlmanip:empty-cell) (list 5 "as" xlmanip:XL-CELL-TEXT)))))))