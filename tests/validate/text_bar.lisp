(in-package :xlmanip-unittest)
(when (boundp '|text_bar.xlsx|)
  (makunbound '|text_bar.xlsx|))
(defvar |text_bar.xlsx|
  (list
    (list "Sheet1" 1 1 1 1
      (vector
        (vector 0 (vector (list 0 "bar" xlmanip:XL-CELL-TEXT)))))))