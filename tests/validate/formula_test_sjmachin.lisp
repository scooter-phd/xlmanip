(in-package :xlmanip-unittest)
(when (boundp '|formula_test_sjmachin.xls|)
  (makunbound '|formula_test_sjmachin.xls|))
(defvar |formula_test_sjmachin.xls|
  (list
    (list "Sheet1" 8 2 8 2
      (vector
        (vector 0 (vector (list 0 "Description" xlmanip:XL-CELL-TEXT) (list 1 "Data" xlmanip:XL-CELL-TEXT)))
        (vector 1 (vector (list 0 "Non-latin1 text" xlmanip:XL-CELL-TEXT) (list 1 (babel:octets-to-string (make-array 25 :element-type '(unsigned-byte 8) :initial-contents (list #xd0 #x9c #xd0 #x9e #xd0 #xa1 #xd0 #x9a #xd0 #x92 #xd0 #x90 (char-code #\Space) #xd0 #x9c #xd0 #xbe #xd1 #x81 #xd0 #xba #xd0 #xb2 #xd0 #xb0)) :encoding :utf-8) xlmanip:XL-CELL-TEXT)))
        (vector 2 (vector (list 0 "formula number" xlmanip:XL-CELL-TEXT) (list 1 1.4285714285714285d-01 xlmanip:XL-CELL-NUMBER)))
        (vector 3 (vector (list 0 "formula text" xlmanip:XL-CELL-TEXT) (list 1 "ABCDEF" xlmanip:XL-CELL-TEXT)))
        (vector 4 (vector (list 0 "formula zero-length text" xlmanip:XL-CELL-TEXT) (list 1 "" xlmanip:XL-CELL-TEXT)))
        (vector 5 (vector (list 0 "formula boolean" xlmanip:XL-CELL-TEXT) (list 1 1 xlmanip:XL-CELL-BOOLEAN)))
        (vector 6 (vector (list 0 "formula error" xlmanip:XL-CELL-TEXT) (list 1 7 xlmanip:XL-CELL-ERROR)))
        (vector 7 (vector (list 0 "formula non-latin1 text" xlmanip:XL-CELL-TEXT) (list 1 (babel:octets-to-string (make-array 25 :element-type '(unsigned-byte 8) :initial-contents (list #xd0 #x9c #xd0 #x9e #xd0 #xa1 #xd0 #x9a #xd0 #x92 #xd0 #x90 (char-code #\Space) #xd0 #x9c #xd0 #xbe #xd1 #x81 #xd0 #xba #xd0 #xb2 #xd0 #xb0)) :encoding :utf-8) xlmanip:XL-CELL-TEXT)))))))