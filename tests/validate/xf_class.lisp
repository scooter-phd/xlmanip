(in-package :xlmanip-unittest)
(when (boundp '|xf_class.xls|)
  (makunbound '|xf_class.xls|))
(defvar |xf_class.xls|
  (list
    (list "table1" 10 3 10 3
      (vector
        (vector 0 (vector (list 0 "RED" xlmanip:XL-CELL-TEXT) (list 1 "RED" xlmanip:XL-CELL-TEXT)))
        (vector 1 (vector (list 0 "GREEN" xlmanip:XL-CELL-TEXT) (list 1 "GREEN" xlmanip:XL-CELL-TEXT)))
        (vector 2 (vector (list 0 "BLUE" xlmanip:XL-CELL-TEXT) (list 1 "BLUE" xlmanip:XL-CELL-TEXT)))
        (vector 3 (vector (list 0 "LEFT" xlmanip:XL-CELL-TEXT) (list 1 "CENTER" xlmanip:XL-CELL-TEXT) (list 2 "RIGHT" xlmanip:XL-CELL-TEXT)))
        (vector 4 (vector (list 0 "TOP" xlmanip:XL-CELL-TEXT) (list 1 "MIDDLE" xlmanip:XL-CELL-TEXT) (list 2 "BOTTOM" xlmanip:XL-CELL-TEXT)))
        (vector 5 #())
        (vector 6 #())
        (vector 7 #())
        (vector 8 #())
        (vector 9 (vector (list 0 "borderstyle" xlmanip:XL-CELL-TEXT)))))
    (list "table2" 4 3 7 5
      (vector
        (vector 0 #())
        (vector 1 #())
        (vector 2 #())
        (vector 3 (vector (list 0 xlmanip:empty-cell) (list 1 xlmanip:empty-cell) (list 2 "MERGED" xlmanip:XL-CELL-TEXT)))))
    (list "table3" 3 2 3 2
      (vector
        (vector 0 (vector (list 0 "RED" xlmanip:XL-CELL-TEXT) (list 1 "RED" xlmanip:XL-CELL-TEXT)))
        (vector 1 (vector (list 0 "GREEN" xlmanip:XL-CELL-TEXT) (list 1 "GREEN" xlmanip:XL-CELL-TEXT)))
        (vector 2 (vector (list 0 "BLUE" xlmanip:XL-CELL-TEXT) (list 1 "BLUE" xlmanip:XL-CELL-TEXT)))))))