(in-package :xlmanip-unittest)
(when (boundp '|self_evaluation_report_2014-05-19.xlsx|)
  (makunbound '|self_evaluation_report_2014-05-19.xlsx|))
(defvar |self_evaluation_report_2014-05-19.xlsx|
  (list
    (list "test1" 1 2 0 0
      (vector
        (vector 0 (vector (list 0 "one" xlmanip:XL-CELL-TEXT) (list 1 "two" xlmanip:XL-CELL-TEXT)))))
    (list "test2" 1 2 0 0
      (vector
        (vector 0 (vector (list 0 "three" xlmanip:XL-CELL-TEXT) (list 1 "four" xlmanip:XL-CELL-TEXT)))))))