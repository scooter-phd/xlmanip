(in-package :xlmanip-unittest)
(when (boundp '|merged_cells.xlsx|)
  (makunbound '|merged_cells.xlsx|))
(defvar |merged_cells.xlsx|
  (list
    (list "Sheet2" 1 2 1 2
      (vector
        (vector 0 (vector (list 0 "Foo" xlmanip:XL-CELL-TEXT)))))
    (list "Sheet3" 9 4 9 4
      (vector
        (vector 0 (vector (list 0 "Foo" xlmanip:XL-CELL-TEXT) (list 1 xlmanip:empty-cell) (list 2 "Bar" xlmanip:XL-CELL-TEXT)))
        (vector 1 (vector (list 0 "Baz" xlmanip:XL-CELL-TEXT) (list 1 xlmanip:empty-cell) (list 2 "Boo" xlmanip:XL-CELL-TEXT)))
        (vector 2 #())
        (vector 3 #())
        (vector 4 #())
        (vector 5 #())
        (vector 6 #())
        (vector 7 #())
        (vector 8 #())))
    (list "Sheet4" 20 5 20 5
      (vector
        (vector 0 (vector (list 0 "Foo" xlmanip:XL-CELL-TEXT)))
        (vector 1 (vector (list 0 xlmanip:empty-cell) (list 1 xlmanip:empty-cell) (list 2 "Baz" xlmanip:XL-CELL-TEXT)))
        (vector 2 (vector (list 0 "Bar" xlmanip:XL-CELL-TEXT)))
        (vector 3 #())
        (vector 4 #())
        (vector 5 #())
        (vector 6 #())
        (vector 7 #())
        (vector 8 #())
        (vector 9 #())
        (vector 10 #())
        (vector 11 #())
        (vector 12 #())
        (vector 13 #())
        (vector 14 #())
        (vector 15 #())
        (vector 16 #())
        (vector 17 #())
        (vector 18 #())
        (vector 19 #())))))