(in-package :xlmanip-unittest)
(when (boundp '|Formate.xls|)
  (makunbound '|Formate.xls|))
(defvar |Formate.xls|
  (list
    (list "Blätt1" 10 2 10 3
      (vector
        (vector 0 (vector (list 0 "Huber" xlmanip:XL-CELL-TEXT) (list 1 2.7410000000000000d+03 xlmanip:XL-CELL-DATE)))
        (vector 1 (vector (list 0 (babel:octets-to-string (make-array 6 :element-type '(unsigned-byte 8) :initial-contents (list #xc3 #x84 (char-code #\c) (char-code #\k) (char-code #\e) (char-code #\r))) :encoding :utf-8) xlmanip:XL-CELL-TEXT) (list 1 3.8406000000000000d+04 xlmanip:XL-CELL-DATE)))
        (vector 2 (vector (list 0 (babel:octets-to-string (make-array 6 :element-type '(unsigned-byte 8) :initial-contents (list #xc3 #x96 (char-code #\c) (char-code #\k) (char-code #\e) (char-code #\r))) :encoding :utf-8) xlmanip:XL-CELL-TEXT) (list 1 3.2266000000000000d+04 xlmanip:XL-CELL-DATE)))
        (vector 3 (vector (list 0 "Morgen" xlmanip:XL-CELL-TEXT) (list 1 2.7361111111111108d-01 xlmanip:XL-CELL-DATE)))
        (vector 4 (vector (list 0 "Mittag" xlmanip:XL-CELL-TEXT) (list 1 5.3888888888888886d-01 xlmanip:XL-CELL-DATE)))
        (vector 5 (vector (list 0 "Abends" xlmanip:XL-CELL-TEXT) (list 1 7.4112268518518520d-01 xlmanip:XL-CELL-DATE)))
        (vector 6 (vector (list 0 "gut" xlmanip:XL-CELL-TEXT) (list 1 9.7399999999999998d-01 xlmanip:XL-CELL-NUMBER)))
        (vector 7 (vector (list 0 "schlecht" xlmanip:XL-CELL-TEXT) (list 1 1.2400000000000000d-01 xlmanip:XL-CELL-NUMBER)))
        (vector 8 (vector (list 0 "viel" xlmanip:XL-CELL-TEXT) (list 1 1.0003000000000000d+03 xlmanip:XL-CELL-NUMBER)))
        (vector 9 (vector (list 0 "wenig" xlmanip:XL-CELL-TEXT) (list 1 1.2000000000000000d+00 xlmanip:XL-CELL-NUMBER)))))
    (list "ÖÄÜ" 3 3 5 5
      (vector
        (vector 0 (vector (list 0 -1.0000000000000000d+02 xlmanip:XL-CELL-NUMBER)))
        (vector 1 #())
        (vector 2 (vector (list 0 xlmanip:empty-cell) (list 1 xlmanip:empty-cell) (list 2 "MERGED CELLS" xlmanip:XL-CELL-TEXT)))))
    (list "Blätt3" 12 1 12 1
      (vector
        (vector 0 (vector (list 0 1.0000000000000000d+02 xlmanip:XL-CELL-NUMBER)))
        (vector 1 (vector (list 0 2.0000000000000000d+02 xlmanip:XL-CELL-NUMBER)))
        (vector 2 (vector (list 0 3.0000000000000000d+02 xlmanip:XL-CELL-NUMBER)))
        (vector 3 (vector (list 0 4.0000000000000000d+02 xlmanip:XL-CELL-NUMBER)))
        (vector 4 (vector (list 0 5.0000000000000000d+02 xlmanip:XL-CELL-NUMBER)))
        (vector 5 (vector (list 0 6.0000000000000000d+02 xlmanip:XL-CELL-NUMBER)))
        (vector 6 (vector (list 0 7.0000000000000000d+02 xlmanip:XL-CELL-NUMBER)))
        (vector 7 (vector (list 0 8.0000000000000000d+02 xlmanip:XL-CELL-NUMBER)))
        (vector 8 (vector (list 0 9.0000000000000000d+02 xlmanip:XL-CELL-NUMBER)))
        (vector 9 (vector (list 0 1.0000000000000000d+03 xlmanip:XL-CELL-NUMBER)))
        (vector 10 (vector (list 0 1.1000000000000000d+03 xlmanip:XL-CELL-NUMBER)))
        (vector 11 (vector (list 0 1.2000000000000000d+03 xlmanip:XL-CELL-NUMBER)))))
    (list "Formate" 3 2 3 2
      (vector
        (vector 0 (vector (list 0 "RED" xlmanip:XL-CELL-TEXT) (list 1 "RED" xlmanip:XL-CELL-TEXT)))
        (vector 1 (vector (list 0 "GREEN" xlmanip:XL-CELL-TEXT) (list 1 "GREEN" xlmanip:XL-CELL-TEXT)))
        (vector 2 (vector (list 0 "BLUE" xlmanip:XL-CELL-TEXT) (list 1 "BLUE" xlmanip:XL-CELL-TEXT)))))))