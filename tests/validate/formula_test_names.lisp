(in-package :xlmanip-unittest)
(when (boundp '|formula_test_names.xls|)
  (makunbound '|formula_test_names.xls|))
(defvar |formula_test_names.xls|
  (list
    (list "Sheet1" 8 2 8 2
      (vector
        (vector 0 (vector (list 0 "Description" xlmanip:XL-CELL-TEXT) (list 1 "Data" xlmanip:XL-CELL-TEXT)))
        (vector 1 (vector (list 0 "Unary operation" xlmanip:XL-CELL-TEXT) (list 1 -7.0000000000000000d+00 xlmanip:XL-CELL-NUMBER)))
        (vector 2 (vector (list 0 "Single param sum" xlmanip:XL-CELL-TEXT) (list 1 4.0000000000000000d+00 xlmanip:XL-CELL-NUMBER)))
        (vector 3 (vector (list 0 "Function call" xlmanip:XL-CELL-TEXT) (list 1 6.0000000000000000d+00 xlmanip:XL-CELL-NUMBER)))
        (vector 4 (vector (list 0 "Function with variable # args" xlmanip:XL-CELL-TEXT) (list 1 3.0000000000000000d+00 xlmanip:XL-CELL-NUMBER)))
        (vector 5 (vector (list 0 "If function" xlmanip:XL-CELL-TEXT) (list 1 "b" xlmanip:XL-CELL-TEXT)))
        (vector 6 (vector (list 0 "Choose function" xlmanip:XL-CELL-TEXT) (list 1 "C" xlmanip:XL-CELL-TEXT)))
        (vector 7 (vector (list 0 (babel:octets-to-string (make-array 25 :element-type '(unsigned-byte 8) :initial-contents (list (char-code #\B) (char-code #\i) (char-code #\n) (char-code #\a) (char-code #\r) (char-code #\y) (char-code #\Space) (char-code #\o) (char-code #\p) (char-code #\e) (char-code #\r) (char-code #\a) (char-code #\t) (char-code #\i) (char-code #\o) (char-code #\n) (char-code #\Space) #xe2 #x86 #x92 (char-code #\Space) (char-code #\b) (char-code #\o) (char-code #\o) (char-code #\l))) :encoding :utf-8) xlmanip:XL-CELL-TEXT) (list 1 1 xlmanip:XL-CELL-BOOLEAN)))))))