;; Test suite preamble:

(in-package :cl-user)

(cl:defpackage :xlmanip-unittest
  (:use :cl :xlmanip :fiveam)
  
  ;; Export the names of the test so that they can be run individually.
  (:export #:data-validate
           #:run-all
           #:xlmanip-suite

           #:*compare-verbose*
           #:*float-compare-verbose*))

(in-package :xlmanip-unittest)

;; Define FiveAM test suite:
(def-suite xlmanip-suite :description "XLMANIP unit tests")
