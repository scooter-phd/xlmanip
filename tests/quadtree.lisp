#|
  This file is a part of quadtree project.
  Copyright (c) 2015 Masayuki Takagi (kamonama@gmail.com)
|#

(in-package :xlmanip-unittest)
(in-suite xlmanip-suite)

(def-test compare-sheet-cells ()
  (let ((p1 (xlmanip:make-sheet-cell :ctype xlmanip:XL-CELL-TEXT :value "foo" :row 0 :col 0))
        (p2 (xlmanip:make-sheet-cell :ctype xlmanip:XL-CELL-TEXT :value "foo" :row 1 :col 0))
        (p3 (xlmanip:make-sheet-cell :ctype xlmanip:XL-CELL-TEXT :value "foo" :row 3 :col 4)))
    (is-true (xlmanip:compare-child-objects p1 p2))
    (is-false (xlmanip:compare-child-objects p2 p1))
    (is-true (xlmanip:compare-child-objects p2 p3))
    (is-false (xlmanip:compare-child-objects p3 p2))
    ;; Note: compare-child-objects returns nil if x1 >= x2.
    (is-false (xlmanip:compare-child-objects p3 p3))))

(def-test compare-cell-ranges ()
  (let ((r1 (make-instance 'xlmanip:cell-range :start-row 0 :start-col 0 :end-row 1 :end-col 1))
        (r2 (make-instance 'xlmanip:cell-range :start-row 1 :start-col 0 :end-row 1 :end-col 1)))
    (is-true (xlmanip:compare-child-objects r1 r2))))

(def-test worksheet-cell-quadtree (:depends-on compare-sheet-cells)
  (let ((cvals (xlmanip:make-worksheet-cell-values))
        (test-cells '((1 1 "biff")
                      (2 0 "baz")
                      (1024 0 "baff")
                      (0 0 "Foobars!")
                      (1024 1 "quux")
                      (1 0 "bronze")
                      (1023 0 "yellow")
                      (1023 1 "brick")
                      (1023 2 "road")
                      (1023 3 "Toto")
                      (1023 4 "Tin Man")
                      (1023 5 "Scarecrow")
                      (1023 6 "Dorothy")))
        retval)
    (dolist (l test-cells)
      (xlmanip:insert-cell-value cvals (xlmanip:make-sheet-cell :ctype xlmanip:XL-CELL-TEXT :value (third l) :xf-index 0
                                                                :row (first l) :col (second l))))
    (dotimes (r 15)
      (xlmanip:insert-cell-value cvals (xlmanip:make-sheet-cell :ctype xlmanip:XL-CELL-TEXT :value "filler" :xf-index 0
                                                                :row (+ r 3) :col 5))
      (xlmanip:insert-cell-value cvals (xlmanip:make-sheet-cell :ctype xlmanip:XL-CELL-TEXT :value "--filler--" :xf-index 0
                                                                :row 1023 :col (+ r 6))))

    ;; Query on cell ranges:
    (setf retval (xlmanip:collect-cell-value-subtrees cvals (make-instance 'xlmanip:cell-range
                                                                           :start-row 1023 :start-col 1
                                                                           :end-row 1025 :end-col 10)))

    (if (not (null retval))
        (is (= (length retval) 2))
        (is-true retval))

    (setf retval (xlmanip:collect-cell-value-subtrees cvals (make-instance 'xlmanip:cell-range
                                                                           :start-row 1 :start-col 2
                                                                           :end-row 1 :end-col 2)))
    (if (not (null retval))
        (is (= (length retval) 1))
        (is-true retval))))

(def-test quadtree-test (:depends-on (and worksheet-cell-quadtree compare-cell-ranges))
  (let ((qt (xlmanip:make-quadtree 0 0 100 200))
        (qt2 (xlmanip:make-quadtree 10 10 20 20))
        (p1 (make-instance 'xlmanip:cell-range :start-row 1 :start-col 1 :end-row 2 :end-col 1))
        (p2 (make-instance 'xlmanip:cell-range :start-row 101 :start-col 0 :end-row 101 :end-col 1))
        (p3 (make-instance 'xlmanip:cell-range :start-row 5 :start-col 1 :end-row 25 :end-col 25))
        (p4 (make-instance 'xlmanip:cell-range :start-row 10 :start-col 5 :end-row 10 :end-col 12))
        (p5 (make-instance 'xlmanip:cell-range :start-row 8 :start-col 8 :end-row 10 :end-col 12))
        (p6 (make-instance 'xlmanip:cell-range :start-row 11 :start-col 17 :end-row 19 :end-col 22))
        (p7 (make-instance 'xlmanip:cell-range :start-row 15 :start-col 12 :end-row 15 :end-col 17)))
    (is-true (xlmanip:intersect-p qt p1))
    (is-false (xlmanip:intersect-p qt p2))
    ;; p3 completely surrounds qt2
    (is-true (xlmanip:intersect-p qt2 p3))
    (is-true (xlmanip:intersect-p qt2 p4))
    (is-true (xlmanip:intersect-p qt2 p5))
    ;; p6 is within the quad, but off the right edge
    (is-true (xlmanip:intersect-p qt2 p6))

    (dotimes (i 8)
      (xlmanip:insert-quadtree qt2 (make-instance 'xlmanip:cell-range
                                                  :start-row (+ 10 i) :start-col 10
                                                  :end-row (+ 10 i) :end-col 12)))
    (xlmanip:insert-quadtree qt2 p7)
    (is-true (position p7 (xlmanip:quadtree-query qt2 15 13) :test 'equal))
    (is-true (position p7 (xlmanip:quadtree-query qt2 15 16) :test 'equal))
    (is-false (xlmanip:quadtree-query qt2 0 15))
    (is-false (xlmanip:quadtree-query qt2 10 4))
    (is-false (xlmanip:quadtree-query qt2 18 22))
    (is-false (xlmanip:quadtree-query qt2 25 14))
    (is-false (xlmanip:quadtree-query qt2 11 18)))

  (let ((qt (xlmanip:make-quadtree 0 0 100 200))
        (ranges '((1 1 2 1) (3 1 3 4) (10 5 20 5) (20 5 22 5)
                  (6 9 8 10) (6 5 6 10) (20 1 20 4) (60 6 60 13)
                  (10 1 15 1))))
    (dolist (pt ranges)
      (xlmanip:insert-quadtree qt (make-instance 'xlmanip:cell-range
                                                 :start-row (first pt) :start-col (second pt)
                                                 :end-row (third pt) :end-col (fourth pt))))
    ;; (format t "~:W~%" qt)
    (is-false (xlmanip:quadtree-query qt 20 20))))
